-- #VERSION:0000001000
--
-- HISTORIAL DE CAMBIOS
--
-- Versi�n     GAP           Solicitud        Fecha        Realiz�                          Descripci�n
-- =========== ============= ================ ============ ================================ ==============================================================================
-- 1000                      84031            28/03/2019   Inaction                         . Se crea tabla temporal para realizar manejo datos tipo date en control dual.
--                                                                                            
-- =========== ============= ================ ============ ================================ ==============================================================================

CREATE GLOBAL TEMPORARY TABLE SF_TTDAT (
  tdat_cmpo           varchar(20),
  tdat_date   date
)
ON COMMIT DELETE ROWS;