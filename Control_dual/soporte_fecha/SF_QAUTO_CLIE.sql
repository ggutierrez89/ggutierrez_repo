prompt
prompt package SF_QAUTO_CLIE
prompt
create or replace package  SF_QAUTO_CLIE is
    --
    -- #VERSION:0000001006
    --
    -- HISTORIAL DE CAMBIOS
    --
    -- Version     GAP           Solicitud        Fecha        Realizo        Descripcion
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1000                      RFC80577         28/03/2019   InAction       Creacion del paquete.
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1001                      RFC80577         17/05/2019   InAction       Se agrega condicion para que todo proceso quede con tpid definido
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1002                      RFC80577         17/05/2019   InAction       Se agrega condicion para que todo proceso quede con tpid definido
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1003                      82870            31/05/2019   InAction       Se agrega usua en los procedimientos de actualizar para que que genere el proceso con el usuario correcto
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1004                      82965            05/06/2019   InAction       Se actualiza nombre de la tabla mapeada en procedimiento
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1005                      83230            13/06/2019   InAction       Se realiza ajuste para que tome el numero de auxi_auxi en caliente y no coja el de la tabla sf_tprfd
    --                                                                        para evitar auxi_auxi duplicados y tener problemas en la creacion de los auxuliares
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1006                      83230            13/06/2019   InAction       Se realizan ajustes para que el mapeo tenga en cuenta los datos del auxiliar, reconociendo el auxi_auxi
    --                                                                        y permientiendo hacer actualizacion sobre la tabla ge_tauxil
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1007                      84031            06/08/2019   InAction       Se crean procedimientos para contolar errores de mascara en datos tipo date, son llamado en actualiza_cliente
    -- =========== ============= ================ ============ ============== ============================================================================================================
    
    /**
      * mapear_campos                 Procedimiento para mapear la informacion del campo, tabla y datos a insertar en la tabla SF_TDTPF
      *
      * @param  p_prfd                Codigo del proceso
      * @param  p_tbla                Tabla origen de los datos
      * @param  p_cmpo                Campo a modificar
      * @param  p_dtor                Dato antiguo
      * @param  p_dtnv                Dato nuevo
      */
    procedure mapear_campos (  p_prfd        sf_tdtpf.dtpf_prfd%type
                             , p_tbla        sf_tdtpf.dtpf_tbla%type
                             , p_cmpo        sf_tdtpf.dtpf_cmpo%type
                             , p_dtor        sf_tdtpf.dtpf_dtor%type
                             , p_dtnv        sf_tdtpf.dtpf_dtnv%type
                             , p_fdei        sf_tdtpf.dtpf_fdei%type
                             , p_idnu        sf_tdtpf.dtpf_idnu%type
                             , p_idvc        sf_tdtpf.dtpf_idvc%type
                             , p_tptr        sf_tdtpf.dtpf_tptr%type DEFAULT 'I'
                            );
    --
    /**
      * validar_proceso               Funcion que valida si existe un proceso previo en estado Insertado. Si existe retorna el numero de proceso,
      *                               si no existe genera un nuevo numero y lo retorna
      *
      * @param  p_tpid                Tipo de documento
      * @param  p_fdei                Numero de documento
      * @param  p_ty_erro             Type de error
      */
    function validar_proceso (  p_tpid   sf_tfdei.fdei_tpid%type
                              , p_fdei   sf_tfdei.fdei_fdei%type
                              , p_usua   sf_tprfd.prfd_usua%type default null
                             ) return number;
    --
    /**
      * insertar_cliente              Procedimiento para controlar la insercion de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_fdei                type de la tabla de clientes SF_TFDEI
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_cliente (  p_fdei        sf_tfdei%rowtype
                                , p_ty_erro out ge_qtipo.tr_error
                               );
    --
    /**
      * actualizar_cliente            Procedimiento para controlar la actualizacion de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_fdei_old            Type de la tabla de clientes SF_TFDEI, datos antiguos.
      * @param  p_fdei_new            Type de la tabla de clientes SF_TFDEI, datos nuevos.
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_cliente (  p_fdei_old    sf_tfdei%rowtype
                                  , p_fdei_new    sf_tfdei%rowtype
                                  , p_ty_erro out ge_qtipo.tr_error
                                 );
    --
    /**
      * insertar_direccion            Procedimiento para controlar la insercion de una direccion de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_drcl                type de la tabla de direcciones SF_TDRCL
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_direccion (  p_drcl        sf_tdrcl%rowtype
                                  , p_ty_erro out ge_qtipo.tr_error
                                 );
    --
    /**
      * actualizar_direccion          Procedimiento para controlar la actualizacion de una direccion de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_drcl_old            Type de la tabla de direcciones SF_TDRCL, datos antiguos.
      * @param  p_drcl_new            Type de la tabla de direcciones SF_TDRCL, datos nuevos.
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_direccion (  p_drcl_old    sf_tdrcl%rowtype
                                    , p_drcl_new    sf_tdrcl%rowtype
                                    , p_ty_erro out ge_qtipo.tr_error
                                   );
    --
    procedure eliminar_direccion (  p_ty_drcl  in out sf_tdrcl%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         );
    --
    /**
      * insertar_contacto             Procedimiento para controlar la insercion de un contacto de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_cnfd                type de la tabla de contactos SF_TCNFD
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_contacto (  p_cnfd        sf_tcnfd%rowtype
                                 , p_ty_erro out ge_qtipo.tr_error
                                );
    --
    /**
      * actualizar_contacto           Procedimiento para controlar la actualizacion de un contacto de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_cnfd_old            Type de la tabla de contactos SF_TCNFD, datos antiguos.
      * @param  p_cnfd_new            Type de la tabla de contactos SF_TCNFD, datos nuevos.
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_contacto (  p_cnfd_old    sf_tcnfd%rowtype
                                   , p_cnfd_new    sf_tcnfd%rowtype
                                   , p_ty_erro out ge_qtipo.tr_error
                                  );
    --
    procedure eliminar_contacto (  p_ty_cnfd  in out sf_tcnfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) ;
    --
    /**
      * insertar_referencia           Procedimiento para controlar la insercion de una referencia de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_rffd                type de la tabla de referencias SF_TRFFD
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_referencia (  p_rffd        sf_trffd%rowtype
                                   , p_ty_erro out ge_qtipo.tr_error
                                  );
    --
    /**
      * actualizar_referencia         Procedimiento para controlar la actualizacion de una referencia de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_rffd_old            Type de la tabla de referencias SF_TRFFD, datos antiguos.
      * @param  p_rffd_new            Type de la tabla de referencias SF_TRFFD, datos nuevos.
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_referencia (  p_rffd_old    sf_trffd%rowtype
                                     , p_rffd_new    sf_trffd%rowtype
                                     , p_ty_erro out ge_qtipo.tr_error
                                    );
    --
    procedure eliminar_referencia(  p_ty_rffd  in out sf_trffd%rowtype
                                  , p_ty_erro     out ge_qtipo.tr_error
                                 ) ;
    --
    /**
      * insertar_email                Procedimiento para controlar la insercion de un email de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_dcfd                Type de la tabla de emails SF_TDCFD
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_email (  p_dcfd        sf_tdcfd%rowtype
                              , p_ty_erro out ge_qtipo.tr_error
                             );
    --
    /**
      * actualizar_email              Procedimiento para controlar la actualizacion de un email de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_dcfd_old            Type de la tabla de emails SF_TDCFD, datos antiguos.
      * @param  p_dcfd_new            Type de la tabla de emails SF_TDCFD, datos nuevos.
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_email (  p_dcfd_old    sf_tdcfd%rowtype
                                , p_dcfd_new    sf_tdcfd%rowtype
                                , p_ty_erro out ge_qtipo.tr_error
                               );
    --
    procedure eliminar_email (  p_dcfd        sf_tdcfd%rowtype
                              , p_ty_erro out ge_qtipo.tr_error
                             );
    /**
      * insertar_apoderado            Procedimiento para controlar la insercion de un apoderado de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_apfd                Type de la tabla de apoderados SF_TAPFD
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_apoderado (  p_apfd        sf_tapfd%rowtype
                                  , p_ty_erro out ge_qtipo.tr_error
                                 );
    --
    /**
      * actualizar_apoderado          Procedimiento para controlar la actualizacion de un apoderado de un cliente, se insertar en la tabla SF_TDTPF
      *
      * @param  p_dcfd_old            Type de la tabla de apoderados SF_TAPFD, datos antiguos.
      * @param  p_dcfd_new            Type de la tabla de apoderados SF_TAPFD, datos nuevos.
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_apoderado (  p_apfd_old     sf_tapfd%rowtype
                                    , p_apfd_new     sf_tapfd%rowtype
                                    , p_ty_erro  out ge_qtipo.tr_error
                                   );
    --
    procedure eliminar_apoderado (    p_ty_apfd  in out sf_tapfd%rowtype
                                    , p_ty_erro     out ge_qtipo.tr_error
                                    );
    --
    /**
      * insertar_empresa_fidei        Procedimiento para controlar la insercion de una empresa donde labora un fideicomitente , se insertar en la tabla SF_TDTPF
      *
      * @param  p_emfd                type de la tabla de clientes SF_TEMFD
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_empresa_fidei (  p_emfd        sf_temfd%rowtype
                                      , p_ty_erro out ge_qtipo.tr_error
                                     );
    /**
      * actualizar_empresa_fidei      Procedimiento para controlar la insercion de una empresa donde labora un fideicomitente , se insertar en la tabla SF_TDTPF
      *
      * @param  p_emfd_new            type de la tabla de clientes SF_TEMFD
      * @param  p_emfd_old            type de la tabla de clientes SF_TEMFD
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_empresa_fidei (  p_emfd_new      sf_temfd%rowtype
                                        , p_emfd_old      sf_temfd%rowtype
                                        , p_ty_erro   out ge_qtipo.tr_error
                                        );
    procedure eliminar_empresa_fidei (  p_ty_emfd  in out sf_temfd%rowtype
                                      , p_ty_erro     out ge_qtipo.tr_error
                         );
    /**
      * insertar_persona_cargo        Procedimiento para controlar la insercion de empresas a cargo por fideicomitente , se insertar en la tabla SF_TDTPF
      *
      * @param  p_prcg                type de la tabla de clientes SF_TPRCG
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_persona_cargo (  p_prcg         sf_tprcg%rowtype
                                      , p_ty_erro  out ge_qtipo.tr_error
                                     );
    /**
      * actualizar_persona_cargo      Procedimiento para controlar personas a cargo por fideicomitente , se actualiza en la tabla SF_TDTPF
      *
      * @param  p_prcg_new            type de la tabla de clientes SF_TPRCG
      * @param  p_prcg_old            type de la tabla de clientes SF_TPRCG
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_persona_cargo (  p_prcg_new      sf_tprcg%rowtype
                                        , p_prcg_old      sf_tprcg%rowtype
                                        , p_ty_erro   out ge_qtipo.tr_error
                                       );
     --
    procedure eliminar_persona_cargo (  p_ty_prcg  in out sf_tprcg%rowtype
                                        , p_ty_erro     out ge_qtipo.tr_error
                                    );
     --
    /**
      * insertar_firmas_autorizadas   Procedimiento para controlar la insercion firmas autorizadas, se insertar en la tabla SF_TDTPF
      *
      * @param  p_frau                type de la tabla de clientes SF_TPRCG
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_firmas_autorizadas (  p_frau         sf_tfrau%rowtype
                                           , p_ty_erro  out ge_qtipo.tr_error
                                          );
    /**
      * actualizar_firmas_autorizadas Procedimiento para controlar la actualziacion firmas autorizadas, se insertar en la tabla SF_TDTPF
      *
      * @param  p_frau_new            type de la tabla de clientes sf_tfrau
      * @param  p_frau_old            type de la tabla de clientes sf_tfrau
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_firmas_autorizadas (  p_frau_new      sf_tfrau%rowtype
                                             , p_frau_old      sf_tfrau%rowtype
                                             , p_ty_erro   out ge_qtipo.tr_error
                                            );
     --
     procedure eliminar_firmas_autorizadas (      p_ty_frau  in out sf_tfrau%rowtype
                                    ,   p_ty_erro     out ge_qtipo.tr_error
                                    ) ;
     --
    /**
      * insertar_sucursales           Procedimiento para controlar la insercion sucursales, se insertar en la tabla SF_TDTPF
      *
      * @param  p_fsuc                type de la tabla de sucursales sf_tfsuc
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_sucursales(  p_fsuc         sf_tfsuc%rowtype
                                  , p_ty_erro  out ge_qtipo.tr_error
                                 );
    /**
      * actualizar_sucursales         Procedimiento para controlar la actualziacion de sucursales, se insertar en la tabla SF_TDTPF
      *
      * @param  p_fsuc_new            type de la tabla de sucursales sf_tfsuc
      * @param  p_fsuc_old            type de la tabla de sucursales sf_tfsuc
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_sucursales (  p_fsuc_new      sf_tfsuc%rowtype
                                     , p_fsuc_old      sf_tfsuc%rowtype
                                     , p_ty_erro   out ge_qtipo.tr_error
                                    );
     --
    procedure eliminar_sucursales (   p_ty_fsuc  in out sf_tfsuc%rowtype
                                    , p_ty_erro     out ge_qtipo.tr_error
                                   ) ;
     --
    /**
      * insertar_socios               Procedimiento para controlar la insercion socios, se insertar en la tabla SF_TDTPF
      *
      * @param  p_soci                type de la tabla de socios sf_tsoci
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_socios (  p_soci        sf_tsoci%rowtype
                               , p_ty_erro out ge_qtipo.tr_error
                              );
    /**
      * actualizar_socios             Procedimiento para controlar la actualziacion de socios, se insertar en la tabla SF_TDTPF
      *
      * @param  p_soci_new            type de la tabla de socios sf_tsoci
      * @param  p_soci_old            type de la tabla de socios sf_tsoci
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_socios (  p_soci_new      sf_tsoci%rowtype
                                 , p_soci_old      sf_tsoci%rowtype
                                 , p_ty_erro   out ge_qtipo.tr_error
                                );
    --
    procedure eliminar_socios (  p_ty_soci  in out sf_tsoci%rowtype
                                , p_ty_erro     out ge_qtipo.tr_error
                         ) ;
    --
    /**
      * insertar_administradores      Procedimiento para controlar la insercion administradores, se insertar en la tabla SF_TDTPF
      *
      * @param  p_admi                type de la tabla de socios sf_tadmi
      * @param  p_ty_erro             Type de error
      */
    procedure insertar_administradores (  p_admi         sf_tadmi%rowtype
                                        , p_ty_erro  out ge_qtipo.tr_error
                                       );
    /**
      * actualizar_administradores    Procedimiento para controlar la actualziacion de administradores, se insertar en la tabla SF_TDTPF
      *
      * @param  p_admi_new            type de la tabla de socios sf_tsoci
      * @param  p_admi_old            type de la tabla de socios sf_tsoci
      * @param  p_ty_erro             Type de error
      */
    procedure actualizar_administradores (  p_admi_new      sf_tadmi%rowtype
                                          , p_admi_old      sf_tadmi%rowtype
                                          , p_ty_erro   out ge_qtipo.tr_error
                                         );
    --
    procedure eliminar_administradores (  p_ty_admi  in out sf_tadmi%rowtype
                                    , p_ty_erro     out ge_qtipo.tr_error
                                    );
    --
    /**
      * obtiene_valor_nuevo           Funcion que retorna el valor nuevo de un campo de una tabla dada
      *
      * @param  p_prfd                Codigo del proceso
      * @param  p_tbla                Nombre de la Tabla
      * @param  p_cmpo                Campo de la Tabla
      * @return                       Valor nuevo del campo
      */
    function obtiene_valor_nuevo (  p_prfd    sf_tdtpf.dtpf_prfd%type
                                  , p_tbla    sf_tdtpf.dtpf_tbla%type
                                  , p_cmpo    sf_tdtpf.dtpf_cmpo%type
                                 ) return varchar2;
    --
    /**
      * obtiene_valor_antiguo         Funcion que retorna el valor antiguo de un campo de una tabla dada
      *
      * @param  p_prfd                Codigo del proceso
      * @param  p_tbla                Nombre de la Tabla
      * @param  p_cmpo                Campo de la Tabla
      * @return                       Valor antiguo del campo
      */
    function obtiene_valor_antiguo (  p_prfd    sf_tdtpf.dtpf_prfd%type
                                    , p_tbla    sf_tdtpf.dtpf_tbla%type
                                    , p_cmpo    sf_tdtpf.dtpf_cmpo%type
                                   ) return varchar2;
    --
    /**
      * nombre_cliente                Funcion que retorna el nombre del cliente a autorizar
      *
      * @param  p_prfd                Codigo del proceso
      * @return                       nombre del cliente
      */
    function nombre_cliente ( p_prfd  sf_tprfd.prfd_prfd%type ) return varchar2;
    --
    /**
      * set_origen                procedimiento  que ingresa el origen de la forma en que seran llamados triggers control dual
      *
      * @param p_origen           Origen del proceso
      */
    procedure set_origen ( p_origen  varchar2) ;
    --
    /**
      * get_origen                funcion  que obtiene el origen de la forma en que seran llamados triggers control dual
      *
      * @return                   Retorna el origen del llamado del procedimiento
      */
    function get_origen  return varchar2;
    --
    /**
      * set_auto                  procedimiento que establece la variable bandera para indicar que si se esta autorizando una operacion
      *
      * @param p_auto             Bandera de autorizacion. True o False.
      */
    procedure set_auto ( p_auto  boolean );
    --
    /**
      * get_auto                  funcion que retorna la variable bandera para indicar que si se esta autorizando una operacion
      *
      * @return                   Bandera de autorizacion. True o False.
      */
    function get_auto return boolean;
    --
    /**
      * set_proceso               procedimiento que guarda el numero del proceso en la variable global vg_prfd
      *
      * @param p_prfd             Codigo del proceso
      */
    procedure set_proceso ( p_prfd  sf_tprfd.prfd_prfd%type );
    --
    /**
      * get_proceso               funcion que retorna el numero del proceso almacenado en la variable global vg_prfd
      *
      * @return                   Numero del proceso
      */
    function get_proceso return number;
    --
     /**
      * get_fdei                  funcion que retorna el numero del fideicomitente asociado al proceso actual
      *
      * @return                   Numero del fideicomitente
      */
    function get_fdei return number;
    --
    /**
      * procesa_datos             procedimiento que procesa la informacion guardada en la tabla de control
      *
      */
    procedure procesa_datos;
    --
    /**
      * inserta_datos             Procedimiento que genera SQL dinamico para insertar informacion en las tablas asociadas al proceso
      *
      * @param  p_tbla            Nombre de la tabla
      * @param  p_idnu            Indice numerico asociado al registro
      * @param  p_idvc            Indice alfabetico asociado al registro
      */
    procedure inserta_datos (  p_tbla      sf_tdtpf.dtpf_tbla%type
                             , p_idnu      sf_tdtpf.dtpf_idnu%type
                             , p_idvc      sf_tdtpf.dtpf_idvc%type
                             );
    --
    /**
      * actualiza_datos           Procedimiento que genera SQL dinamico para actualizar informacion en las tablas asociadas al proceso
      *
      * @param  p_tbla            Nombre de la tabla
      * @param  p_idnu            Indice numerico asociado al registro
      * @param  p_idvc            Indice asociado al registro
      */
    procedure actualiza_datos (  p_tbla      sf_tdtpf.dtpf_tbla%type
                               , p_idnu      sf_tdtpf.dtpf_idnu%type
                               , p_idvc      sf_tdtpf.dtpf_idvc%type
                              );
    --
    /**
      * elimina_datos             Procedimiento que genera SQL dinamico para eliminar informacion en las tablas asociadas al proceso
      *
      * @param  p_tbla            Nombre de la tabla
      * @param  p_idnu            Indice numerico asociado al registro
      * @param  p_idvc            Indice asociado al registro
      */
    procedure elimina_datos (  p_tbla      sf_tdtpf.dtpf_tbla%type
                             , p_idnu      sf_tdtpf.dtpf_idnu%type
                             , p_idvc      sf_tdtpf.dtpf_idvc%type
                            );
    /**
      * conv_dato                 Funcion que retorna el valor ingresado formateado de acuerdo al tipo de dato de la columna asociada
      *
      * @param  pc_dato           Dato a covertir
      * @param  pc_tabla          Nombre de la Tabla
      * @param  pc_col            Nombre de la columna
      * @return                   Valor del dato formateado
      */
    function conv_dato ( pc_dato varchar2, pc_tabla varchar2, pc_col varchar2 ) return varchar2;
    --
    procedure actualiza_estado ( p_estado varchar2,
                                 p_observ varchar2);
    --
    procedure valida_proceso_pendiente(  p_fdei                 sf_tfdei.fdei_fdei%type
                                        ,p_prfd_prfd    out     sf_tprfd.prfd_prfd%type          
                                        ,p_ty_erro      out     ge_qtipo.tr_error       );
                                        
    procedure valida_registros_fdei (   p_fdei_old          in      sf_tfdei%rowtype
                                       ,p_fdei_new          in      sf_tfdei%rowtype
                                       ,p_num_reg           out     number
                                       ,p_ty_erro           out     ge_qtipo.tr_error);
    --
    function valida_tipo_dato(  p_table     varchar2
                               ,p_columna   varchar2) return varchar2 ;   
    -- declaracion
    procedure set_tppr( p_tppr  varchar2 );
    --
    function get_tppr  return varchar2; 
    --
    procedure insertar_cliente_auxil   (    p_auxi        ge_tauxil%rowtype
                                           ,p_ty_erro out ge_qtipo.tr_error
                                        );
    --
    procedure actualiza_cliente_auxil   (    p_auxil_old        ge_tauxil%rowtype
                                            ,p_auxil_new        ge_tauxil%rowtype
                                            ,p_ty_erro out ge_qtipo.tr_error
                                    ) ;
    -- ini 1007
    procedure inserta_temporal_date    (    p_campo       varchar2
                                           ,p_fecha       date
                                           ,p_ty_erro out ge_qtipo.tr_error
                                    ) ;
    --
    function consulta_temporal_date (     p_campo varchar2 ) return varchar2;   
    -- fin 1007
    vg_tppr   varchar2(1);                                   
    -- Variables globales de paquete
    vg_origen varchar2(10);
    vg_auto   boolean := false;
    vg_prfd   sf_tprfd.prfd_prfd%type;
    --
end SF_QAUTO_CLIE;
/

prompt
prompt package body SF_QAUTO_CLIE
prompt
create or replace package body SF_QAUTO_CLIE is
    --
    -- #VERSION:0000001007
    --
    procedure mapear_campos (  p_prfd        sf_tdtpf.dtpf_prfd%type
                             , p_tbla        sf_tdtpf.dtpf_tbla%type
                             , p_cmpo        sf_tdtpf.dtpf_cmpo%type
                             , p_dtor        sf_tdtpf.dtpf_dtor%type
                             , p_dtnv        sf_tdtpf.dtpf_dtnv%type
                             , p_fdei        sf_tdtpf.dtpf_fdei%type
                             , p_idnu        sf_tdtpf.dtpf_idnu%type
                             , p_idvc        sf_tdtpf.dtpf_idvc%type
                             , p_tptr        sf_tdtpf.dtpf_tptr%type DEFAULT 'I'
                            ) is
        --
        p_ty_dtpf   sf_qdtpf_crud.ty_dtpf;
        p_ty_erro   ge_qtipo.tr_error;
        --
    begin
        --
        if ( (nvl( p_dtor, 'XX-1X') <> nvl( p_dtnv,'XX-1X' ) and p_tbla = 'SF_TFDEI') or p_tbla <> 'SF_TFDEI' or (p_cmpo = 'FDEI_FDEI') or (p_cmpo = 'AUXI_AUXI')) then-- 1006
            --
            sf_pins_error('SF_QAUTO_CLIE.MAPEAR_CAMPOS. p_prfd='||p_prfd||' p_tbla='||p_tbla||' p_cmpo='||p_cmpo||' p_dtor='||p_dtor||' p_dtnv='||p_dtnv);
            --
            p_ty_dtpf.dtpf_prfd := p_prfd;
            p_ty_dtpf.dtpf_tbla := p_tbla;
            p_ty_dtpf.dtpf_cmpo := p_cmpo;
            p_ty_dtpf.dtpf_dtor := p_dtor;
            p_ty_dtpf.dtpf_dtnv := p_dtnv;
            p_ty_dtpf.dtpf_fdei := p_fdei;
            p_ty_dtpf.dtpf_idnu := p_idnu;
            p_ty_dtpf.dtpf_idvc := p_idvc;
            p_ty_dtpf.dtpf_tptr := p_tptr;
            --
            sf_qdtpf_crud.insertar_sf_tdtpf( p_ty_dtpf => p_ty_dtpf,
                                             p_ty_erro => p_ty_erro
                                            );
            --
            if ( p_ty_erro.cod_error <> 'OK' ) then
                --
                sf_pins_error('ERROR MAPEAR_CAMPOS='||p_ty_erro.msg_error );
                Raise_Application_Error( -20003, p_ty_erro.msg_error );
                --
            end if;
            --
        end if;
        --
    end mapear_campos;
    --
    function validar_proceso (  p_tpid   sf_tfdei.fdei_tpid%type
                              , p_fdei   sf_tfdei.fdei_fdei%type
                              , p_usua   sf_tprfd.prfd_usua%type default null
                             ) return number is
        --
        cursor c_prfd is
        select prfd_prfd
          from sf_tprfd
         where prfd_fdei = p_fdei
           and prfd_esta = 'I';
        --
        p_ty_prfd     sf_qprfd_crud.ty_prfd;
        p_ty_erro     ge_qtipo.tr_error;
        v_prfd        number;
        -- ini 1001
        cursor c_tpid is select fdei_tpid  from sf_tfdei 
                                        where fdei_fdei=p_fdei;
        -- fin 1001
    begin
        --
        open c_prfd;
        fetch c_prfd into v_prfd;
        close c_prfd;
        --
        
        if ( v_prfd is null ) then
            --

            p_ty_prfd.prfd_fdei  := p_fdei;
            p_ty_prfd.prfd_tpid  := p_tpid;
            p_ty_prfd.prfd_usua  := nvl(p_usua, user);
            p_ty_prfd.prfd_esta  := 'I';
            p_ty_prfd.prfd_feccre:= sysdate;
            p_ty_prfd.prfd_tppr  := sf_qauto_clie.get_tppr;
            --
            -- ini 1001
            if p_tpid is null then
                open    c_tpid;
                fetch   c_tpid into p_ty_prfd.prfd_tpid;
                close   c_tpid;
                    
            end if;
            -- ini 1001
            sf_qprfd_crud.insertar_sf_tprfd( p_ty_prfd => p_ty_prfd,
                                             p_ty_erro => p_ty_erro
                                            );
            if ( p_ty_erro.cod_error <> 'OK' ) then
                Raise_Application_Error( -20003, p_ty_erro.msg_error );
            end if;
            --
            return p_ty_prfd.prfd_prfd;
        else
            --
            return v_prfd;
            --
        end if;
        --
    end validar_proceso;
    --
    procedure insertar_cliente (  p_fdei        sf_tfdei%rowtype
                                , p_ty_erro out ge_qtipo.tr_error
                               ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        v_num_reg    number;
        --
    begin
        --

        --

        v_prfd := validar_proceso( p_fdei.fdei_tpid, p_fdei.fdei_fdei, p_fdei.fdei_usua );
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FDEI'           , null, p_fdei.fdei_fdei            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE'         , null, p_fdei.fdei_nombre          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_APELL1'         , null, p_fdei.fdei_apell1          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_APELL2'         , null, p_fdei.fdei_apell2          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_AUXI'           , null, p_fdei.fdei_auxi            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPID'           , null, p_fdei.fdei_tpid            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_EXP'       , null, p_fdei.fdei_ciud_exp        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECEXP'         , null, p_fdei.fdei_fecexp          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECNAC'         , null, p_fdei.fdei_fecnac          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_NAC'       , null, p_fdei.fdei_ciud_nac        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FISEXO'         , null, p_fdei.fdei_fisexo          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ESTCIV'         , null, p_fdei.fdei_estciv          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NIVEST'         , null, p_fdei.fdei_nivest          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PROF'           , null, p_fdei.fdei_prof            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EMAIL'          , null, p_fdei.fdei_email           , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ACARGO'         , null, p_fdei.fdei_acargo          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUMHIJ'         , null, p_fdei.fdei_numhij          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_DOM'       , null, p_fdei.fdei_ciud_dom        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_DIRE_DOM'       , null, p_fdei.fdei_dire_dom        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TELE_DOM'       , null, p_fdei.fdei_tele_dom        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NROFAX'         , null, p_fdei.fdei_nrofax          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CELULA'         , null, p_fdei.fdei_celula          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OCUPAC'         , null, p_fdei.fdei_ocupac          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PERSON'         , null, p_fdei.fdei_person          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUA'           , null, p_fdei.fdei_ciua            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_INGRES'         , null, p_fdei.fdei_ingres          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EGRESO'         , null, p_fdei.fdei_egreso          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_IMPUSA'         , null, p_fdei.fdei_impusa          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE_EMP'     , null, p_fdei.fdei_nombre_emp      , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NIT_EMP'        , null, p_fdei.fdei_nit_emp         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_DIRE_EMP'       , null, p_fdei.fdei_dire_emp        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TELE_EMP'       , null, p_fdei.fdei_tele_emp        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_EMP'       , null, p_fdei.fdei_ciud_emp        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUA_EMP'       , null, p_fdei.fdei_ciua_emp        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECVIN'         , null, p_fdei.fdei_fecvin          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CARGO'          , null, p_fdei.fdei_cargo           , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ORIG_RECU'      , null, p_fdei.fdei_orig_recu       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMB_REPLEG'    , null, p_fdei.fdei_nomb_repleg     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TELE_REPLEG'    , null, p_fdei.fdei_tele_repleg     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CELU_REPLEG'    , null, p_fdei.fdei_celu_repleg     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FAX_REPLEG'     , null, p_fdei.fdei_fax_repleg      , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EMAIL_REPLEG'   , null, p_fdei.fdei_email_repleg    , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUM_ESCRI'      , null, p_fdei.fdei_num_escri       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOTARIA'        , null, p_fdei.fdei_notaria         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_NOTARIA'   , null, p_fdei.fdei_ciud_notaria    , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ACTIVOS'        , null, p_fdei.fdei_activos         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PASIVOS'        , null, p_fdei.fdei_pasivos         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_MATRIC_MERC'    , null, p_fdei.fdei_matric_merc     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPO_EMPRESA'    , null, p_fdei.fdei_tpo_empresa     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_DIR_CORRESP'    , null, p_fdei.fdei_dir_corresp     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXENTO_CONESP'  , null, p_fdei.fdei_exento_conesp   , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SGMR'           , null, p_fdei.fdei_sgmr            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPCL'           , null, p_fdei.fdei_tpcl            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_STAT'           , null, p_fdei.fdei_stat            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECACT'         , null, p_fdei.fdei_fecact          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_REPLEG'    , null, p_fdei.fdei_ciud_repleg     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_DIRE_REPLEG'    , null, p_fdei.fdei_dire_repleg     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUCX'           , null, p_fdei.fdei_nucx            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_RETEF'          , null, p_fdei.fdei_retef           , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_GERE_PD'        , null, p_fdei.fdei_gere_pd         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_GERE_SG'        , null, p_fdei.fdei_gere_sg         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_RIN'            , null, p_fdei.fdei_rin             , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OFIC'           , null, p_fdei.fdei_ofic            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_QDIRE'          , null, p_fdei.fdei_qdire           , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPBN'           , null, p_fdei.fdei_tpbn            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXONERADO'      , null, p_fdei.fdei_exonerado       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE1'        , null, p_fdei.fdei_nombre1         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE2'        , null, p_fdei.fdei_nombre2         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EMAIL_EMP'      , null, p_fdei.fdei_email_emp       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_INGRES_OTROS'   , null, p_fdei.fdei_ingres_otros    , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CONCEPTO_OTIN'  , null, p_fdei.fdei_concepto_otin   , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_HOBBIE'         , null, p_fdei.fdei_hobbie          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CONO_EMP'       , null, p_fdei.fdei_cono_emp        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPDO_REPLEG'    , null, p_fdei.fdei_tpdo_repleg     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUDO_REPLEG'    , null, p_fdei.fdei_nudo_repleg     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE1_REPLEG' , null, p_fdei.fdei_nombre1_repleg  , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE2_REPLEG' , null, p_fdei.fdei_nombre2_repleg  , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_APELL1_REPLEG'  , null, p_fdei.fdei_apell1_repleg   , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_APELL2_REPLEG'  , null, p_fdei.fdei_apell2_repleg   , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SEXO_REPLEG'    , null, p_fdei.fdei_sexo_repleg     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OPIN'           , null, p_fdei.fdei_opin            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ENTI_OPIN'      , null, p_fdei.fdei_enti_opin       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUCU_OPIN'      , null, p_fdei.fdei_nucu_opin       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_MONE_OPIN'      , null, p_fdei.fdei_mone_opin       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OBSE_OPIN'      , null, p_fdei.fdei_obse_opin       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CLASE_TRIB'     , null, p_fdei.fdei_clase_trib      , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PAIS_OPIN'      , null, p_fdei.fdei_pais_opin       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_OPIN'      , null, p_fdei.fdei_ciud_opin       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECH_NAC_REPLEG', null, p_fdei.fdei_fech_nac_repleg , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_NAC_REPLEG', null, p_fdei.fdei_ciud_nac_repleg , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECCDAT'        , null, p_fdei.fdei_feccdat         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_USUACDAT'       , null, p_fdei.fdei_usuacdat        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUM_EMP'        , null, p_fdei.fdei_num_emp         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SOC_VIG_HASTA'  , null, p_fdei.fdei_soc_vig_hasta   , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ORRC'           , null, p_fdei.fdei_orrc            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXENTO'         , null, p_fdei.fdei_exento          , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_VERIF_SIP'      , null, p_fdei.fdei_verif_sip       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_USR_SIP'        , null, p_fdei.fdei_usr_sip         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECVER_SIP'     , null, p_fdei.fdei_fecver_sip      , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PROD'           , null, p_fdei.fdei_prod            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FEC_EXON'       , null, p_fdei.fdei_fec_exon        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXON'           , null, p_fdei.fdei_exon            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXON_OBSERV'    , null, p_fdei.fdei_exon_observ     , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SGCL'           , null, p_fdei.fdei_sgcl            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CANA'           , null, p_fdei.fdei_cana            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TERMINAL'       , null, p_fdei.fdei_terminal        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PAIS_NAC'       , null, p_fdei.fdei_pais_nac        , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OPME'           , null, p_fdei.fdei_opme            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPPR'           , null, p_fdei.fdei_tppr            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_MONTO_PROD'     , null, p_fdei.fdei_monto_prod      , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PAIS_NAC_REPLEG', null, p_fdei.fdei_pais_nac_repleg , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_EXP_REPLEG', null, p_fdei.fdei_ciud_exp_repleg , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECH_EXP_REPLEG', null, p_fdei.fdei_fech_exp_repleg , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SECT'           , null, p_fdei.fdei_sect            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXTR_ELECT'     , null, p_fdei.fdei_extr_elect      , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PAIS_CONS'      , null, p_fdei.fdei_pais_cons       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_GIIN'           , null, p_fdei.fdei_giin            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FITP'           , null, p_fdei.fdei_fitp            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SPONSOR'        , null, p_fdei.fdei_sponsor         , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_GIIN_SPONSOR'   , null, p_fdei.fdei_giin_sponsor    , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_AEXT_EMAIL'     , null, p_fdei.fdei_aext_email      , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_LNDS'           , null, p_fdei.fdei_lnds            , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PEP'            , null, p_fdei.fdei_pep             , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPCL_BANC'      , null, p_fdei.fdei_tpcl_banc       , null, null, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PEP_PJ'         , null, p_fdei.fdei_pep_pj          , null, null, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECCRE'         , null, p_fdei.fdei_feccre          , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_USUA'           , null, p_fdei.fdei_usua            , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_USUAMOD'        , null, p_fdei.fdei_usuamod         , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECMOD'         , null, p_fdei.fdei_fecmod          , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';

        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_CLIENTE.'||sqlerrm;
            --
    end insertar_cliente;
    --
    procedure actualizar_cliente (  p_fdei_old    sf_tfdei%rowtype
                                  , p_fdei_new    sf_tfdei%rowtype
                                  , p_ty_erro out ge_qtipo.tr_error
                                 ) is
        --
        v_prfd       number;
        v_num_reg number;

        
    begin
        --
        valida_registros_fdei (   p_fdei_old,p_fdei_new,v_num_reg,p_ty_erro);
        sf_pins_error('v_num_reg'||v_num_reg);
        if p_ty_erro.cod_error <> 'OK' then
            Raise_Application_Error( -20003, p_ty_erro.msg_error ); 
        end if;
        if v_num_reg > 0 then 
            sf_pins_error('SF_QAUTO_CLIE.ACTUALIZAR_CLIENTE  --> INICIO');
            v_prfd := validar_proceso( p_fdei_old.fdei_tpid, p_fdei_old.fdei_fdei, p_fdei_new.fdei_usua );
            --
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FDEI'           , p_fdei_old.fdei_fdei           , p_fdei_new.fdei_fdei            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE'         , p_fdei_old.fdei_nombre         , p_fdei_new.fdei_nombre          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_APELL1'         , p_fdei_old.fdei_apell1         , p_fdei_new.fdei_apell1          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_APELL2'         , p_fdei_old.fdei_apell2         , p_fdei_new.fdei_apell2          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_AUXI'           , p_fdei_old.fdei_auxi           , p_fdei_new.fdei_auxi            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPID'           , p_fdei_old.fdei_tpid           , p_fdei_new.fdei_tpid            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_EXP'       , p_fdei_old.fdei_ciud_exp       , p_fdei_new.fdei_ciud_exp        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECEXP'         , p_fdei_old.fdei_fecexp         , nvl(consulta_temporal_date( 'FDEI_FECEXP'),p_fdei_new.fdei_fecexp) , null, null, null, 'A' );--1007
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECNAC'         , p_fdei_old.fdei_fecnac         , nvl(consulta_temporal_date( 'FDEI_FECNAC'),p_fdei_new.fdei_fecnac), null, null, null, 'A' );--1007
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_NAC'       , p_fdei_old.fdei_ciud_nac       , p_fdei_new.fdei_ciud_nac        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FISEXO'         , p_fdei_old.fdei_fisexo         , p_fdei_new.fdei_fisexo          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ESTCIV'         , p_fdei_old.fdei_estciv         , p_fdei_new.fdei_estciv          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NIVEST'         , p_fdei_old.fdei_nivest         , p_fdei_new.fdei_nivest          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PROF'           , p_fdei_old.fdei_prof           , p_fdei_new.fdei_prof            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EMAIL'          , p_fdei_old.fdei_email          , p_fdei_new.fdei_email           , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ACARGO'         , p_fdei_old.fdei_acargo         , p_fdei_new.fdei_acargo          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUMHIJ'         , p_fdei_old.fdei_numhij         , p_fdei_new.fdei_numhij          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_DOM'       , p_fdei_old.fdei_ciud_dom       , p_fdei_new.fdei_ciud_dom        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_DIRE_DOM'       , p_fdei_old.fdei_dire_dom       , p_fdei_new.fdei_dire_dom        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TELE_DOM'       , p_fdei_old.fdei_tele_dom       , p_fdei_new.fdei_tele_dom        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NROFAX'         , p_fdei_old.fdei_nrofax         , p_fdei_new.fdei_nrofax          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CELULA'         , p_fdei_old.fdei_celula         , p_fdei_new.fdei_celula          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OCUPAC'         , p_fdei_old.fdei_ocupac         , p_fdei_new.fdei_ocupac          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PERSON'         , p_fdei_old.fdei_person         , p_fdei_new.fdei_person          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUA'           , p_fdei_old.fdei_ciua           , p_fdei_new.fdei_ciua            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_INGRES'         , p_fdei_old.fdei_ingres         , p_fdei_new.fdei_ingres          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EGRESO'         , p_fdei_old.fdei_egreso         , p_fdei_new.fdei_egreso          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_IMPUSA'         , p_fdei_old.fdei_impusa         , p_fdei_new.fdei_impusa          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE_EMP'     , p_fdei_old.fdei_nombre_emp     , p_fdei_new.fdei_nombre_emp      , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NIT_EMP'        , p_fdei_old.fdei_nit_emp        , p_fdei_new.fdei_nit_emp         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_DIRE_EMP'       , p_fdei_old.fdei_dire_emp       , p_fdei_new.fdei_dire_emp        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TELE_EMP'       , p_fdei_old.fdei_tele_emp       , p_fdei_new.fdei_tele_emp        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_EMP'       , p_fdei_old.fdei_ciud_emp       , p_fdei_new.fdei_ciud_emp        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUA_EMP'       , p_fdei_old.fdei_ciua_emp       , p_fdei_new.fdei_ciua_emp        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECVIN'         , p_fdei_old.fdei_fecvin         , p_fdei_new.fdei_fecvin          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CARGO'          , p_fdei_old.fdei_cargo          , p_fdei_new.fdei_cargo           , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ORIG_RECU'      , p_fdei_old.fdei_orig_recu      , p_fdei_new.fdei_orig_recu       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMB_REPLEG'    , p_fdei_old.fdei_nomb_repleg    , p_fdei_new.fdei_nomb_repleg     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TELE_REPLEG'    , p_fdei_old.fdei_tele_repleg    , p_fdei_new.fdei_tele_repleg     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CELU_REPLEG'    , p_fdei_old.fdei_celu_repleg    , p_fdei_new.fdei_celu_repleg     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FAX_REPLEG'     , p_fdei_old.fdei_fax_repleg     , p_fdei_new.fdei_fax_repleg      , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EMAIL_REPLEG'   , p_fdei_old.fdei_email_repleg   , p_fdei_new.fdei_email_repleg    , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUM_ESCRI'      , p_fdei_old.fdei_num_escri      , p_fdei_new.fdei_num_escri       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOTARIA'        , p_fdei_old.fdei_notaria        , p_fdei_new.fdei_notaria         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_NOTARIA'   , p_fdei_old.fdei_ciud_notaria   , p_fdei_new.fdei_ciud_notaria    , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ACTIVOS'        , p_fdei_old.fdei_activos        , p_fdei_new.fdei_activos         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PASIVOS'        , p_fdei_old.fdei_pasivos        , p_fdei_new.fdei_pasivos         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_MATRIC_MERC'    , p_fdei_old.fdei_matric_merc    , p_fdei_new.fdei_matric_merc     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPO_EMPRESA'    , p_fdei_old.fdei_tpo_empresa    , p_fdei_new.fdei_tpo_empresa     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_DIR_CORRESP'    , p_fdei_old.fdei_dir_corresp    , p_fdei_new.fdei_dir_corresp     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXENTO_CONESP'  , p_fdei_old.fdei_exento_conesp  , p_fdei_new.fdei_exento_conesp   , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SGMR'           , p_fdei_old.fdei_sgmr           , p_fdei_new.fdei_sgmr            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPCL'           , p_fdei_old.fdei_tpcl           , p_fdei_new.fdei_tpcl            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_STAT'           , p_fdei_old.fdei_stat           , p_fdei_new.fdei_stat            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECACT'         , p_fdei_old.fdei_fecact         , p_fdei_new.fdei_fecact          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_REPLEG'    , p_fdei_old.fdei_ciud_repleg    , p_fdei_new.fdei_ciud_repleg     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_DIRE_REPLEG'    , p_fdei_old.fdei_dire_repleg    , p_fdei_new.fdei_dire_repleg     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUCX'           , p_fdei_old.fdei_nucx           , p_fdei_new.fdei_nucx            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_RETEF'          , p_fdei_old.fdei_retef          , p_fdei_new.fdei_retef           , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_GERE_PD'        , p_fdei_old.fdei_gere_pd        , p_fdei_new.fdei_gere_pd         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_GERE_SG'        , p_fdei_old.fdei_gere_sg        , p_fdei_new.fdei_gere_sg         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_RIN'            , p_fdei_old.fdei_rin            , p_fdei_new.fdei_rin             , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OFIC'           , p_fdei_old.fdei_ofic           , p_fdei_new.fdei_ofic            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_QDIRE'          , p_fdei_old.fdei_qdire          , p_fdei_new.fdei_qdire           , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPBN'           , p_fdei_old.fdei_tpbn           , p_fdei_new.fdei_tpbn            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXONERADO'      , p_fdei_old.fdei_exonerado      , p_fdei_new.fdei_exonerado       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE1'        , p_fdei_old.fdei_nombre1        , p_fdei_new.fdei_nombre1         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE2'        , p_fdei_old.fdei_nombre2        , p_fdei_new.fdei_nombre2         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EMAIL_EMP'      , p_fdei_old.fdei_email_emp      , p_fdei_new.fdei_email_emp       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_INGRES_OTROS'   , p_fdei_old.fdei_ingres_otros   , p_fdei_new.fdei_ingres_otros    , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CONCEPTO_OTIN'  , p_fdei_old.fdei_concepto_otin  , p_fdei_new.fdei_concepto_otin   , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_HOBBIE'         , p_fdei_old.fdei_hobbie         , p_fdei_new.fdei_hobbie          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CONO_EMP'       , p_fdei_old.fdei_cono_emp       , p_fdei_new.fdei_cono_emp        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPDO_REPLEG'    , p_fdei_old.fdei_tpdo_repleg    , p_fdei_new.fdei_tpdo_repleg     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUDO_REPLEG'    , p_fdei_old.fdei_nudo_repleg    , p_fdei_new.fdei_nudo_repleg     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE1_REPLEG' , p_fdei_old.fdei_nombre1_repleg , p_fdei_new.fdei_nombre1_repleg  , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NOMBRE2_REPLEG' , p_fdei_old.fdei_nombre2_repleg , p_fdei_new.fdei_nombre2_repleg  , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_APELL1_REPLEG'  , p_fdei_old.fdei_apell1_repleg  , p_fdei_new.fdei_apell1_repleg   , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_APELL2_REPLEG'  , p_fdei_old.fdei_apell2_repleg  , p_fdei_new.fdei_apell2_repleg   , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SEXO_REPLEG'    , p_fdei_old.fdei_sexo_repleg    , p_fdei_new.fdei_sexo_repleg     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OPIN'           , p_fdei_old.fdei_opin           , p_fdei_new.fdei_opin            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ENTI_OPIN'      , p_fdei_old.fdei_enti_opin      , p_fdei_new.fdei_enti_opin       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUCU_OPIN'      , p_fdei_old.fdei_nucu_opin      , p_fdei_new.fdei_nucu_opin       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_MONE_OPIN'      , p_fdei_old.fdei_mone_opin      , p_fdei_new.fdei_mone_opin       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OBSE_OPIN'      , p_fdei_old.fdei_obse_opin      , p_fdei_new.fdei_obse_opin       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CLASE_TRIB'     , p_fdei_old.fdei_clase_trib     , p_fdei_new.fdei_clase_trib      , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PAIS_OPIN'      , p_fdei_old.fdei_pais_opin      , p_fdei_new.fdei_pais_opin       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_OPIN'      , p_fdei_old.fdei_ciud_opin      , p_fdei_new.fdei_ciud_opin       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECH_NAC_REPLEG', p_fdei_old.fdei_fech_nac_repleg, p_fdei_new.fdei_fech_nac_repleg , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_NAC_REPLEG', p_fdei_old.fdei_ciud_nac_repleg, p_fdei_new.fdei_ciud_nac_repleg , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECCDAT'        , p_fdei_old.fdei_feccdat        , p_fdei_new.fdei_feccdat         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_USUACDAT'       , p_fdei_old.fdei_usuacdat       , p_fdei_new.fdei_usuacdat        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_NUM_EMP'        , p_fdei_old.fdei_num_emp        , p_fdei_new.fdei_num_emp         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SOC_VIG_HASTA'  , p_fdei_old.fdei_soc_vig_hasta  , p_fdei_new.fdei_soc_vig_hasta   , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_ORRC'           , p_fdei_old.fdei_orrc           , p_fdei_new.fdei_orrc            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXENTO'         , p_fdei_old.fdei_exento         , p_fdei_new.fdei_exento          , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_VERIF_SIP'      , p_fdei_old.fdei_verif_sip      , p_fdei_new.fdei_verif_sip       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_USR_SIP'        , p_fdei_old.fdei_usr_sip        , p_fdei_new.fdei_usr_sip         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECVER_SIP'     , p_fdei_old.fdei_fecver_sip     , p_fdei_new.fdei_fecver_sip      , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PROD'           , p_fdei_old.fdei_prod           , p_fdei_new.fdei_prod            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FEC_EXON'       , p_fdei_old.fdei_fec_exon       , p_fdei_new.fdei_fec_exon        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXON'           , p_fdei_old.fdei_exon           , p_fdei_new.fdei_exon            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXON_OBSERV'    , p_fdei_old.fdei_exon_observ    , p_fdei_new.fdei_exon_observ     , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SGCL'           , p_fdei_old.fdei_sgcl           , p_fdei_new.fdei_sgcl            , null, null, null, 'A' );
           -- sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CANA'           , p_fdei_old.fdei_cana           , p_fdei_new.fdei_cana            , null, null, null, 'A' );
            --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TERMINAL'       , p_fdei_old.fdei_terminal       , p_fdei_new.fdei_terminal        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PAIS_NAC'       , p_fdei_old.fdei_pais_nac       , p_fdei_new.fdei_pais_nac        , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_OPME'           , p_fdei_old.fdei_opme           , p_fdei_new.fdei_opme            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPPR'           , p_fdei_old.fdei_tppr           , p_fdei_new.fdei_tppr            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_MONTO_PROD'     , p_fdei_old.fdei_monto_prod     , p_fdei_new.fdei_monto_prod      , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PAIS_NAC_REPLEG', p_fdei_old.fdei_pais_nac_repleg, p_fdei_new.fdei_pais_nac_repleg , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_CIUD_EXP_REPLEG', p_fdei_old.fdei_ciud_exp_repleg, p_fdei_new.fdei_ciud_exp_repleg , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECH_EXP_REPLEG', p_fdei_old.fdei_fech_exp_repleg, p_fdei_new.fdei_fech_exp_repleg , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SECT'           , p_fdei_old.fdei_sect           , p_fdei_new.fdei_sect            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_EXTR_ELECT'     , p_fdei_old.fdei_extr_elect     , p_fdei_new.fdei_extr_elect      , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PAIS_CONS'      , p_fdei_old.fdei_pais_cons      , p_fdei_new.fdei_pais_cons       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_GIIN'           , p_fdei_old.fdei_giin           , p_fdei_new.fdei_giin            , null, null, null, 'A' );
          --  sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FITP'           , p_fdei_old.fdei_fitp           , p_fdei_new.fdei_fitp            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_SPONSOR'        , p_fdei_old.fdei_sponsor        , p_fdei_new.fdei_sponsor         , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_GIIN_SPONSOR'   , p_fdei_old.fdei_giin_sponsor   , p_fdei_new.fdei_giin_sponsor    , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_AEXT_EMAIL'     , p_fdei_old.fdei_aext_email     , p_fdei_new.fdei_aext_email      , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_LNDS'           , p_fdei_old.fdei_lnds           , p_fdei_new.fdei_lnds            , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PEP'            , p_fdei_old.fdei_pep            , p_fdei_new.fdei_pep             , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_TPCL_BANC'      , p_fdei_old.fdei_tpcl_banc      , p_fdei_new.fdei_tpcl_banc       , null, null, null, 'A' );
            sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_PEP_PJ'         , p_fdei_old.fdei_pep_pj         , p_fdei_new.fdei_pep_pj          , null, null, null, 'A' );
            --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECCRE'         , p_fdei_old.fdei_feccre         , p_fdei_new.fdei_feccre          , null, null, null );
            --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_USUA'           , p_fdei_old.fdei_usua           , p_fdei_new.fdei_usua            , null, null, null );
            --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_USUAMOD'        , p_fdei_old.fdei_usuamod        , p_fdei_new.fdei_usuamod         , null, null, null );
            --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFDEI', 'FDEI_FECMOD'         , p_fdei_old.fdei_fecmod         , p_fdei_new.fdei_fecmod          , null, null, null );
            --
            sf_pins_error('SF_QAUTO_CLIE.ACTUALIZAR_CLIENTE  --> FIN');
            --
            --
            p_ty_erro.cod_error := 'OK';
            p_ty_erro.msg_error := 'Mapeo Correcto';
            --
        end if;
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_CLIENTE.'||sqlerrm;
            --
    end actualizar_cliente;
    --
    procedure insertar_direccion (  p_drcl        sf_tdrcl%rowtype
                                  , p_ty_erro out ge_qtipo.tr_error
                                 ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_drcl.drcl_fdei );
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_FDEI'     , null, p_drcl.drcl_fdei     , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_TPDR'     , null, p_drcl.drcl_tpdr     , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_DIRE'     , null, p_drcl.drcl_dire     , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_CIUD'     , null, p_drcl.drcl_ciud     , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_TELE'     , null, p_drcl.drcl_tele     , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_EXT_TELE' , null, p_drcl.drcl_ext_tele , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_NROFAX'   , null, p_drcl.drcl_nrofax   , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_CELULAR'  , null, p_drcl.drcl_celular  , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_DRCO'     , null, p_drcl.drcl_drco     , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_PAIS'     , null, p_drcl.drcl_pais     , p_drcl.drcl_fdei, p_drcl.drcl_tpdr, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_USUA'     , null, p_drcl.drcl_usua     , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_FECCRE'   , null, p_drcl.drcl_feccre   , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_DIRECCION.'||sqlerrm;
            --
    end insertar_direccion;
    --
    procedure actualizar_direccion (  p_drcl_old    sf_tdrcl%rowtype
                                    , p_drcl_new    sf_tdrcl%rowtype
                                    , p_ty_erro out ge_qtipo.tr_error
                                   ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_drcl_old.drcl_fdei,p_drcl_new.drcl_usua );--1003
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_FDEI'     , p_drcl_old.drcl_fdei     , p_drcl_new.drcl_fdei     , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_TPDR'     , p_drcl_old.drcl_tpdr     , p_drcl_new.drcl_tpdr     , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_DIRE'     , p_drcl_old.drcl_dire     , p_drcl_new.drcl_dire     , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_CIUD'     , p_drcl_old.drcl_ciud     , p_drcl_new.drcl_ciud     , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_TELE'     , p_drcl_old.drcl_tele     , p_drcl_new.drcl_tele     , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_EXT_TELE' , p_drcl_old.drcl_ext_tele , p_drcl_new.drcl_ext_tele , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_NROFAX'   , p_drcl_old.drcl_nrofax   , p_drcl_new.drcl_nrofax   , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_CELULAR'  , p_drcl_old.drcl_celular  , p_drcl_new.drcl_celular  , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_DRCO'     , p_drcl_old.drcl_drco     , p_drcl_new.drcl_drco     , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_PAIS'     , p_drcl_old.drcl_pais     , p_drcl_new.drcl_pais     , p_drcl_old.drcl_fdei, p_drcl_old.drcl_tpdr, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_USUA'     , p_drcl_old.drcl_usua     , p_drcl_new.drcl_usua     , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_FECCRE'   , p_drcl_old.drcl_feccre   , p_drcl_new.drcl_feccre   , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_DIRECCION.'||sqlerrm;
            --
    end actualizar_direccion;
    --
    procedure eliminar_direccion (  p_ty_drcl  in out sf_tdrcl%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_drcl.drcl_fdei )  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_FDEI'    , p_ty_drcl.drcl_fdei, null     , null, null, null ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDRCL', 'DRCL_TPDR'    , p_ty_drcl.drcl_tpdr, null     , null, null, null ,'E');
        sf_pins_error('SF_QAUTO_CLIE.ELIMINA_EMAIL --> FIN');                                                                                          
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_sf_tdrcl.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_direccion; 
    --
    procedure insertar_contacto (  p_cnfd        sf_tcnfd%rowtype
                                 , p_ty_erro out ge_qtipo.tr_error
                                ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_cnfd.cnfd_fdei);
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_FDEI'   , null, p_cnfd.cnfd_fdei   , p_cnfd.cnfd_fdei, p_cnfd.cnfd_cnfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_CNFD'   , null, p_cnfd.cnfd_cnfd   , p_cnfd.cnfd_fdei, p_cnfd.cnfd_cnfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_DESCRI' , null, p_cnfd.cnfd_descri , p_cnfd.cnfd_fdei, p_cnfd.cnfd_cnfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_DIRE'   , null, p_cnfd.cnfd_dire   , p_cnfd.cnfd_fdei, p_cnfd.cnfd_cnfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_CIUD'   , null, p_cnfd.cnfd_ciud   , p_cnfd.cnfd_fdei, p_cnfd.cnfd_cnfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_TELE'   , null, p_cnfd.cnfd_tele   , p_cnfd.cnfd_fdei, p_cnfd.cnfd_cnfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_CELU'   , null, p_cnfd.cnfd_celu   , p_cnfd.cnfd_fdei, p_cnfd.cnfd_cnfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_FAX'    , null, p_cnfd.cnfd_fax    , p_cnfd.cnfd_fdei, p_cnfd.cnfd_cnfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_EMAIL'  , null, p_cnfd.cnfd_email  , p_cnfd.cnfd_fdei, p_cnfd.cnfd_cnfd, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_FECCRE' , null, p_cnfd.cnfd_feccre , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_USUA'   , null, p_cnfd.cnfd_usua   , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_CONTACTO.'||sqlerrm;
            --
    end insertar_contacto;
    --
    procedure actualizar_contacto (  p_cnfd_old    sf_tcnfd%rowtype
                                   , p_cnfd_new    sf_tcnfd%rowtype
                                   , p_ty_erro out ge_qtipo.tr_error
                                  ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_cnfd_old.cnfd_fdei, p_cnfd_new.cnfd_usua);-- 1003
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_FDEI'    , p_cnfd_old.cnfd_fdei   , p_cnfd_new.cnfd_fdei   , p_cnfd_old.cnfd_fdei, p_cnfd_old.cnfd_cnfd, null, 'A' ); -- 1004
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_CNFD'    , p_cnfd_old.cnfd_cnfd   , p_cnfd_new.cnfd_cnfd   , p_cnfd_old.cnfd_fdei, p_cnfd_old.cnfd_cnfd, null, 'A' ); -- 1004
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_DESCRI'  , p_cnfd_old.cnfd_descri , p_cnfd_new.cnfd_descri , p_cnfd_old.cnfd_fdei, p_cnfd_old.cnfd_cnfd, null, 'A' ); -- 1004
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_DIRE'    , p_cnfd_old.cnfd_dire   , p_cnfd_new.cnfd_dire   , p_cnfd_old.cnfd_fdei, p_cnfd_old.cnfd_cnfd, null, 'A' ); -- 1004
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_CIUD'    , p_cnfd_old.cnfd_ciud   , p_cnfd_new.cnfd_ciud   , p_cnfd_old.cnfd_fdei, p_cnfd_old.cnfd_cnfd, null, 'A' ); -- 1004
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_TELE'    , p_cnfd_old.cnfd_tele   , p_cnfd_new.cnfd_tele   , p_cnfd_old.cnfd_fdei, p_cnfd_old.cnfd_cnfd, null, 'A' ); -- 1004
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_CELU'    , p_cnfd_old.cnfd_celu   , p_cnfd_new.cnfd_celu   , p_cnfd_old.cnfd_fdei, p_cnfd_old.cnfd_cnfd, null, 'A' ); -- 1004
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_FAX'     , p_cnfd_old.cnfd_fax    , p_cnfd_new.cnfd_fax    , p_cnfd_old.cnfd_fdei, p_cnfd_old.cnfd_cnfd, null, 'A' ); -- 1004
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_EMAIL'   , p_cnfd_old.cnfd_email  , p_cnfd_new.cnfd_email  , p_cnfd_old.cnfd_fdei, p_cnfd_old.cnfd_cnfd, null, 'A' ); -- 1004
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_FECCRE'  , p_cnfd_old.cnfd_feccre , p_cnfd_new.cnfd_feccre , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_USUA'    , p_cnfd_old.cnfd_usua   , p_cnfd_new.cnfd_usua   , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_CONTACTO.'||sqlerrm;
            --
    end actualizar_contacto;
    --
    procedure eliminar_contacto (  p_ty_cnfd  in out sf_tcnfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_cnfd.cnfd_fdei)  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_FDEI'    , p_ty_cnfd.cnfd_fdei, null     , null, null, null ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TCNFD', 'CNFD_CNFD'    , p_ty_cnfd.cnfd_cnfd, null     , null, null, null ,'E');
        sf_pins_error('SF_QAUTO_CLIE.ELIMINA_EMAIL --> FIN');                                                                                          
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_contacto.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_contacto; 
    --
    procedure insertar_referencia (  p_rffd        sf_trffd%rowtype
                                   , p_ty_erro out ge_qtipo.tr_error
                                  ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_rffd.rffd_fdei );
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_FDEI'          , null, p_rffd.rffd_fdei          , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_RFFD'          , null, p_rffd.rffd_rffd          , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_TIPO'          , null, p_rffd.rffd_tipo          , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_DIRE'          , null, p_rffd.rffd_dire          , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_DESCRI'        , null, p_rffd.rffd_descri        , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_CIUD'          , null, p_rffd.rffd_ciud          , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_TELE'          , null, p_rffd.rffd_tele          , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_CUENTA'        , null, p_rffd.rffd_cuenta        , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_OBSERVA'       , null, p_rffd.rffd_observa       , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_SUCU_AVAL_REF' , null, p_rffd.rffd_sucu_aval_ref , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_TPBA'          , null, p_rffd.rffd_tpba          , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_NUCX'          , null, p_rffd.rffd_nucx          , p_rffd.rffd_fdei, p_rffd.rffd_rffd, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_FECCRE'        , null, p_rffd.rffd_feccre        , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_USUA'          , null, p_rffd.rffd_usua          , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_REFERENCIA.'||sqlerrm;
            --
    end insertar_referencia;
    --
    procedure actualizar_referencia (  p_rffd_old    sf_trffd%rowtype
                                     , p_rffd_new    sf_trffd%rowtype
                                     , p_ty_erro out ge_qtipo.tr_error
                                    ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_rffd_old.rffd_fdei, p_rffd_new.rffd_usua);--1003
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_FDEI'          , p_rffd_old.rffd_fdei          , p_rffd_new.rffd_fdei          , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_RFFD'          , p_rffd_old.rffd_rffd          , p_rffd_new.rffd_rffd          , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_TIPO'          , p_rffd_old.rffd_tipo          , p_rffd_new.rffd_tipo          , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_DIRE'          , p_rffd_old.rffd_dire          , p_rffd_new.rffd_dire          , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_DESCRI'        , p_rffd_old.rffd_descri        , p_rffd_new.rffd_descri        , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_CIUD'          , p_rffd_old.rffd_ciud          , p_rffd_new.rffd_ciud          , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_TELE'          , p_rffd_old.rffd_tele          , p_rffd_new.rffd_tele          , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_CUENTA'        , p_rffd_old.rffd_cuenta        , p_rffd_new.rffd_cuenta        , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_OBSERVA'       , p_rffd_old.rffd_observa       , p_rffd_new.rffd_observa       , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_SUCU_AVAL_REF' , p_rffd_old.rffd_sucu_aval_ref , p_rffd_new.rffd_sucu_aval_ref , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_TPBA'          , p_rffd_old.rffd_tpba          , p_rffd_new.rffd_tpba          , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_NUCX'          , p_rffd_old.rffd_nucx          , p_rffd_new.rffd_nucx          , p_rffd_old.rffd_fdei, p_rffd_old.rffd_rffd, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_FECCRE'        , p_rffd_old.rffd_feccre        , p_rffd_new.rffd_feccre        , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_USUA'          , p_rffd_old.rffd_usua          , p_rffd_new.rffd_usua          , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_REFERENCIA.'||sqlerrm;
            --
    end actualizar_referencia;
    --
        procedure eliminar_referencia(  p_ty_rffd  in out sf_trffd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_rffd.rffd_fdei )  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_FDEI'    , p_ty_rffd.rffd_fdei, null     , null, null, null ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TRFFD', 'RFFD_RFFD'    , p_ty_rffd.rffd_rffd, null     , null, null, null ,'E');
                                                                                         
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_referencia.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_referencia;   
    --
    procedure insertar_email (  p_dcfd        sf_tdcfd%rowtype
                              , p_ty_erro out ge_qtipo.tr_error
                             ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        sf_pins_error('SF_QAUTO_CLIE.INSERTAR_EMAIL --> INI');
        v_prfd := validar_proceso( null, p_dcfd.dcfd_fdei );
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_FDEI'     , null, p_dcfd.dcfd_fdei     , p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_EMAIL'    , null, p_dcfd.dcfd_email    , p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_TIPO'     , null, p_dcfd.dcfd_tipo     , p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_DRCO'     , null, p_dcfd.dcfd_drco     , p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_USUA'     , null, p_dcfd.dcfd_usua     , p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_FECCRE'   , null, p_dcfd.dcfd_feccre   , p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_USUAMOD'  , null, p_dcfd.dcfd_usuamod  , p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_FECMOD'   , null, p_dcfd.dcfd_fecmod   , p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email );
        sf_pins_error('SF_QAUTO_CLIE.INSERTAR_EMAIL --> FIN');
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_EMAIL.'||sqlerrm;
            --
    end insertar_email;
    --
    procedure actualizar_email (  p_dcfd_old    sf_tdcfd%rowtype
                                , p_dcfd_new    sf_tdcfd%rowtype
                                , p_ty_erro out ge_qtipo.tr_error
                               ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_dcfd_old.dcfd_fdei,p_dcfd_new.dcfd_usua );--1003
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_FDEI'     , p_dcfd_old.dcfd_fdei    , p_dcfd_new.dcfd_fdei     , p_dcfd_old.dcfd_fdei, null, p_dcfd_old.dcfd_email, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_EMAIL'    , p_dcfd_old.dcfd_email   , p_dcfd_new.dcfd_email    , p_dcfd_old.dcfd_fdei, null, p_dcfd_old.dcfd_email, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_TIPO'     , p_dcfd_old.dcfd_tipo    , p_dcfd_new.dcfd_tipo     , p_dcfd_old.dcfd_fdei, null, p_dcfd_old.dcfd_email, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_DRCO'     , p_dcfd_old.dcfd_drco    , p_dcfd_new.dcfd_drco     , p_dcfd_old.dcfd_fdei, null, p_dcfd_old.dcfd_email, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_USUA'     , p_dcfd_old.dcfd_usua    , p_dcfd_new.dcfd_usua     , p_dcfd_old.dcfd_fdei, null, p_dcfd_old.dcfd_email );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_FECCRE'   , p_dcfd_old.dcfd_feccre  , p_dcfd_new.dcfd_feccre   , p_dcfd_old.dcfd_fdei, null, p_dcfd_old.dcfd_email );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_USUAMOD'  , p_dcfd_old.dcfd_usuamod , p_dcfd_new.dcfd_usuamod  , p_dcfd_old.dcfd_fdei, null, p_dcfd_old.dcfd_email );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_FECMOD'   , p_dcfd_old.dcfd_fecmod  , p_dcfd_new.dcfd_fecmod   , p_dcfd_old.dcfd_fdei, null, p_dcfd_old.dcfd_email );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_EMAIL.'||sqlerrm;
            --
    end actualizar_email;
    --
    procedure eliminar_email (  p_dcfd        sf_tdcfd%rowtype
                              , p_ty_erro out ge_qtipo.tr_error
                             ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        sf_pins_error('SF_QAUTO_CLIE.eliminar_email --> INI');
        v_prfd := validar_proceso( null, p_dcfd.dcfd_fdei);
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_FDEI'     , p_dcfd.dcfd_fdei  , null, p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TDCFD', 'DCFD_EMAIL'    , p_dcfd.dcfd_email , null, p_dcfd.dcfd_fdei, null, p_dcfd.dcfd_email ,'E');
        sf_pins_error('SF_QAUTO_CLIE.ELIMINA_EMAIL --> FIN');
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_email.'||sqlerrm;
            --
    end eliminar_email;
    --
    procedure insertar_apoderado (  p_apfd        sf_tapfd%rowtype
                                  , p_ty_erro out ge_qtipo.tr_error
                                 ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_apfd.apfd_fdei_rela );
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FDEI'      , null, p_apfd.apfd_fdei      , p_apfd.apfd_fdei, p_apfd.apfd_fdei_rela, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FDEI_RELA' , null, p_apfd.apfd_fdei_rela , p_apfd.apfd_fdei, p_apfd.apfd_fdei_rela, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_DESC_RELA' , null, p_apfd.apfd_desc_rela , p_apfd.apfd_fdei, p_apfd.apfd_fdei_rela, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_ESTA_ASOC' , null, p_apfd.apfd_esta_asoc , p_apfd.apfd_fdei, p_apfd.apfd_fdei_rela, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FECH_INAC' , null, p_apfd.apfd_fech_inac , p_apfd.apfd_fdei, p_apfd.apfd_fdei_rela, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_RADI'      , null, p_apfd.apfd_radi      , p_apfd.apfd_fdei, p_apfd.apfd_fdei_rela, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_PAIS'      , null, p_apfd.apfd_pais      , p_apfd.apfd_fdei, p_apfd.apfd_fdei_rela, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_USUA'      , null, p_apfd.apfd_usua      , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FECCRE'    , null, p_apfd.apfd_feccre    , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_USUAMOD'   , null, p_apfd.apfd_usuamod   , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FECMOD'    , null, p_apfd.apfd_fecmod    , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_APODERADO.'||sqlerrm;
            --
    end insertar_apoderado;
    --
    procedure actualizar_apoderado (  p_apfd_old     sf_tapfd%rowtype
                                    , p_apfd_new     sf_tapfd%rowtype
                                    , p_ty_erro  out ge_qtipo.tr_error
                                   ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_apfd_old.apfd_fdei,p_apfd_new.apfd_usua );--1003
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FDEI'      , p_apfd_old.apfd_fdei      , p_apfd_new.apfd_fdei      , p_apfd_old.apfd_fdei, p_apfd_old.apfd_fdei_rela, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FDEI_RELA' , p_apfd_old.apfd_fdei_rela , p_apfd_new.apfd_fdei_rela , p_apfd_old.apfd_fdei, p_apfd_old.apfd_fdei_rela, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_DESC_RELA' , p_apfd_old.apfd_desc_rela , p_apfd_new.apfd_desc_rela , p_apfd_old.apfd_fdei, p_apfd_old.apfd_fdei_rela, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_ESTA_ASOC' , p_apfd_old.apfd_esta_asoc , p_apfd_new.apfd_esta_asoc , p_apfd_old.apfd_fdei, p_apfd_old.apfd_fdei_rela, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FECH_INAC' , p_apfd_old.apfd_fech_inac , p_apfd_new.apfd_fech_inac , p_apfd_old.apfd_fdei, p_apfd_old.apfd_fdei_rela, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_RADI'      , p_apfd_old.apfd_radi      , p_apfd_new.apfd_radi      , p_apfd_old.apfd_fdei, p_apfd_old.apfd_fdei_rela, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_PAIS'      , p_apfd_old.apfd_pais      , p_apfd_new.apfd_pais      , p_apfd_old.apfd_fdei, p_apfd_old.apfd_fdei_rela, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_USUA'      , p_apfd_old.apfd_usua      , p_apfd_new.apfd_usua      , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FECCRE'    , p_apfd_old.apfd_feccre    , p_apfd_new.apfd_feccre    , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_USUAMOD'   , p_apfd_old.apfd_usuamod   , p_apfd_new.apfd_usuamod   , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FECMOD'    , p_apfd_old.apfd_fecmod    , p_apfd_new.apfd_fecmod    , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_APODERADO.'||sqlerrm;
            --
    end actualizar_apoderado;
    --
    procedure eliminar_apoderado (  p_ty_apfd  in out sf_tapfd%rowtype
                                  , p_ty_erro     out ge_qtipo.tr_error
                                     ) is
    --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_apfd.apfd_fdei )  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FDEI'         , p_ty_apfd.apfd_fdei, null             , null, null, null ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TAPFD', 'APFD_FDEI_RELA'    , p_ty_apfd.apfd_fdei_rela, null        , null, null, null ,'E');
        sf_pins_error('SF_QAUTO_CLIE.ELIMINA_EMAIL --> FIN');                                                                                          
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_apoderado.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_apoderado; 
    --
    procedure insertar_empresa_fidei (  p_emfd        sf_temfd%rowtype
                                      , p_ty_erro out ge_qtipo.tr_error
                                     ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_emfd.emfd_fdei );
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_FDEI'   , null , p_emfd.emfd_fdei    , p_emfd.emfd_fdei, p_emfd.emfd_emfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_EMFD'   , null , p_emfd.emfd_emfd    , p_emfd.emfd_fdei, p_emfd.emfd_emfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_DESCRI' , null , p_emfd.emfd_descri  , p_emfd.emfd_fdei, p_emfd.emfd_emfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_CARGO'  , null , p_emfd.emfd_cargo   , p_emfd.emfd_fdei, p_emfd.emfd_emfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_DIRE'   , null , p_emfd.emfd_dire    , p_emfd.emfd_fdei, p_emfd.emfd_emfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_CIUD'   , null , p_emfd.emfd_ciud    , p_emfd.emfd_fdei, p_emfd.emfd_emfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_TELE'   , null , p_emfd.emfd_tele    , p_emfd.emfd_fdei, p_emfd.emfd_emfd, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_FECVINC', null , p_emfd.emfd_fecvinc , p_emfd.emfd_fdei, p_emfd.emfd_emfd, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_FECCRE' , null , p_emfd.emfd_feccre  , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_USUA'   , null , p_emfd.emfd_usua    , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_EMPRESA_FIDEI.'||sqlerrm;
            --
    end insertar_empresa_fidei;
    --
    procedure actualizar_empresa_fidei (  p_emfd_new      sf_temfd%rowtype
                                        , p_emfd_old      sf_temfd%rowtype
                                        , p_ty_erro   out ge_qtipo.tr_error
                                        ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_emfd_old.emfd_fdei,p_emfd_new.emfd_usua );-- 1003
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_FDEI'   , p_emfd_old.emfd_fdei    ,p_emfd_new.emfd_fdei    , p_emfd_old.emfd_fdei, p_emfd_old.emfd_emfd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_EMFD'   , p_emfd_old.emfd_emfd    ,p_emfd_new.emfd_emfd    , p_emfd_old.emfd_fdei, p_emfd_old.emfd_emfd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_DESCRI' , p_emfd_old.emfd_descri  ,p_emfd_new.emfd_descri  , p_emfd_old.emfd_fdei, p_emfd_old.emfd_emfd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_CARGO'  , p_emfd_old.emfd_cargo   ,p_emfd_new.emfd_cargo   , p_emfd_old.emfd_fdei, p_emfd_old.emfd_emfd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_DIRE'   , p_emfd_old.emfd_dire    ,p_emfd_new.emfd_dire    , p_emfd_old.emfd_fdei, p_emfd_old.emfd_emfd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_CIUD'   , p_emfd_old.emfd_ciud    ,p_emfd_new.emfd_ciud    , p_emfd_old.emfd_fdei, p_emfd_old.emfd_emfd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_TELE'   , p_emfd_old.emfd_tele    ,p_emfd_new.emfd_tele    , p_emfd_old.emfd_fdei, p_emfd_old.emfd_emfd, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_FECVINC', p_emfd_old.emfd_fecvinc ,p_emfd_new.emfd_fecvinc , p_emfd_old.emfd_fdei, p_emfd_old.emfd_emfd, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_FECCRE' , p_emfd_old.emfd_feccre  ,p_emfd_new.emfd_feccre  , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_USUA'   , p_emfd_old.emfd_usua    ,p_emfd_new.emfd_usua    , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_EMPRESA_FIDEI.'||sqlerrm;
            --
    end actualizar_empresa_fidei;
    --
    procedure eliminar_empresa_fidei (  p_ty_emfd  in out sf_temfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_emfd.emfd_fdei )  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_FDEI'    , p_ty_emfd.emfd_fdei, null     , null, null, null ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TEMFD', 'EMFD_EMFD'    , p_ty_emfd.emfd_emfd, null     , null, null, null ,'E');
        sf_pins_error('SF_QAUTO_CLIE.ELIMINA_EMAIL --> FIN');                                                                                          
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_empresa_fidei.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_empresa_fidei;
    --    
    procedure insertar_persona_cargo (  p_prcg         sf_tprcg%rowtype
                                      , p_ty_erro  out ge_qtipo.tr_error
                                     ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_prcg.prcg_fdei );
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_PRCG'      , null, p_prcg.prcg_prcg     , p_prcg.prcg_fdei, p_prcg.prcg_prcg, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_FDEI'      , null, p_prcg.prcg_fdei     , p_prcg.prcg_fdei, p_prcg.prcg_prcg, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_PARENT'    , null, p_prcg.prcg_parent   , p_prcg.prcg_fdei, p_prcg.prcg_prcg, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_DESCRI'    , null, p_prcg.prcg_descri   , p_prcg.prcg_fdei, p_prcg.prcg_prcg, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_FECH_NAC'  , null, p_prcg.prcg_fech_nac , p_prcg.prcg_fdei, p_prcg.prcg_prcg, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_PEP'       , null, p_prcg.prcg_pep      , p_prcg.prcg_fdei, p_prcg.prcg_prcg, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_ID'        , null, p_prcg.prcg_id       , p_prcg.prcg_fdei, p_prcg.prcg_prcg, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_USUA'      , null ,p_prcg.prcg_usua     , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_FECCRE'    , null ,p_prcg.prcg_feccre   , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_USUAMOD'   , null ,p_prcg.prcg_usuamod  , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_FECMOD'    , null ,p_prcg.prcg_fecmod   , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_PERSONA_CARGO.'||sqlerrm;
            --
    end insertar_persona_cargo;
    --
    procedure actualizar_persona_cargo (  p_prcg_new      sf_tprcg%rowtype
                                        , p_prcg_old      sf_tprcg%rowtype
                                        , p_ty_erro   out ge_qtipo.tr_error
                                       ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_prcg_old.prcg_fdei ,p_prcg_new.prcg_usua );-- 1003
        --
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_PRCG'      , p_prcg_old.prcg_prcg     , p_prcg_new.prcg_prcg     , p_prcg_old.prcg_fdei, p_prcg_old.prcg_prcg, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_FDEI'      , p_prcg_old.prcg_fdei     , p_prcg_new.prcg_fdei     , p_prcg_old.prcg_fdei, p_prcg_old.prcg_prcg, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_PARENT'    , p_prcg_old.prcg_parent   , p_prcg_new.prcg_parent   , p_prcg_old.prcg_fdei, p_prcg_old.prcg_prcg, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_DESCRI'    , p_prcg_old.prcg_descri   , p_prcg_new.prcg_descri   , p_prcg_old.prcg_fdei, p_prcg_old.prcg_prcg, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_FECH_NAC'  , p_prcg_old.prcg_fech_nac , p_prcg_new.prcg_fech_nac , p_prcg_old.prcg_fdei, p_prcg_old.prcg_prcg, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_PEP'       , p_prcg_old.prcg_pep      , p_prcg_new.prcg_pep      , p_prcg_old.prcg_fdei, p_prcg_old.prcg_prcg, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG' ,'PRCG_ID'       , p_prcg_old.prcg_id       , p_prcg_new.prcg_id       , p_prcg_old.prcg_fdei, p_prcg_old.prcg_prcg, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_USUA'      ,p_prcg_old.prcg_usua     ,p_prcg_new.prcg_usua     , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_FECCRE'    ,p_prcg_old.prcg_feccre   ,p_prcg_new.prcg_feccre   , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_USUAMOD'   ,p_prcg_old.prcg_usuamod  ,p_prcg_new.prcg_usuamod  , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG','PRCG_FECMOD'    ,p_prcg_old.prcg_fecmod   ,p_prcg_new.prcg_fecmod   , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_PERSONA_CARGO.'||sqlerrm;
            --
    end actualizar_persona_cargo;
    --
    procedure eliminar_persona_cargo (  p_ty_prcg  in out sf_tprcg%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_prcg.prcg_fdei )  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_PRCG'    , p_ty_prcg.prcg_prcg, null     , null, null, null ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TPRCG', 'PRCG_FDEI'    , p_ty_prcg.prcg_fdei, null     , null, null, null ,'E');
        sf_pins_error('SF_QAUTO_CLIE.ELIMINA_EMAIL --> FIN');                                                                                          
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_persona_cargo.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_persona_cargo; 
    procedure insertar_firmas_autorizadas (  p_frau         sf_tfrau%rowtype
                                           , p_ty_erro  out ge_qtipo.tr_error
                                          ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_frau.frau_fdei );
        --
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU' , 'FRAU_FDEI'  , null, p_frau.frau_fdei   , p_frau.frau_fdei, p_frau.frau_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU' , 'FRAU_AUXI'  , null, p_frau.frau_auxi   , p_frau.frau_fdei, p_frau.frau_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU' , 'FRAU_CARGO' , null, p_frau.frau_cargo  , p_frau.frau_fdei, p_frau.frau_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU' , 'FRAU_PEP_PN', null, p_frau.frau_pep_pn , p_frau.frau_fdei, p_frau.frau_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU' , 'FRAU_PEP_PJ', null, p_frau.frau_pep_pj , p_frau.frau_fdei, p_frau.frau_auxi, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFRAU','FRAU_USUA',     null ,p_frau.frau_usua   , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFRAU','FRAU_FECCRE',   null ,p_frau.frau_feccre , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_FIRMAS_AUTORIZADAS.'||sqlerrm;
            --
    end insertar_firmas_autorizadas;
    --
    procedure actualizar_firmas_autorizadas (  p_frau_new      sf_tfrau%rowtype
                                             , p_frau_old      sf_tfrau%rowtype
                                             , p_ty_erro   out ge_qtipo.tr_error
                                            ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_frau_old.frau_fdei, p_frau_new.frau_usua );--1003
        --
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU', 'FRAU_FDEI'  , p_frau_old.frau_fdei   , p_frau_new.frau_fdei   , p_frau_old.frau_fdei, p_frau_old.frau_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU', 'FRAU_AUXI'  , p_frau_old.frau_auxi   , p_frau_new.frau_auxi   , p_frau_old.frau_fdei, p_frau_old.frau_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU', 'FRAU_CARGO' , p_frau_old.frau_cargo  , p_frau_new.frau_cargo  , p_frau_old.frau_fdei, p_frau_old.frau_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU', 'FRAU_PEP_PN', p_frau_old.frau_pep_pn , p_frau_new.frau_pep_pn , p_frau_old.frau_fdei, p_frau_old.frau_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFRAU', 'FRAU_PEP_PJ', p_frau_old.frau_pep_pj , p_frau_new.frau_pep_pj , p_frau_old.frau_fdei, p_frau_old.frau_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFRAU','FRAU_USUA',     p_frau_old.frau_usua   ,p_frau_new.frau_usua   , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFRAU','FRAU_FECCRE',   p_frau_old.frau_feccre ,p_frau_new.frau_feccre , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_FIRMAS_AUTORIZADAS.'||sqlerrm;
            --
    end actualizar_firmas_autorizadas;
    --
    procedure eliminar_firmas_autorizadas (  p_ty_frau  in out sf_tfrau%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_frau.frau_fdei )  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFRAU', 'FRAU_FDEI'    , p_ty_frau.frau_fdei, null     , null, null, null ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFRAU', 'FRAU_AUXI'    , p_ty_frau.frau_auxi, null     , null, null, null ,'E');
        sf_pins_error('SF_QAUTO_CLIE.ELIMINA_EMAIL --> FIN');                                                                                          
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_firmas_autorizadas.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_firmas_autorizadas;     
    --
    procedure insertar_sucursales(  p_fsuc         sf_tfsuc%rowtype
                                  , p_ty_erro  out ge_qtipo.tr_error
                                 ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_fsuc.fsuc_fdei );
        --
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_FDEI'  , null, p_fsuc.fsuc_fdei   , p_fsuc.fsuc_fdei, p_fsuc.fsuc_fsuc, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_FSUC'  , null, p_fsuc.fsuc_fsuc   , p_fsuc.fsuc_fdei, p_fsuc.fsuc_fsuc, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_DESCRI', null, p_fsuc.fsuc_descri , p_fsuc.fsuc_fdei, p_fsuc.fsuc_fsuc, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_DIRE'  , null, p_fsuc.fsuc_dire   , p_fsuc.fsuc_fdei, p_fsuc.fsuc_fsuc, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_CIUD'  , null, p_fsuc.fsuc_ciud   , p_fsuc.fsuc_fdei, p_fsuc.fsuc_fsuc, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_TELE'  , null, p_fsuc.fsuc_tele   , p_fsuc.fsuc_fdei, p_fsuc.fsuc_fsuc, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_FAX'   , null, p_fsuc.fsuc_fax    , p_fsuc.fsuc_fdei, p_fsuc.fsuc_fsuc, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFSUC','FSUC_FECCRE', null ,p_fsuc.fsuc_feccre , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFSUC','FSUC_USUA'  , null ,p_fsuc.fsuc_usua   , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_SUCURSALES.'||sqlerrm;
            --
    end insertar_sucursales;
    --
    procedure actualizar_sucursales (  p_fsuc_new      sf_tfsuc%rowtype
                                     , p_fsuc_old      sf_tfsuc%rowtype
                                     , p_ty_erro   out ge_qtipo.tr_error
                                    )is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_fsuc_old.fsuc_fdei, p_fsuc_new.fsuc_usua);--1003
        --
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_FDEI'  , p_fsuc_old.fsuc_fdei     , p_fsuc_new.fsuc_fdei   , p_fsuc_old.fsuc_fdei, p_fsuc_old.fsuc_fsuc, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_FSUC'  , p_fsuc_old.fsuc_fsuc     , p_fsuc_new.fsuc_fsuc   , p_fsuc_old.fsuc_fdei, p_fsuc_old.fsuc_fsuc, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_DESCRI', p_fsuc_old.fsuc_descri   , p_fsuc_new.fsuc_descri , p_fsuc_old.fsuc_fdei, p_fsuc_old.fsuc_fsuc, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_DIRE'  , p_fsuc_old.fsuc_dire     , p_fsuc_new.fsuc_dire   , p_fsuc_old.fsuc_fdei, p_fsuc_old.fsuc_fsuc, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_CIUD'  , p_fsuc_old.fsuc_ciud     , p_fsuc_new.fsuc_ciud   , p_fsuc_old.fsuc_fdei, p_fsuc_old.fsuc_fsuc, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_TELE'  , p_fsuc_old.fsuc_tele     , p_fsuc_new.fsuc_tele   , p_fsuc_old.fsuc_fdei, p_fsuc_old.fsuc_fsuc, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TFSUC', 'FSUC_FAX'   , p_fsuc_old.fsuc_fax      , p_fsuc_new.fsuc_fax    , p_fsuc_old.fsuc_fdei, p_fsuc_old.fsuc_fsuc, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFSUC','FSUC_FECCRE', p_fsuc_old.fsuc_feccre   ,p_fsuc_new.fsuc_feccre , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TFSUC','FSUC_USUA'  , p_fsuc_old.fsuc_usua     ,p_fsuc_new.fsuc_usua   , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_SUCURSALES.'||sqlerrm;
            --
    end actualizar_sucursales;
    --
    procedure eliminar_sucursales (  p_ty_fsuc  in out sf_tfsuc%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_fsuc.fsuc_fdei )  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFSUC', 'FSUC_FDEI'    , p_ty_fsuc.fsuc_fdei, null     , null, null, null ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TFSUC', 'FSUC_FSUC'    , p_ty_fsuc.fsuc_fsuc, null     , null, null, null ,'E');
        sf_pins_error('SF_QAUTO_CLIE.ELIMINA_EMAIL --> FIN');                                                                                          
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_sf_tfsuc.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_sucursales; 
    --
    procedure insertar_socios (  p_soci        sf_tsoci%rowtype
                               , p_ty_erro out ge_qtipo.tr_error
                              ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_soci.soci_fdei );
        --
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_FDEI'     , null , p_soci.soci_fdei      , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_AUXI'     , null , p_soci.soci_auxi      , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_PORC'     , null , p_soci.soci_porc      , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_PAIS'     , null , p_soci.soci_pais      , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_ID_TRIB'  , null , p_soci.soci_id_trib   , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_TIPO_SOCI', null , p_soci.soci_tipo_soci , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_PEP'      , null , p_soci.soci_pep       , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_STAT'     , null , p_soci.soci_stat      , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_USUA_INA' , null , p_soci.soci_usua_ina  , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_FEC_INA'  , null , p_soci.soci_fec_ina   , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_SOCI'     , null , p_soci.soci_soci      , p_soci.soci_fdei, p_soci.soci_auxi, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TSOCI','SOCI_USUA'     , null ,p_soci.soci_usua      , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TSOCI','SOCI_FECCRE'   , null ,p_soci.soci_feccre    , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_SOCIOS.'||sqlerrm;
            --
    end insertar_socios;
    --
    procedure actualizar_socios (  p_soci_new      sf_tsoci%rowtype
                                 , p_soci_old      sf_tsoci%rowtype
                                 , p_ty_erro   out ge_qtipo.tr_error
                                ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_soci_old.soci_fdei ,p_soci_new.soci_usua );--1003
        --
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_FDEI'     , p_soci_old.soci_fdei      , p_soci_new.soci_fdei      , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_AUXI'     , p_soci_old.soci_auxi      , p_soci_new.soci_auxi      , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_PORC'     , p_soci_old.soci_porc      , p_soci_new.soci_porc      , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_PAIS'     , p_soci_old.soci_pais      , p_soci_new.soci_pais      , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_ID_TRIB'  , p_soci_old.soci_id_trib   , p_soci_new.soci_id_trib   , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_TIPO_SOCI', p_soci_old.soci_tipo_soci , p_soci_new.soci_tipo_soci , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_PEP'      , p_soci_old.soci_pep       , p_soci_new.soci_pep       , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_STAT'     , p_soci_old.soci_stat      , p_soci_new.soci_stat      , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_USUA_INA' , p_soci_old.soci_usua_ina  , p_soci_new.soci_usua_ina  , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_FEC_INA'  , p_soci_old.soci_fec_ina   , p_soci_new.soci_fec_ina   , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TSOCI', 'SOCI_SOCI'     , p_soci_old.soci_soci      , p_soci_new.soci_soci      , p_soci_old.soci_fdei, p_soci_old.soci_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TSOCI','SOCI_USUA'     , p_soci_old.soci_usua      ,p_soci_new.soci_usua      , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TSOCI','SOCI_FECCRE'   , p_soci_old.soci_feccre    ,p_soci_new.soci_feccre    , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_SOCIOS.'||sqlerrm;
            --
    end actualizar_socios;
    --
    procedure eliminar_socios (  p_ty_soci  in out sf_tsoci%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_soci.soci_fdei )  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TSOCI', 'SOCI_FDEI'    , p_ty_soci.soci_fdei, null     , null, null, null ,'E');
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TSOCI', 'SOCI_AUXI'    , p_ty_soci.soci_auxi, null     , null, null, null ,'E');
        sf_pins_error('SF_QAUTO_CLIE.ELIMINA_EMAIL --> FIN');                                                                                          
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_socios.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_socios;  
    --
    procedure insertar_administradores (  p_admi         sf_tadmi%rowtype
                                        , p_ty_erro  out ge_qtipo.tr_error
                                       ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_admi.admi_fdei );
        --
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_FDEI'    , null, p_admi.admi_fdei     , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_NIT'     , null, p_admi.admi_nit      , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_TPID'    , null, p_admi.admi_tpid     , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_NOMBRE1' , null, p_admi.admi_nombre1  , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_NOMBRE2' , null, p_admi.admi_nombre2  , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_APELL1'  , null, p_admi.admi_apell1   , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_APELL2'  , null, p_admi.admi_apell2   , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_VINCULO' , null, p_admi.admi_vinculo  , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_PEP'     , null, p_admi.admi_pep      , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_STAT'    , null, p_admi.admi_stat     , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_USUA_INA', null, p_admi.admi_usua_ina , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_FEC_INA' , null, p_admi.admi_fec_ina  , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_TIAD'    , null, p_admi.admi_tiad     , p_admi.admi_fdei, p_admi.admi_nit, null, 'I' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TADMI','ADMI_USUA'    , null ,p_admi.admi_usua     , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TADMI','ADMI_FECH'    , null ,p_admi.admi_fech     , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.INSERTAR_ADMINISTRADORES.'||sqlerrm;
            --
    end insertar_administradores;
    --
    procedure actualizar_administradores (  p_admi_new      sf_tadmi%rowtype
                                          , p_admi_old      sf_tadmi%rowtype
                                          , p_ty_erro   out ge_qtipo.tr_error
                                         ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        --
    begin
        --
        v_prfd := validar_proceso( null, p_admi_old.admi_fdei ,p_admi_new.admi_usua  );-- 1003
        --
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_FDEI'    , p_admi_old.admi_fdei    , p_admi_new.admi_fdei     , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_NIT'     , p_admi_old.admi_nit     , p_admi_new.admi_nit      , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_TPID'    , p_admi_old.admi_tpid    , p_admi_new.admi_tpid     , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_NOMBRE1' , p_admi_old.admi_nombre1 , p_admi_new.admi_nombre1  , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_NOMBRE2' , p_admi_old.admi_nombre2 , p_admi_new.admi_nombre2  , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_APELL1'  , p_admi_old.admi_apell1  , p_admi_new.admi_apell1   , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_APELL2'  , p_admi_old.admi_apell2  , p_admi_new.admi_apell2   , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_VINCULO' , p_admi_old.admi_vinculo , p_admi_new.admi_vinculo  , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_PEP'     , p_admi_old.admi_pep     , p_admi_new.admi_pep      , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_STAT'    , p_admi_old.admi_stat    , p_admi_new.admi_stat     , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_USUA_INA', p_admi_old.admi_usua_ina, p_admi_new.admi_usua_ina , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_FEC_INA' , p_admi_old.admi_fec_ina , p_admi_new.admi_fec_ina  , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd, 'SF_TADMI', 'ADMI_TIAD'    , p_admi_old.admi_tiad    , p_admi_new.admi_tiad     , p_admi_old.admi_fdei, p_admi_old.admi_nit, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TADMI','ADMI_USUA'    , p_admi_old.admi_usua    ,p_admi_new.admi_usua     , null, null, null );
        --sf_qauto_clie.mapear_campos( v_prfd,'SF_TADMI','ADMI_FECH'    , p_admi_old.admi_fech    ,p_admi_new.admi_fech     , null, null, null );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.ACTUALIZAR_ADMINISTRADORES.'||sqlerrm;
            --
    end actualizar_administradores;
    --
procedure eliminar_administradores (  p_ty_admi  in out sf_tadmi%rowtype
                                    , p_ty_erro     out ge_qtipo.tr_error
                                    ) is
         --
         --                                 
         p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
         v_prfd       number;               
         --                                 
    begin                                   
        --                                                                                                                                             
        v_prfd := validar_proceso( null, p_ty_admi.admi_fdei )  ;                                                                                    
        --                                                                                                                                             
        sf_qauto_clie.mapear_campos( v_prfd,'SF_TADMI', 'ADMI_NIT'    , p_ty_admi.admi_nit, null     , null, null, null ,'E');                                                                                        
        --                                                                                                                                             
        p_ty_erro.cod_error := 'OK';                                                                                                                   
        p_ty_erro.msg_error := 'Mapeo Correcto';                                                                                                       
        --                                                                                                                                             
    exception                                                                                                                                          
        when others then                                                                                                                               
            --                                                                                                                                         
            p_ty_erro.cod_error := 'ERROR';                                                                                                            
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.eliminar_sf_tadmi.'||sqlerrm;                                                                           
            --                                                                                                                                         
    end eliminar_administradores;   
    function obtiene_valor_nuevo (  p_prfd    sf_tdtpf.dtpf_prfd%type
                                  , p_tbla    sf_tdtpf.dtpf_tbla%type
                                  , p_cmpo    sf_tdtpf.dtpf_cmpo%type
                                 ) return varchar2 is
        --
        cursor c_dtpf is
        select dtpf_dtnv
          from sf_tdtpf
         where dtpf_prfd = p_prfd
           and dtpf_tbla = p_tbla
           and dtpf_cmpo = p_cmpo;
        --
        v_dtnv  sf_tdtpf.dtpf_dtnv%type;
        --
    begin
        --
        open c_dtpf;
        fetch c_dtpf into v_dtnv;
        close c_dtpf;
        --
        return v_dtnv;
        --
    end;
    --
    function obtiene_valor_antiguo (  p_prfd    sf_tdtpf.dtpf_prfd%type
                                    , p_tbla    sf_tdtpf.dtpf_tbla%type
                                    , p_cmpo    sf_tdtpf.dtpf_cmpo%type
                                   ) return varchar2 is
        --
        cursor c_dtpf is
        select dtpf_dtor
          from sf_tdtpf
         where dtpf_prfd = p_prfd
           and dtpf_tbla = p_tbla
           and dtpf_cmpo = p_cmpo;
        --
        v_dtor  sf_tdtpf.dtpf_dtor%type;
        --
    begin
        --
        open c_dtpf;
        fetch c_dtpf into v_dtor;
        close c_dtpf;
        --
        return v_dtor;
        --
    end;
    --
    function nombre_cliente ( p_prfd  sf_tprfd.prfd_prfd%type ) return varchar2 is
        --
        cursor c_prfd is
        select prfd_fdei, prfd_tppr
          from sf_tprfd
         where prfd_prfd = p_prfd;
        --
        v_prfd  c_prfd%rowtype;
        --
        v_nombre varchar2(500);
        v_apell1 varchar2(500);
        v_apell2 varchar2(500);
        --
        v_descri varchar2(1500);
        --
    begin
        --
        open c_prfd;
        fetch c_prfd into v_prfd;
        close c_prfd;
        --
        if ( v_prfd.prfd_tppr = 'I' ) then
            --
            v_descri :=     sf_qauto_clie.obtiene_valor_nuevo( p_prfd => p_prfd,
                                                           p_tbla => 'SF_TFDEI',
                                                           p_cmpo => 'FDEI_APELL1')
                        ||' '||sf_qauto_clie.obtiene_valor_nuevo( p_prfd => p_prfd,
                                                           p_tbla => 'SF_TFDEI',
                                                           p_cmpo => 'FDEI_APELL2')
                        ||' '||sf_qauto_clie.obtiene_valor_nuevo( p_prfd => p_prfd,
                                                           p_tbla => 'SF_TFDEI',
                                                           p_cmpo => 'FDEI_NOMBRE')        ;                                                   
            --
        else
            --
            v_descri := sf_qfdei.nombre_completo(v_prfd.prfd_fdei);
            --
        end if;
        --
        return v_descri;
        --
    end nombre_cliente;
    --
    procedure set_origen ( p_origen  varchar2) is
    begin
        --
        vg_origen:=p_origen;
        --
    end set_origen;
    --
    function get_origen  return varchar2 is
    begin
        return vg_origen;
    end get_origen;
    --
    procedure set_auto ( p_auto  boolean ) is
    begin
        --
        vg_auto := p_auto;
        --
    end set_auto;
    --
    function get_auto return boolean is
    begin
        --
        return vg_auto;
        --
    end get_auto;
    --
    procedure set_proceso ( p_prfd  sf_tprfd.prfd_prfd%type ) is
    --
    begin
        --
        vg_prfd := p_prfd;
        --
    end set_proceso;
    function get_proceso return number is
    begin
        --
        return vg_prfd;
        --
    end get_proceso;
    --
    function get_fdei return number is
        --
        cursor c_prfd ( pc_prfd  sf_tprfd.prfd_prfd%type ) is
        select prfd_fdei
          from sf_tprfd
         where prfd_prfd = pc_prfd;
        --
        v_prfd   sf_tprfd.prfd_prfd%type;
        v_fdei   sf_tprfd.prfd_fdei%type;
    begin
        --
        v_prfd := sf_qauto_clie.get_proceso;
        --
        open c_prfd(v_prfd);
        fetch c_prfd into v_fdei;
        close c_prfd;
        --
        return v_fdei;
        --
    end get_fdei;
    --
    procedure procesa_datos is
        --
        cursor c_dtpf is
      select distinct dtpf_tbla, dtpf_idnu, dtpf_idvc, dtpf_tptr,1
          from  sf_tdtpf
         where  dtpf_prfd = sf_qauto_clie.get_proceso
         AND    dtpf_tbla= 'GE_TAUXIL'
         union
      select distinct dtpf_tbla, dtpf_idnu, dtpf_idvc, dtpf_tptr,2
          from  sf_tdtpf
         where  dtpf_prfd = sf_qauto_clie.get_proceso
         AND    dtpf_tbla= 'SF_TFDEI'
         union          
        select  distinct dtpf_tbla, dtpf_idnu, dtpf_idvc, dtpf_tptr,3
          from  sf_tdtpf
         where  dtpf_prfd = sf_qauto_clie.get_proceso
         AND    dtpf_tbla NOT IN ('SF_TFDEI','GE_TAUXIL')-- 1005
         ORDER BY 5;
         --
         CURSOR c_auxi (pc_auxi number )is select auxi_auxi from ge_tauxil where auxi_auxi=pc_auxi;
        --
        v_dtpf  c_dtpf%rowtype;
         v_auxi number;
        --
    begin
      --
      for d in c_dtpf loop
        --
        if ( d.dtpf_tptr = 'I') then
            --ini 1005
            /*
            open c_auxi(d.dtpf_idnu);
            fetch c_auxi into v_auxi;
            close c_auxi;
            if (d.dtpf_tbla  in ('GE_TAUXIL')) and ( v_auxi is not null )then 
                null;
            else
                sf_qauto_clie.inserta_datos( d.dtpf_tbla, d.dtpf_idnu, d.dtpf_idvc );
            end if;
            */
            
            sf_qauto_clie.inserta_datos( d.dtpf_tbla, d.dtpf_idnu, d.dtpf_idvc );
            --
        elsif ( d.dtpf_tptr = 'A' ) then
            --
           -- if d.dtpf_tbla not in ('GE_TAUXIL') then -- 1006
                sf_qauto_clie.actualiza_datos( d.dtpf_tbla, d.dtpf_idnu, d.dtpf_idvc );
           --end if; -- 1006
            --
        elsif ( d.dtpf_tptr = 'E' ) then
            --
            if d.dtpf_tbla not in ('GE_TAUXIL') then
                sf_qauto_clie.elimina_datos( d.dtpf_tbla, d.dtpf_idnu, d.dtpf_idvc );
            end if;
            --
        end if;
        --
      end loop;
      --
    end procesa_datos;
    --
    procedure inserta_datos (  p_tbla      sf_tdtpf.dtpf_tbla%type
                             , p_idnu      sf_tdtpf.dtpf_idnu%type
                             , p_idvc      sf_tdtpf.dtpf_idvc%type
                             ) is
        --
        cursor c_dtpf ( pc_col varchar2 )is
        select dtpf_tbla, dtpf_cmpo, dtpf_dtor, dtpf_dtnv, dtpf_idnu, dtpf_idvc
          from sf_tdtpf
         where dtpf_prfd   = sf_qauto_clie.get_proceso
           and dtpf_tbla   = p_tbla
           and dtpf_cmpo   = nvl( pc_col, dtpf_cmpo )
           and ( dtpf_idvc = nvl( p_idvc, dtpf_idvc ) or dtpf_idvc is null )
           and ( dtpf_idnu = nvl( p_idnu, dtpf_idnu ) or dtpf_idnu is null )
           and dtpf_tptr   = 'I'
         order by dtpf_dtpf;
        -- ini 1005
        Cursor c_auxi_auxi is   select nvl(max(auxi_auxi),0) + 1
                                from ge_tauxil;
        --
        cursor c_fdei_auxi (p_nit number) is  select auxi_auxi 
                                              from ge_tauxil where auxi_nit=p_nit; 
            
        cursor c_insert_auxi   is    select COUNT(1)
                                     from   sf_tdtpf
                                     where  dtpf_prfd   = sf_qauto_clie.get_proceso
                                     and    dtpf_CMPO   = 'AUXI_AUXI'
                                     AND    nvl(dtpf_dtnv,'x-1') <> nvl(dtpf_dtor,'x-1');
        v_auxi_auxi     number;       
        v_insert_auxi   number;
        -- fin 1005
        
        v_sql  varchar(4000);
        v_cont number;
       
        --
    begin
        --
        sf_pins_error('inserta_datos...');
        --
        v_sql := 'insert into '||lower(p_tbla)||' ( ';
        --
        --sf_pins_error(v_sql);
        -- columnas
        v_cont := 1;

        for c in c_dtpf ( null ) loop
            --
            if ( nvl(c.dtpf_dtnv,'x-1') <> nvl(c.dtpf_dtor,'x-1') ) then
                --
                if ( v_cont <> 1 ) then
                    --
                    v_sql := v_sql||' , ';
                    --
                end if;
                --
                v_sql := v_sql||lower(c.dtpf_cmpo);
                --
                v_cont := v_cont + 1;
                --
            end if;
            --
        end loop;
        --
        v_sql := v_sql||' ) values ( ';
        --
        -- Valores
        v_cont := 1;
        -- ini 1005
            open    c_insert_auxi;
            fetch   c_insert_auxi   into v_insert_auxi;
            close   c_insert_auxi;
        -- fin 1005
        for c in c_dtpf ( null ) loop
            --
            if ( nvl(c.dtpf_dtnv,'x-1') <> nvl(c.dtpf_dtor,'x-1') ) then
                --
                if ( v_cont <> 1 ) then
                    --
                    v_sql := v_sql||' , ';
                    --
                end if;
                -- ini 1005
                if (c.dtpf_cmpo = 'AUXI_AUXI')  then
                    --
                    open  c_auxi_auxi;
                    fetch c_auxi_auxi into v_auxi_auxi;
                    close c_auxi_auxi;
                    --
                    v_sql := v_sql||sf_qauto_clie.conv_dato ( v_auxi_auxi, p_tbla, c.dtpf_cmpo);
                elsif(c.dtpf_cmpo = 'FDEI_AUXI') and (v_insert_auxi > 0 )then 
                    --
                    open  c_auxi_auxi;
                    fetch c_auxi_auxi into v_auxi_auxi;
                    close c_auxi_auxi;
                    --
                    v_auxi_auxi:=v_auxi_auxi-1;

                    --
                    v_sql := v_sql||sf_qauto_clie.conv_dato ( v_auxi_auxi, p_tbla, c.dtpf_cmpo);
                    --
                else
                    v_sql := v_sql||sf_qauto_clie.conv_dato ( c.dtpf_dtnv, p_tbla, c.dtpf_cmpo);
                end if;
                -- fin 1005
                v_cont := v_cont + 1;
                --
            end if;
            --
        end loop;
        --
        v_sql := v_sql||' ) ';
        --
        sf_pins_error(v_sql);
        execute immediate v_sql;
        --
    end inserta_datos;
    --
    procedure actualiza_datos (  p_tbla      sf_tdtpf.dtpf_tbla%type
                               , p_idnu      sf_tdtpf.dtpf_idnu%type
                               , p_idvc      sf_tdtpf.dtpf_idvc%type
                              ) is
        --
        cursor c_dtpf ( pc_col varchar2 )is
        select distinct dtpf_tbla, dtpf_cmpo, dtpf_dtor, dtpf_dtnv, dtpf_idnu, dtpf_idvc
          from sf_tdtpf
         where dtpf_prfd   = sf_qauto_clie.get_proceso
           and dtpf_tbla   = p_tbla
           and dtpf_cmpo   = nvl( pc_col, dtpf_cmpo )
           and ( dtpf_idvc = nvl( p_idvc, dtpf_idvc ) or dtpf_idvc is null )
           and ( dtpf_idnu = nvl( p_idnu, dtpf_idnu ) or dtpf_idnu is null )
           and dtpf_tptr   = 'A';
        --
        cursor c_pk is
        select col.column_name, col.position
          from all_constraints con, all_cons_columns col
         where con.table_name = upper(p_tbla)
           and con.constraint_type = 'P'
           and con.constraint_name = col.constraint_name
         order by col.position;
        --
        v_dtpf  c_dtpf%rowtype;
        v_sql   varchar(4000);
        v_cont  number;
        --
    begin
        --
        sf_pins_error('actualiza_datos...');
        --
        v_sql := 'update '||lower(p_tbla)||' set ';
        --
        --sf_pins_error(v_sql);
        -- columnas
        v_cont := 1;
        for c in c_dtpf ( null ) loop
            --
            if ( nvl(c.dtpf_dtnv,'x-1') <> nvl(c.dtpf_dtor,'x-1') ) then
                --
                if ( v_cont <> 1 ) then
                    --
                    v_sql := v_sql||' , ';
                    --
                end if;
                --
                if ( c.dtpf_dtnv is not null ) then
                    v_sql := v_sql||lower(c.dtpf_cmpo)||' = '|| sf_qauto_clie.conv_dato ( c.dtpf_dtnv, p_tbla, c.dtpf_cmpo)||' ';
                else
                    v_sql := v_sql||lower(c.dtpf_cmpo)||' = null ';
                end if;
                --
                v_cont := v_cont + 1;
                --
            end if;
            --
        end loop;
        --
        v_sql := v_sql||'where ';
        --sf_pins_error(v_sql);
        -- where
        for p in c_pk loop
            --
            open c_dtpf( p.column_name );
            fetch c_dtpf into v_dtpf;
            close c_dtpf;
            --
            if ( p.position <> 1 ) then
                v_sql:= v_sql||' and ';
            end if;
            --
            v_sql:= v_sql||lower(p.column_name)||' = '||  sf_qauto_clie.conv_dato ( v_dtpf.dtpf_dtor, p_tbla, p.column_name)||' ';
            --
        end loop;
        --
        if ( v_cont > 1 ) then
            sf_pins_error(v_sql);
            execute immediate v_sql;
        end if;
        
        --
    end actualiza_datos;
    --
    procedure elimina_datos (  p_tbla      sf_tdtpf.dtpf_tbla%type
                             , p_idnu      sf_tdtpf.dtpf_idnu%type
                             , p_idvc      sf_tdtpf.dtpf_idvc%type
                            ) is
        --
        cursor c_dtpf ( pc_col varchar2 )is
        select dtpf_tbla, dtpf_cmpo, dtpf_dtor, dtpf_dtnv, dtpf_idnu, dtpf_idvc
          from sf_tdtpf
         where dtpf_prfd   = sf_qauto_clie.get_proceso
           and dtpf_tbla   = p_tbla
           and dtpf_cmpo   = nvl( pc_col, dtpf_cmpo )
           and ( dtpf_idvc = nvl( p_idvc, dtpf_idvc ) or dtpf_idvc is null )
           and ( dtpf_idnu = nvl( p_idnu, dtpf_idnu ) or dtpf_idnu is null )
           and dtpf_tptr   = 'E'
         order by dtpf_dtpf;
        --
        cursor c_pk is
        select col.column_name, col.position
        from all_constraints con, all_cons_columns col
        where con.table_name = upper(p_tbla)
        and con.constraint_type = 'P'
        and con.constraint_name = col.constraint_name
        order by col.position;
        --
        v_dtpf  c_dtpf%rowtype;
        v_sql   varchar(4000);
        v_cont  number;
        --
    begin
        --
        sf_pins_error('elimina_datos...');
        --
        v_sql := 'delete from '||lower(p_tbla)||' where ';
        --
        --sf_pins_error(v_sql);
        -- where
        for p in c_pk loop
            --
            open c_dtpf( p.column_name );
            fetch c_dtpf into v_dtpf;
            close c_dtpf;
            --
            if ( p.position <> 1 ) then
                v_sql:= v_sql||' and ';
            end if;
            --
            v_sql:= v_sql||lower(p.column_name)||' = '||  sf_qauto_clie.conv_dato ( v_dtpf.dtpf_dtor, p_tbla, p.column_name)||' ';
            --
        end loop;
        --
        sf_pins_error(v_sql);
        execute immediate v_sql;
        --
    end elimina_datos;
    --
    function conv_dato ( pc_dato varchar2, pc_tabla varchar2, pc_col varchar2 ) return varchar2 is
        cursor c_tipo is
        select data_type
          from all_tab_columns
         where table_name = pc_tabla
           and column_name = pc_col;
        --
        v_tipo  varchar2(50);
        v_dato  varchar2(4000);
        --
    begin
        --
        open c_tipo;
        fetch c_tipo into v_tipo;
        close c_tipo;
        --
        if ( v_tipo = 'VARCHAR2' ) then
            --
            v_dato := ''''||pc_dato||'''';
            --
        elsif ( v_tipo = 'DATE' ) then
            --
            v_dato := 'to_date('''||pc_dato||''')';-- 1007
        else
            --
            v_dato := pc_dato;
            --
        end if;
        --
        return v_dato;
        --
    end conv_dato;
    --
    procedure actualiza_estado ( p_estado varchar2,
                                 p_observ varchar2 ) is 
        pragma autonomous_transaction;
    begin
        --
        update sf_tprfd
           set prfd_esta = p_estado,
               prfd_obser = p_observ,
               prfd_fecmod = sysdate,
               prfd_usuamod = user
         where prfd_prfd = sf_qauto_clie.get_proceso;
        --
        commit;
        --
    end actualiza_estado;
    --
    procedure valida_proceso_pendiente(  p_fdei                 sf_tfdei.fdei_fdei%type
                                        ,p_prfd_prfd    out     sf_tprfd.prfd_prfd%type          
                                        ,p_ty_erro      out     ge_qtipo.tr_error       )is
        cursor c_prfd is
        select prfd_prfd
          from sf_tprfd
         where prfd_fdei = p_fdei
           and prfd_esta = 'I';
           
        v_prfd varchar2(1);
    begin
        p_ty_erro.cod_error:='OK';
        --
        open  c_prfd;
        fetch c_prfd into p_prfd_prfd;
        close c_prfd;
        --
    exception 
        when others then 

            p_ty_erro.msg_error:='Error valida_proceso_pendiente '||sqlerrm;
            p_ty_erro.cod_error:='ERROR';
    
    end valida_proceso_pendiente;
    procedure valida_registros_fdei (   p_fdei_old          in      sf_tfdei%rowtype
                                       ,p_fdei_new          in      sf_tfdei%rowtype
                                       ,p_num_reg           out     number
                                       ,p_ty_erro           out     ge_qtipo.tr_error) is
    --
    v_num_reg number;                                   
    --
    begin
        v_num_reg:=0;
        p_ty_erro.cod_error:='OK';
        --
    if nvl(TO_CHAR(p_fdei_old.fdei_nombre         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nombre          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nombre           '||p_fdei_new.fdei_nombre         ||'-- p_fdei_old.fdei_nombre           '||p_fdei_old.fdei_nombre         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_apell1         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_apell1          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_apell1           '||p_fdei_new.fdei_apell1         ||'-- p_fdei_old.fdei_apell1           '||p_fdei_old.fdei_apell1         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_apell2         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_apell2          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_apell2           '||p_fdei_new.fdei_apell2         ||'-- p_fdei_old.fdei_apell2           '||p_fdei_old.fdei_apell2         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_auxi           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_auxi            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_auxi             '||p_fdei_new.fdei_auxi           ||'-- p_fdei_old.fdei_auxi             '||p_fdei_old.fdei_auxi           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tpid           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tpid            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tpid             '||p_fdei_new.fdei_tpid           ||'-- p_fdei_old.fdei_tpid             '||p_fdei_old.fdei_tpid           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciud_exp       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciud_exp        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciud_exp         '||p_fdei_new.fdei_ciud_exp       ||'-- p_fdei_old.fdei_ciud_exp         '||p_fdei_old.fdei_ciud_exp       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fecexp         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fecexp          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fecexp           '||p_fdei_new.fdei_fecexp         ||'-- p_fdei_old.fdei_fecexp           '||p_fdei_old.fdei_fecexp         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fecnac         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fecnac          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fecnac           '||p_fdei_new.fdei_fecnac         ||'-- p_fdei_old.fdei_fecnac           '||p_fdei_old.fdei_fecnac         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciud_nac       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciud_nac        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciud_nac         '||p_fdei_new.fdei_ciud_nac       ||'-- p_fdei_old.fdei_ciud_nac         '||p_fdei_old.fdei_ciud_nac       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fisexo         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fisexo          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fisexo           '||p_fdei_new.fdei_fisexo         ||'-- p_fdei_old.fdei_fisexo           '||p_fdei_old.fdei_fisexo         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_estciv         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_estciv          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_estciv           '||p_fdei_new.fdei_estciv         ||'-- p_fdei_old.fdei_estciv           '||p_fdei_old.fdei_estciv         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nivest         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nivest          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nivest           '||p_fdei_new.fdei_nivest         ||'-- p_fdei_old.fdei_nivest           '||p_fdei_old.fdei_nivest         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_prof           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_prof            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_prof             '||p_fdei_new.fdei_prof           ||'-- p_fdei_old.fdei_prof             '||p_fdei_old.fdei_prof           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_email          ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_email           ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_email            '||p_fdei_new.fdei_email          ||'-- p_fdei_old.fdei_email            '||p_fdei_old.fdei_email          ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_acargo         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_acargo          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_acargo           '||p_fdei_new.fdei_acargo         ||'-- p_fdei_old.fdei_acargo           '||p_fdei_old.fdei_acargo         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_numhij         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_numhij          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_numhij           '||p_fdei_new.fdei_numhij         ||'-- p_fdei_old.fdei_numhij           '||p_fdei_old.fdei_numhij         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciud_dom       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciud_dom        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciud_dom         '||p_fdei_new.fdei_ciud_dom       ||'-- p_fdei_old.fdei_ciud_dom         '||p_fdei_old.fdei_ciud_dom       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_dire_dom       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_dire_dom        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_dire_dom         '||p_fdei_new.fdei_dire_dom       ||'-- p_fdei_old.fdei_dire_dom         '||p_fdei_old.fdei_dire_dom       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tele_dom       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tele_dom        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tele_dom         '||p_fdei_new.fdei_tele_dom       ||'-- p_fdei_old.fdei_tele_dom         '||p_fdei_old.fdei_tele_dom       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nrofax         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nrofax          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nrofax           '||p_fdei_new.fdei_nrofax         ||'-- p_fdei_old.fdei_nrofax           '||p_fdei_old.fdei_nrofax         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_celula         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_celula          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_celula           '||p_fdei_new.fdei_celula         ||'-- p_fdei_old.fdei_celula           '||p_fdei_old.fdei_celula         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ocupac         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ocupac          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ocupac           '||p_fdei_new.fdei_ocupac         ||'-- p_fdei_old.fdei_ocupac           '||p_fdei_old.fdei_ocupac         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_person         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_person          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_person           '||p_fdei_new.fdei_person         ||'-- p_fdei_old.fdei_person           '||p_fdei_old.fdei_person         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciua           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciua            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciua             '||p_fdei_new.fdei_ciua           ||'-- p_fdei_old.fdei_ciua             '||p_fdei_old.fdei_ciua           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ingres         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ingres          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ingres           '||p_fdei_new.fdei_ingres         ||'-- p_fdei_old.fdei_ingres           '||p_fdei_old.fdei_ingres         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_egreso         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_egreso          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_egreso           '||p_fdei_new.fdei_egreso         ||'-- p_fdei_old.fdei_egreso           '||p_fdei_old.fdei_egreso         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_impusa         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_impusa          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_impusa           '||p_fdei_new.fdei_impusa         ||'-- p_fdei_old.fdei_impusa           '||p_fdei_old.fdei_impusa         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nombre_emp     ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nombre_emp      ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nombre_emp       '||p_fdei_new.fdei_nombre_emp     ||'-- p_fdei_old.fdei_nombre_emp       '||p_fdei_old.fdei_nombre_emp     ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nit_emp        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nit_emp         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nit_emp          '||p_fdei_new.fdei_nit_emp        ||'-- p_fdei_old.fdei_nit_emp          '||p_fdei_old.fdei_nit_emp        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_dire_emp       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_dire_emp        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_dire_emp         '||p_fdei_new.fdei_dire_emp       ||'-- p_fdei_old.fdei_dire_emp         '||p_fdei_old.fdei_dire_emp       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tele_emp       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tele_emp        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tele_emp         '||p_fdei_new.fdei_tele_emp       ||'-- p_fdei_old.fdei_tele_emp         '||p_fdei_old.fdei_tele_emp       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciud_emp       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciud_emp        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciud_emp         '||p_fdei_new.fdei_ciud_emp       ||'-- p_fdei_old.fdei_ciud_emp         '||p_fdei_old.fdei_ciud_emp       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciua_emp       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciua_emp        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciua_emp         '||p_fdei_new.fdei_ciua_emp       ||'-- p_fdei_old.fdei_ciua_emp         '||p_fdei_old.fdei_ciua_emp       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fecvin         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fecvin          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fecvin           '||p_fdei_new.fdei_fecvin         ||'-- p_fdei_old.fdei_fecvin           '||p_fdei_old.fdei_fecvin         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_cargo          ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_cargo           ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_cargo            '||p_fdei_new.fdei_cargo          ||'-- p_fdei_old.fdei_cargo            '||p_fdei_old.fdei_cargo          ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_orig_recu      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_orig_recu       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_orig_recu        '||p_fdei_new.fdei_orig_recu      ||'-- p_fdei_old.fdei_orig_recu        '||p_fdei_old.fdei_orig_recu      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nomb_repleg    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nomb_repleg     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nomb_repleg      '||p_fdei_new.fdei_nomb_repleg    ||'-- p_fdei_old.fdei_nomb_repleg      '||p_fdei_old.fdei_nomb_repleg    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tele_repleg    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tele_repleg     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tele_repleg      '||p_fdei_new.fdei_tele_repleg    ||'-- p_fdei_old.fdei_tele_repleg      '||p_fdei_old.fdei_tele_repleg    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_celu_repleg    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_celu_repleg     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_celu_repleg      '||p_fdei_new.fdei_celu_repleg    ||'-- p_fdei_old.fdei_celu_repleg      '||p_fdei_old.fdei_celu_repleg    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fax_repleg     ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fax_repleg      ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fax_repleg       '||p_fdei_new.fdei_fax_repleg     ||'-- p_fdei_old.fdei_fax_repleg       '||p_fdei_old.fdei_fax_repleg     ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_email_repleg   ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_email_repleg    ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_email_repleg     '||p_fdei_new.fdei_email_repleg   ||'-- p_fdei_old.fdei_email_repleg     '||p_fdei_old.fdei_email_repleg   ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_num_escri      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_num_escri       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_num_escri        '||p_fdei_new.fdei_num_escri      ||'-- p_fdei_old.fdei_num_escri        '||p_fdei_old.fdei_num_escri      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_notaria        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_notaria         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_notaria          '||p_fdei_new.fdei_notaria        ||'-- p_fdei_old.fdei_notaria          '||p_fdei_old.fdei_notaria        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciud_notaria   ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciud_notaria    ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciud_notaria     '||p_fdei_new.fdei_ciud_notaria   ||'-- p_fdei_old.fdei_ciud_notaria     '||p_fdei_old.fdei_ciud_notaria   ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_activos        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_activos         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_activos          '||p_fdei_new.fdei_activos        ||'-- p_fdei_old.fdei_activos          '||p_fdei_old.fdei_activos        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_pasivos        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_pasivos         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_pasivos          '||p_fdei_new.fdei_pasivos        ||'-- p_fdei_old.fdei_pasivos          '||p_fdei_old.fdei_pasivos        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_matric_merc    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_matric_merc     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_matric_merc      '||p_fdei_new.fdei_matric_merc    ||'-- p_fdei_old.fdei_matric_merc      '||p_fdei_old.fdei_matric_merc    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tpo_empresa    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tpo_empresa     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tpo_empresa      '||p_fdei_new.fdei_tpo_empresa    ||'-- p_fdei_old.fdei_tpo_empresa      '||p_fdei_old.fdei_tpo_empresa    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_dir_corresp    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_dir_corresp     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_dir_corresp      '||p_fdei_new.fdei_dir_corresp    ||'-- p_fdei_old.fdei_dir_corresp      '||p_fdei_old.fdei_dir_corresp    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_exento_conesp  ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_exento_conesp   ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_exento_conesp    '||p_fdei_new.fdei_exento_conesp  ||'-- p_fdei_old.fdei_exento_conesp    '||p_fdei_old.fdei_exento_conesp  ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_sgmr           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_sgmr            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_sgmr             '||p_fdei_new.fdei_sgmr           ||'-- p_fdei_old.fdei_sgmr             '||p_fdei_old.fdei_sgmr           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tpcl           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tpcl            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tpcl             '||p_fdei_new.fdei_tpcl           ||'-- p_fdei_old.fdei_tpcl             '||p_fdei_old.fdei_tpcl           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_stat           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_stat            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_stat             '||p_fdei_new.fdei_stat           ||'-- p_fdei_old.fdei_stat             '||p_fdei_old.fdei_stat           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fecact         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fecact          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fecact           '||p_fdei_new.fdei_fecact         ||'-- p_fdei_old.fdei_fecact           '||p_fdei_old.fdei_fecact         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciud_repleg    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciud_repleg     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciud_repleg      '||p_fdei_new.fdei_ciud_repleg    ||'-- p_fdei_old.fdei_ciud_repleg      '||p_fdei_old.fdei_ciud_repleg    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_dire_repleg    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_dire_repleg     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_dire_repleg      '||p_fdei_new.fdei_dire_repleg    ||'-- p_fdei_old.fdei_dire_repleg      '||p_fdei_old.fdei_dire_repleg    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nucx           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nucx            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nucx             '||p_fdei_new.fdei_nucx           ||'-- p_fdei_old.fdei_nucx             '||p_fdei_old.fdei_nucx           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_retef          ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_retef           ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_retef            '||p_fdei_new.fdei_retef          ||'-- p_fdei_old.fdei_retef            '||p_fdei_old.fdei_retef          ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_gere_pd        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_gere_pd         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_gere_pd          '||p_fdei_new.fdei_gere_pd        ||'-- p_fdei_old.fdei_gere_pd          '||p_fdei_old.fdei_gere_pd        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_gere_sg        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_gere_sg         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_gere_sg          '||p_fdei_new.fdei_gere_sg        ||'-- p_fdei_old.fdei_gere_sg          '||p_fdei_old.fdei_gere_sg        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_rin            ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_rin             ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_rin              '||p_fdei_new.fdei_rin            ||'-- p_fdei_old.fdei_rin              '||p_fdei_old.fdei_rin            ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ofic           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ofic            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ofic             '||p_fdei_new.fdei_ofic           ||'-- p_fdei_old.fdei_ofic             '||p_fdei_old.fdei_ofic           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_qdire          ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_qdire           ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_qdire            '||p_fdei_new.fdei_qdire          ||'-- p_fdei_old.fdei_qdire            '||p_fdei_old.fdei_qdire          ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tpbn           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tpbn            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tpbn             '||p_fdei_new.fdei_tpbn           ||'-- p_fdei_old.fdei_tpbn             '||p_fdei_old.fdei_tpbn           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_exonerado      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_exonerado       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_exonerado        '||p_fdei_new.fdei_exonerado      ||'-- p_fdei_old.fdei_exonerado        '||p_fdei_old.fdei_exonerado      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nombre1        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nombre1         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nombre1          '||p_fdei_new.fdei_nombre1        ||'-- p_fdei_old.fdei_nombre1          '||p_fdei_old.fdei_nombre1        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nombre2        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nombre2         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nombre2          '||p_fdei_new.fdei_nombre2        ||'-- p_fdei_old.fdei_nombre2          '||p_fdei_old.fdei_nombre2        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_email_emp      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_email_emp       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_email_emp        '||p_fdei_new.fdei_email_emp      ||'-- p_fdei_old.fdei_email_emp        '||p_fdei_old.fdei_email_emp      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ingres_otros   ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ingres_otros    ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ingres_otros     '||p_fdei_new.fdei_ingres_otros   ||'-- p_fdei_old.fdei_ingres_otros     '||p_fdei_old.fdei_ingres_otros   ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_concepto_otin  ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_concepto_otin   ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_concepto_otin    '||p_fdei_new.fdei_concepto_otin  ||'-- p_fdei_old.fdei_concepto_otin    '||p_fdei_old.fdei_concepto_otin  ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_hobbie         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_hobbie          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_hobbie           '||p_fdei_new.fdei_hobbie         ||'-- p_fdei_old.fdei_hobbie           '||p_fdei_old.fdei_hobbie         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_cono_emp       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_cono_emp        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_cono_emp         '||p_fdei_new.fdei_cono_emp       ||'-- p_fdei_old.fdei_cono_emp         '||p_fdei_old.fdei_cono_emp       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tpdo_repleg    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tpdo_repleg     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tpdo_repleg      '||p_fdei_new.fdei_tpdo_repleg    ||'-- p_fdei_old.fdei_tpdo_repleg      '||p_fdei_old.fdei_tpdo_repleg    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nudo_repleg    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nudo_repleg     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nudo_repleg      '||p_fdei_new.fdei_nudo_repleg    ||'-- p_fdei_old.fdei_nudo_repleg      '||p_fdei_old.fdei_nudo_repleg    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nombre1_repleg ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nombre1_repleg  ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nombre1_repleg   '||p_fdei_new.fdei_nombre1_repleg ||'-- p_fdei_old.fdei_nombre1_repleg   '||p_fdei_old.fdei_nombre1_repleg ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nombre2_repleg ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nombre2_repleg  ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nombre2_repleg   '||p_fdei_new.fdei_nombre2_repleg ||'-- p_fdei_old.fdei_nombre2_repleg   '||p_fdei_old.fdei_nombre2_repleg ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_apell1_repleg  ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_apell1_repleg   ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_apell1_repleg    '||p_fdei_new.fdei_apell1_repleg  ||'-- p_fdei_old.fdei_apell1_repleg    '||p_fdei_old.fdei_apell1_repleg  ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_apell2_repleg  ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_apell2_repleg   ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_apell2_repleg    '||p_fdei_new.fdei_apell2_repleg  ||'-- p_fdei_old.fdei_apell2_repleg    '||p_fdei_old.fdei_apell2_repleg  ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_sexo_repleg    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_sexo_repleg     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_sexo_repleg      '||p_fdei_new.fdei_sexo_repleg    ||'-- p_fdei_old.fdei_sexo_repleg      '||p_fdei_old.fdei_sexo_repleg    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_opin           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_opin            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_opin             '||p_fdei_new.fdei_opin           ||'-- p_fdei_old.fdei_opin             '||p_fdei_old.fdei_opin           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_enti_opin      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_enti_opin       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_enti_opin        '||p_fdei_new.fdei_enti_opin      ||'-- p_fdei_old.fdei_enti_opin        '||p_fdei_old.fdei_enti_opin      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_nucu_opin      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_nucu_opin       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_nucu_opin        '||p_fdei_new.fdei_nucu_opin      ||'-- p_fdei_old.fdei_nucu_opin        '||p_fdei_old.fdei_nucu_opin      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_mone_opin      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_mone_opin       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_mone_opin        '||p_fdei_new.fdei_mone_opin      ||'-- p_fdei_old.fdei_mone_opin        '||p_fdei_old.fdei_mone_opin      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_obse_opin      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_obse_opin       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_obse_opin        '||p_fdei_new.fdei_obse_opin      ||'-- p_fdei_old.fdei_obse_opin        '||p_fdei_old.fdei_obse_opin      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_clase_trib     ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_clase_trib      ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_clase_trib       '||p_fdei_new.fdei_clase_trib     ||'-- p_fdei_old.fdei_clase_trib       '||p_fdei_old.fdei_clase_trib     ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_pais_opin      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_pais_opin       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_pais_opin        '||p_fdei_new.fdei_pais_opin      ||'-- p_fdei_old.fdei_pais_opin        '||p_fdei_old.fdei_pais_opin      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciud_opin      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciud_opin       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciud_opin        '||p_fdei_new.fdei_ciud_opin      ||'-- p_fdei_old.fdei_ciud_opin        '||p_fdei_old.fdei_ciud_opin      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fech_nac_repleg), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fech_nac_repleg ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fech_nac_repleg  '||p_fdei_new.fdei_fech_nac_repleg||'-- p_fdei_old.fdei_fech_nac_repleg  '||p_fdei_old.fdei_fech_nac_repleg); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciud_nac_repleg), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciud_nac_repleg ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciud_nac_repleg  '||p_fdei_new.fdei_ciud_nac_repleg||'-- p_fdei_old.fdei_ciud_nac_repleg  '||p_fdei_old.fdei_ciud_nac_repleg); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_feccdat        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_feccdat         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_feccdat          '||p_fdei_new.fdei_feccdat        ||'-- p_fdei_old.fdei_feccdat          '||p_fdei_old.fdei_feccdat        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_usuacdat       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_usuacdat        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_usuacdat         '||p_fdei_new.fdei_usuacdat       ||'-- p_fdei_old.fdei_usuacdat         '||p_fdei_old.fdei_usuacdat       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_num_emp        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_num_emp         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_num_emp          '||p_fdei_new.fdei_num_emp        ||'-- p_fdei_old.fdei_num_emp          '||p_fdei_old.fdei_num_emp        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_soc_vig_hasta  ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_soc_vig_hasta   ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_soc_vig_hasta    '||p_fdei_new.fdei_soc_vig_hasta  ||'-- p_fdei_old.fdei_soc_vig_hasta    '||p_fdei_old.fdei_soc_vig_hasta  ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_orrc           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_orrc            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_orrc             '||p_fdei_new.fdei_orrc           ||'-- p_fdei_old.fdei_orrc             '||p_fdei_old.fdei_orrc           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_exento         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_exento          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_exento           '||p_fdei_new.fdei_exento         ||'-- p_fdei_old.fdei_exento           '||p_fdei_old.fdei_exento         ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_verif_sip      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_verif_sip       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_verif_sip        '||p_fdei_new.fdei_verif_sip      ||'-- p_fdei_old.fdei_verif_sip        '||p_fdei_old.fdei_verif_sip      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_usr_sip        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_usr_sip         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_usr_sip          '||p_fdei_new.fdei_usr_sip        ||'-- p_fdei_old.fdei_usr_sip          '||p_fdei_old.fdei_usr_sip        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fecver_sip     ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fecver_sip      ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fecver_sip       '||p_fdei_new.fdei_fecver_sip     ||'-- p_fdei_old.fdei_fecver_sip       '||p_fdei_old.fdei_fecver_sip     ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_prod           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_prod            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_prod             '||p_fdei_new.fdei_prod           ||'-- p_fdei_old.fdei_prod             '||p_fdei_old.fdei_prod           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fec_exon       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fec_exon        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fec_exon         '||p_fdei_new.fdei_fec_exon       ||'-- p_fdei_old.fdei_fec_exon         '||p_fdei_old.fdei_fec_exon       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_exon           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_exon            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_exon             '||p_fdei_new.fdei_exon           ||'-- p_fdei_old.fdei_exon             '||p_fdei_old.fdei_exon           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_exon_observ    ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_exon_observ     ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_exon_observ      '||p_fdei_new.fdei_exon_observ    ||'-- p_fdei_old.fdei_exon_observ      '||p_fdei_old.fdei_exon_observ    ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_sgcl           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_sgcl            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_sgcl             '||p_fdei_new.fdei_sgcl           ||'-- p_fdei_old.fdei_sgcl             '||p_fdei_old.fdei_sgcl           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_pais_nac       ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_pais_nac        ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_pais_nac         '||p_fdei_new.fdei_pais_nac       ||'-- p_fdei_old.fdei_pais_nac         '||p_fdei_old.fdei_pais_nac       ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_opme           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_opme            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_opme             '||p_fdei_new.fdei_opme           ||'-- p_fdei_old.fdei_opme             '||p_fdei_old.fdei_opme           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tppr           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tppr            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tppr             '||p_fdei_new.fdei_tppr           ||'-- p_fdei_old.fdei_tppr             '||p_fdei_old.fdei_tppr           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_monto_prod     ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_monto_prod      ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_monto_prod       '||p_fdei_new.fdei_monto_prod     ||'-- p_fdei_old.fdei_monto_prod       '||p_fdei_old.fdei_monto_prod     ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_pais_nac_repleg), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_pais_nac_repleg ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_pais_nac_repleg  '||p_fdei_new.fdei_pais_nac_repleg||'-- p_fdei_old.fdei_pais_nac_repleg  '||p_fdei_old.fdei_pais_nac_repleg); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_ciud_exp_repleg), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_ciud_exp_repleg ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_ciud_exp_repleg  '||p_fdei_new.fdei_ciud_exp_repleg||'-- p_fdei_old.fdei_ciud_exp_repleg  '||p_fdei_old.fdei_ciud_exp_repleg); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_fech_exp_repleg), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_fech_exp_repleg ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_fech_exp_repleg  '||p_fdei_new.fdei_fech_exp_repleg||'-- p_fdei_old.fdei_fech_exp_repleg  '||p_fdei_old.fdei_fech_exp_repleg); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_sect           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_sect            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_sect             '||p_fdei_new.fdei_sect           ||'-- p_fdei_old.fdei_sect             '||p_fdei_old.fdei_sect           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_extr_elect     ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_extr_elect      ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_extr_elect       '||p_fdei_new.fdei_extr_elect     ||'-- p_fdei_old.fdei_extr_elect       '||p_fdei_old.fdei_extr_elect     ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_pais_cons      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_pais_cons       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_pais_cons        '||p_fdei_new.fdei_pais_cons      ||'-- p_fdei_old.fdei_pais_cons        '||p_fdei_old.fdei_pais_cons      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_giin           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_giin            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_giin             '||p_fdei_new.fdei_giin           ||'-- p_fdei_old.fdei_giin             '||p_fdei_old.fdei_giin           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_sponsor        ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_sponsor         ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_sponsor          '||p_fdei_new.fdei_sponsor        ||'-- p_fdei_old.fdei_sponsor          '||p_fdei_old.fdei_sponsor        ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_giin_sponsor   ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_giin_sponsor    ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_giin_sponsor     '||p_fdei_new.fdei_giin_sponsor   ||'-- p_fdei_old.fdei_giin_sponsor     '||p_fdei_old.fdei_giin_sponsor   ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_aext_email     ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_aext_email      ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_aext_email       '||p_fdei_new.fdei_aext_email     ||'-- p_fdei_old.fdei_aext_email       '||p_fdei_old.fdei_aext_email     ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_lnds           ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_lnds            ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_lnds             '||p_fdei_new.fdei_lnds           ||'-- p_fdei_old.fdei_lnds             '||p_fdei_old.fdei_lnds           ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_pep            ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_pep             ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_pep              '||p_fdei_new.fdei_pep            ||'-- p_fdei_old.fdei_pep              '||p_fdei_old.fdei_pep            ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_tpcl_banc      ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_tpcl_banc       ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_tpcl_banc        '||p_fdei_new.fdei_tpcl_banc      ||'-- p_fdei_old.fdei_tpcl_banc        '||p_fdei_old.fdei_tpcl_banc      ); 
    end if;
    if nvl(TO_CHAR(p_fdei_old.fdei_pep_pj         ), 'XX-1X')<>nvl(TO_CHAR(p_fdei_new.fdei_pep_pj          ), 'XX-1X') then
        v_num_reg:=   v_num_reg+1;
        sf_pins_error('valida_ori_nvo- p_fdei_new.fdei_pep_pj           '||p_fdei_new.fdei_pep_pj         ||'-- p_fdei_old.fdei_pep_pj           '||p_fdei_old.fdei_pep_pj         ); 
    end if;
        --
        p_num_reg:= v_num_reg;
    exception
        when others then 

            p_ty_erro.msg_error:='Error valida_registros_fdei '||sqlerrm;
            p_ty_erro.cod_error:='ERROR';
    end valida_registros_fdei;
    --
    function valida_tipo_dato(  p_table     varchar2
                               ,p_columna   varchar2) return varchar2 is
        Cursor c_tpo is     select  data_type
                            from    all_tab_columns
                            where   table_name = p_table
                            and     column_name = p_columna;
        v_tipo      varchar2(20);

    begin
        --
        open c_tpo;
        fetch c_tpo into v_tipo;
        close c_tpo;
        --
        return nvl(v_tipo,'VARCHAR2');
        --
    end valida_tipo_dato; 
    --
    procedure set_tppr( p_tppr  varchar2 ) is
       begin
           --
           vg_tppr:= p_tppr;
           --
       end set_tppr;
       --
    function get_tppr  return varchar2 is
       begin
           --
           return vg_tppr;
           --
       end get_tppr;

    procedure insertar_cliente_auxil   (    p_auxi        ge_tauxil%rowtype
                                           ,p_ty_erro out ge_qtipo.tr_error
                                    ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        v_num_reg    number;
        --
    begin
        --

        --

        v_prfd := validar_proceso  ( p_auxi.auxi_clase, p_auxi.AUXI_NIT, p_auxi.auxi_user );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_AUXI'            , null, p_auxi.auxi_auxi            , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_CLASE'           , null, p_auxi.auxi_clase           , null, p_auxi.auxi_auxi, null, 'I' );       --
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_NIT'             , null, p_auxi.auxi_nit             , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_DIGITO'          , null, p_auxi.auxi_digito          , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_APELL1'          , null, p_auxi.auxi_apell1          , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_APELL2'          , null, p_auxi.auxi_apell2          , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_NOMBRE1'         , null, p_auxi.auxi_nombre1         , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_NOMBRE2'         , null, p_auxi.auxi_nombre2         , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_TIPO'            , null, p_auxi.auxi_tipo            , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_NATU'            , null, p_auxi.auxi_natu            , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_TPRET'           , null, p_auxi.auxi_tpret           , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_CIUD'            , null, p_auxi.auxi_ciud            , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_DIRE'            , null, p_auxi.auxi_dire            , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_TELE'            , null, p_auxi.auxi_tele            , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_FECCRE'          , null, p_auxi.auxi_feccre          , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_USER'            , null, p_auxi.auxi_user            , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_STATUS'          , null, p_auxi.auxi_status          , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_DESCRI'          , null, p_auxi.auxi_descri          , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_CIUA'            , null, p_auxi.auxi_ciua            , null, p_auxi.auxi_auxi, null, 'I' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_EMAIL'           , null, p_auxi.auxi_email           , null, p_auxi.auxi_auxi, null, 'I' );

        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';

        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.insertar_cliente_auxil.'||sqlerrm;
            --
    end insertar_cliente_auxil;
    --
    procedure actualiza_cliente_auxil   (    p_auxil_old        ge_tauxil%rowtype
                                            ,p_auxil_new        ge_tauxil%rowtype
                                            ,p_ty_erro out ge_qtipo.tr_error
                                    ) is
        --
        p_ty_dtpf    sf_qdtpf_crud.ty_dtpf;
        v_prfd       number;
        v_num_reg    number;
        --
    begin
        --

        --

        v_prfd := validar_proceso( p_auxil_old.auxi_clase, p_auxil_old.AUXI_NIT, p_auxil_new.auxi_user );--1003
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_AUXI'            , p_auxil_old.auxi_auxi   , p_auxil_new.auxi_auxi            , null, p_auxil_old.auxi_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_CLASE'           , p_auxil_old.auxi_clase  , p_auxil_new.auxi_clase           , null, p_auxil_old.auxi_auxi, null, 'A' );       --
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_NIT'             , p_auxil_old.auxi_nit    , p_auxil_new.auxi_nit             , null, p_auxil_old.auxi_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_DIGITO'          , p_auxil_old.auxi_digito , p_auxil_new.auxi_digito          , null, p_auxil_old.auxi_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_APELL1'          , p_auxil_old.auxi_apell1 , p_auxil_new.auxi_apell1          , null, p_auxil_old.auxi_auxi, null, 'A' );-- 1005
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_APELL2'          , p_auxil_old.auxi_apell2 , p_auxil_new.auxi_apell2          , null, p_auxil_old.auxi_auxi, null, 'A' );-- 1005
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_NOMBRE1'         , p_auxil_old.auxi_nombre1, p_auxil_new.auxi_nombre1         , null, p_auxil_old.auxi_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_NOMBRE2'         , p_auxil_old.auxi_nombre2, p_auxil_new.auxi_nombre2         , null, p_auxil_old.auxi_auxi, null, 'A' );-- 1005
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_TIPO'            , p_auxil_old.auxi_tipo   , p_auxil_new.auxi_tipo            , null, p_auxil_old.auxi_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_NATU'            , p_auxil_old.auxi_natu   , p_auxil_new.auxi_natu            , null, p_auxil_old.auxi_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_TPRET'           , p_auxil_old.auxi_tpret  , p_auxil_new.auxi_tpret           , null, p_auxil_old.auxi_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_CIUD'            , p_auxil_old.auxi_ciud   , p_auxil_new.auxi_ciud            , null, p_auxil_old.auxi_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_DIRE'            , p_auxil_old.auxi_dire   , p_auxil_new.auxi_dire            , null, p_auxil_old.auxi_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_TELE'            , p_auxil_old.auxi_tele   , p_auxil_new.auxi_tele            , null, p_auxil_old.auxi_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_FECCRE'          , p_auxil_old.auxi_feccre , p_auxil_new.auxi_feccre          , null, p_auxil_old.auxi_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_USER'            , p_auxil_old.auxi_user   , p_auxil_new.auxi_user            , null, p_auxil_old.auxi_auxi, null, 'A' );
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_STATUS'          , p_auxil_old.auxi_status , p_auxil_new.auxi_status          , null, p_auxil_old.auxi_auxi, null, 'A' );
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_DESCRI'          , p_auxil_old.auxi_descri , p_auxil_new.auxi_descri          , null, p_auxil_old.auxi_auxi, null, 'A' ); -- 1005
        sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_CIUA'            , p_auxil_old.auxi_ciua   , p_auxil_new.auxi_ciua            , null, p_auxil_old.auxi_auxi, null, 'A' ); -- 1005
        --sf_qauto_clie.mapear_campos( v_prfd,'GE_TAUXIL', 'AUXI_EMAIL'           , p_auxil_old.auxi_email  , p_auxil_new.auxi_email           , null, p_auxil_old.auxi_auxi, null, 'A' );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Mapeo Correcto';

        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.actualiza_cliente_auxil.'||sqlerrm;
            --
    end actualiza_cliente_auxil;
    -- ini 1007
    procedure inserta_temporal_date    (    p_campo       varchar2
                                           ,p_fecha       date
                                           ,p_ty_erro out ge_qtipo.tr_error
                                    ) is 
    begin
        P_ty_erro.cod_error := 'OK';
        P_ty_erro.msg_error := 'SF_QAUTO_CLIE.inserta_temporal_date.'||sqlerrm;

        insert into sf_ttdat
        (tdat_cmpo,  tdat_date)
        values
        (p_campo,p_fecha     );
            --
    EXCEPTION
        WHEN OTHERS THEN
        --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'SF_QAUTO_CLIE.inserta_temporal_date.'||sqlerrm; 
        --            
    end inserta_temporal_date;
    --
    function consulta_temporal_date (     p_campo varchar2 ) return varchar2 is
            
        cursor c_date (cp_campo varchar2) is    select tdat_date 
                                               from sf_ttdat 
                                               where tdat_cmpo=cp_campo;
        --
        v_date     date;   
        --
        begin
        --
            open  c_date(p_campo);
            fetch c_date into v_date;
            close c_date;
        --
            return to_char(v_date,'DD-Mon-RRRR');
        end;
        
    -- fin 1007
end SF_QAUTO_CLIE;
/