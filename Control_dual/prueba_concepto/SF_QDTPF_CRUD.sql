prompt
prompt package SF_QDTPF_CRUD
prompt
create or replace package SF_QDTPF_CRUD as
    --
    -- #VERSION:0000001000
    --
    -- HISTORIAL DE CAMBIOS
    --
    -- Versi�n     GAP           Solicitud        Fecha        Realiz�                          Descripci�n
    -- =========== ============= ================ ============ ================================ ==============================================================================
    -- 1000                      80577            28/03/2019   Inaction                         .Se crea paquete CRUD detalle proceso fideicomitente (SF_TDTPF)
    --
    -- =========== ============= ================ ============ ================================ ==============================================================================
    --
    type ty_dtpf is record  (  dtpf_dtpf  sf_tdtpf.dtpf_dtpf%type
                             , dtpf_prfd  sf_tdtpf.dtpf_prfd%type
                             , dtpf_tbla  sf_tdtpf.dtpf_tbla%type
                             , dtpf_cmpo  sf_tdtpf.dtpf_cmpo%type
                             , dtpf_dtor  sf_tdtpf.dtpf_dtor%type
                             , dtpf_dtnv  sf_tdtpf.dtpf_dtnv%type
                             , dtpf_fdei  sf_tdtpf.dtpf_fdei%type
                             , dtpf_idnu  sf_tdtpf.dtpf_idnu%type
                             , dtpf_idvc  sf_tdtpf.dtpf_idvc%type
                             , dtpf_tptr  sf_tdtpf.dtpf_tptr%type
                            );
    --
    ----------------------------------------------------------------------------------
    --
    type rc_ty_dtpf is ref cursor return ty_dtpf;
    --
    ----------------------------------------------------------------------------------
    --
    procedure insertar_sf_tdtpf(  p_ty_dtpf      in out  ty_dtpf
                                , p_ty_erro      out     ge_qtipo.tr_error
                               );
    --
    ----------------------------------------------------------------------------------
    --
    procedure consulta_sf_tdtpf (  p_ty_dtpf     in  ty_dtpf
                                 , p_rc_ty_dtpf  out rc_ty_dtpf
                                 , p_ty_erro     out ge_qtipo.tr_error
                                );
    --
    ----------------------------------------------------------------------------------
    --
end SF_QDTPF_CRUD;
/
prompt
prompt package body SF_QDTPF_CRUD
prompt
create or replace package body SF_QDTPF_CRUD as
    --
    -- #VERSION:0000001010
    --
    procedure insertar_sf_tdtpf(  p_ty_dtpf      in out  ty_dtpf
                                , p_ty_erro      out     ge_qtipo.tr_error
                               ) is
                               pragma autonomous_transaction;-- 1001
    begin
        --
        ge_psecuencia( 'SF_SDTPF', p_ty_dtpf.dtpf_dtpf , p_ty_erro.msg_error );
        --
        insert into sf_tdtpf (
                               dtpf_dtpf
                              ,dtpf_prfd
                              ,dtpf_tbla
                              ,dtpf_cmpo
                              ,dtpf_dtor
                              ,dtpf_dtnv
                              ,dtpf_fdei
                              ,dtpf_idnu
                              ,dtpf_idvc
                              ,dtpf_tptr
                             )
                      values (
                               p_ty_dtpf.dtpf_dtpf
                              ,p_ty_dtpf.dtpf_prfd
                              ,p_ty_dtpf.dtpf_tbla
                              ,p_ty_dtpf.dtpf_cmpo
                              ,p_ty_dtpf.dtpf_dtor
                              ,p_ty_dtpf.dtpf_dtnv
                              ,p_ty_dtpf.dtpf_fdei
                              ,p_ty_dtpf.dtpf_idnu
                              ,p_ty_dtpf.dtpf_idvc
                              ,p_ty_dtpf.dtpf_tptr
                              );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Inserci�n �xitosa '||sqlerrm;
        --
         commit;
    exception
        when others then
        p_ty_erro.cod_error := 'ERROR';
        p_ty_erro.msg_error := 'Inserci�n erronea insertar_SF_TDTPF: '||sqlerrm;


  end insertar_SF_TDTPF;
    --
    ----------------------------------------------------------------------------------
    --
    procedure consulta_sf_tdtpf (  p_ty_dtpf     in  ty_dtpf
                                 , p_rc_ty_dtpf  out rc_ty_dtpf
                                 , p_ty_erro     out ge_qtipo.tr_error
                                ) is

    begin
        --
        open p_rc_ty_dtpf for select * from SF_TDTPF
        where dtpf_prfd     =   p_ty_dtpf.dtpf_prfd;
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Consulta �xitosa consulta_SF_TDTPF';
        --
    exception 
        when others then
        --
        p_ty_erro.cod_error := 'ERROR';
        p_ty_erro.msg_error := 'Consulta erronea consulta_SF_TDTPF: '||sqlerrm;
        --
    end consulta_SF_TDTPF;
    --
end SF_QDTPF_CRUD;
/