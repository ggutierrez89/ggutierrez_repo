create or replace package  SF_QMDFD_CRUD is
    --
    -- #VERSION:0000001001
    --
    -- Version     GAP           Solicitud        Fecha        Realizo        Descripcion
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1000                      RFC80577         28/03/2019   InAction       Creacion del paquete procedimientos crud de las forma sfmsmdfei  
    -- =========== ============= ================ ============ ============== ============================================================================================================
    -- 1001                      RFC80577         28/03/2019   InAction       se crea procedimiento  consultar_auxil
    -- =========== ============= ================ ============ ============== ============================================================================================================

--   
procedure insertar_fdei (  p_fdei         sf_tfdei%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        );
--
procedure actualizar_fdei (  p_fdei         sf_tfdei%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) ;
--
procedure consultar_fdei (  p_ty_fdei  in out sf_tfdei%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) ;  
--
procedure insertar_drcl (  p_drcl         sf_tdrcl%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) ; 
--                        
procedure actualizar_drcl (  p_drcl         sf_tdrcl%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ); 
--
procedure consultar_drcl (  p_ty_drcl  in out sf_tdrcl%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         );
procedure insertar_apfd (  p_apfd         sf_tapfd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) ;
--
procedure actualizar_apfd (  p_apfd         sf_tapfd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) ;
--
procedure consultar_apfd (  p_ty_apfd  in out sf_tapfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) ;
 --
procedure insertar_rffd (  p_rffd         sf_trffd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ); 
 --
 procedure actualizar_rffd (  p_rffd         sf_trffd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          );
 --
 procedure consultar_rffd (  p_ty_rffd  in out sf_trffd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         );
 --
 procedure insertar_cnfd (  p_cnfd         sf_tcnfd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        );
 --
 procedure actualizar_cnfd (  p_cnfd         sf_tcnfd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) ;
-- 
procedure consultar_cnfd (  p_ty_cnfd  in out sf_tcnfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         );
--
procedure insertar_soci (  p_soci         sf_tsoci%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) ;
--
procedure actualizar_soci (  p_soci         sf_tsoci%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          );
--
procedure consultar_soci (  p_ty_soci  in out sf_tsoci%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) ;
--
procedure insertar_fsuc (  p_fsuc         sf_tfsuc%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        );
--
procedure actualizar_fsuc (  p_fsuc         sf_tfsuc%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) ;
--   
procedure consultar_fsuc (  p_ty_fsuc  in out sf_tfsuc%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) ;
--
procedure insertar_admi (  p_admi         sf_tadmi%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        );
--   
procedure actualizar_admi (  p_admi         sf_tadmi%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) ;
--       
procedure consultar_admi (  p_ty_admi  in out sf_tadmi%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         );
--
procedure insertar_frau (  p_frau         sf_tfrau%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) ;
--
procedure actualizar_frau (  p_frau         sf_tfrau%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          );
--           
procedure consultar_frau (  p_ty_frau  in out sf_tfrau%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         );
--
procedure insertar_emfd (  p_emfd         sf_temfd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) ;
--
procedure actualizar_emfd (  p_emfd         sf_temfd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          );
--
procedure consultar_emfd (  p_ty_emfd  in out sf_temfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         );
--
procedure insertar_dcfd (  p_dcfd         sf_tdcfd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) ;
--
procedure actualizar_dcfd (  p_dcfd         sf_tdcfd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) ;
--    
procedure consultar_dcfd (  p_ty_dcfd  in out sf_tdcfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         );
--
procedure insertar_prcg (  p_prcg         sf_tprcg%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) ;
--
 procedure actualizar_prcg (  p_prcg         sf_tprcg%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) ;
--
procedure consultar_prcg (  p_ty_prcg  in out sf_tprcg%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) ; 
--
procedure elimina_dcfd (  p_ty_dcfd  in out sf_tdcfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) ;
--
-- ini 1001
procedure consultar_auxil (  p_ty_auxil  in out ge_tauxil%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         )  ;
--    fin 1001                 
end SF_QMDFD_CRUD;
/

create or replace package body SF_QMDFD_CRUD is
    --
    -- #VERSION:0000001001
    --
procedure insertar_fdei (  p_fdei         sf_tfdei%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tfdei (  fdei_dir_corresp
                          , fdei_feccre
                          , fdei_usua
                          , fdei_exento_conesp
                          , fdei_sgmr
                          , fdei_tpcl
                          , fdei_stat
                          , fdei_fecact
                          , fdei_ciud_repleg
                          , fdei_dire_repleg
                          , fdei_nucx
                          , fdei_retef
                          , fdei_gere_pd
                          , fdei_gere_sg
                          , fdei_rin
                          , fdei_ofic
                          , fdei_qdire
                          , fdei_tpbn
                          , fdei_exonerado
                          , fdei_nombre1
                          , fdei_nombre2
                          , fdei_email_emp
                          , fdei_ingres_otros
                          , fdei_concepto_otin
                          , fdei_hobbie
                          , fdei_cono_emp
                          , fdei_tpdo_repleg
                          , fdei_nudo_repleg
                          , fdei_nombre1_repleg
                          , fdei_nombre2_repleg
                          , fdei_apell1_repleg
                          , fdei_apell2_repleg
                          , fdei_sexo_repleg
                          , fdei_opin
                          , fdei_enti_opin
                          , fdei_nucu_opin
                          , fdei_mone_opin
                          , fdei_obse_opin
                          , fdei_clase_trib
                          , fdei_pais_opin
                          , fdei_ciud_opin
                          , fdei_fech_nac_repleg
                          , fdei_ciud_nac_repleg
                          , fdei_feccdat
                          , fdei_usuacdat
                          , fdei_num_emp
                          , fdei_soc_vig_hasta
                          , fdei_orrc
                          , fdei_exento
                          , fdei_verif_sip
                          , fdei_usr_sip
                          , fdei_fecver_sip
                          , fdei_prod
                          , fdei_fec_exon
                          , fdei_exon
                          , fdei_exon_observ
                          , fdei_usuamod
                          , fdei_fecmod
                          , fdei_sgcl
                          , fdei_cana
                          , fdei_terminal
                          , fdei_pais_nac
                          , fdei_opme
                          , fdei_tppr
                          , fdei_monto_prod
                          , fdei_pais_nac_repleg
                          , fdei_ciud_exp_repleg
                          , fdei_fech_exp_repleg
                          , fdei_sect
                          , fdei_extr_elect
                          , fdei_pais_cons
                          , fdei_giin
                          , fdei_fitp
                          , fdei_sponsor
                          , fdei_giin_sponsor
                          , fdei_aext_email
                          , fdei_lnds
                          , fdei_pep
                          , fdei_tpcl_banc
                          , fdei_pep_pj
                          , fdei_fdei
                          , fdei_nombre
                          , fdei_apell1
                          , fdei_apell2
                          , fdei_auxi
                          , fdei_tpid
                          , fdei_ciud_exp
                          , fdei_fecexp
                          , fdei_fecnac
                          , fdei_ciud_nac
                          , fdei_fisexo
                          , fdei_estciv
                          , fdei_nivest
                          , fdei_prof
                          , fdei_email
                          , fdei_acargo
                          , fdei_numhij
                          , fdei_ciud_dom
                          , fdei_dire_dom
                          , fdei_tele_dom
                          , fdei_nrofax
                          , fdei_celula
                          , fdei_ocupac
                          , fdei_person
                          , fdei_ciua
                          , fdei_ingres
                          , fdei_egreso
                          , fdei_impusa
                          , fdei_nombre_emp
                          , fdei_nit_emp
                          , fdei_dire_emp
                          , fdei_tele_emp
                          , fdei_ciud_emp
                          , fdei_ciua_emp
                          , fdei_fecvin
                          , fdei_cargo
                          , fdei_orig_recu
                          , fdei_nomb_repleg
                          , fdei_tele_repleg
                          , fdei_celu_repleg
                          , fdei_fax_repleg
                          , fdei_email_repleg
                          , fdei_num_escri
                          , fdei_notaria
                          , fdei_ciud_notaria
                          , fdei_activos
                          , fdei_pasivos
                          , fdei_matric_merc
                          , fdei_tpo_empresa
                          )
                  values (  p_fdei.fdei_dir_corresp
                          , p_fdei.fdei_feccre
                          , p_fdei.fdei_usua
                          , p_fdei.fdei_exento_conesp
                          , p_fdei.fdei_sgmr
                          , p_fdei.fdei_tpcl
                          , p_fdei.fdei_stat
                          , p_fdei.fdei_fecact
                          , p_fdei.fdei_ciud_repleg
                          , p_fdei.fdei_dire_repleg
                          , p_fdei.fdei_nucx
                          , p_fdei.fdei_retef
                          , p_fdei.fdei_gere_pd
                          , p_fdei.fdei_gere_sg
                          , p_fdei.fdei_rin
                          , p_fdei.fdei_ofic
                          , p_fdei.fdei_qdire
                          , p_fdei.fdei_tpbn
                          , p_fdei.fdei_exonerado
                          , p_fdei.fdei_nombre1
                          , p_fdei.fdei_nombre2
                          , p_fdei.fdei_email_emp
                          , p_fdei.fdei_ingres_otros
                          , p_fdei.fdei_concepto_otin
                          , p_fdei.fdei_hobbie
                          , p_fdei.fdei_cono_emp
                          , p_fdei.fdei_tpdo_repleg
                          , p_fdei.fdei_nudo_repleg
                          , p_fdei.fdei_nombre1_repleg
                          , p_fdei.fdei_nombre2_repleg
                          , p_fdei.fdei_apell1_repleg
                          , p_fdei.fdei_apell2_repleg
                          , p_fdei.fdei_sexo_repleg
                          , p_fdei.fdei_opin
                          , p_fdei.fdei_enti_opin
                          , p_fdei.fdei_nucu_opin
                          , p_fdei.fdei_mone_opin
                          , p_fdei.fdei_obse_opin
                          , p_fdei.fdei_clase_trib
                          , p_fdei.fdei_pais_opin
                          , p_fdei.fdei_ciud_opin
                          , p_fdei.fdei_fech_nac_repleg
                          , p_fdei.fdei_ciud_nac_repleg
                          , p_fdei.fdei_feccdat
                          , p_fdei.fdei_usuacdat
                          , p_fdei.fdei_num_emp
                          , p_fdei.fdei_soc_vig_hasta
                          , p_fdei.fdei_orrc
                          , p_fdei.fdei_exento
                          , p_fdei.fdei_verif_sip
                          , p_fdei.fdei_usr_sip
                          , p_fdei.fdei_fecver_sip
                          , p_fdei.fdei_prod
                          , p_fdei.fdei_fec_exon
                          , p_fdei.fdei_exon
                          , p_fdei.fdei_exon_observ
                          , p_fdei.fdei_usuamod
                          , p_fdei.fdei_fecmod
                          , p_fdei.fdei_sgcl
                          , p_fdei.fdei_cana
                          , p_fdei.fdei_terminal
                          , p_fdei.fdei_pais_nac
                          , p_fdei.fdei_opme
                          , p_fdei.fdei_tppr
                          , p_fdei.fdei_monto_prod
                          , p_fdei.fdei_pais_nac_repleg
                          , p_fdei.fdei_ciud_exp_repleg
                          , p_fdei.fdei_fech_exp_repleg
                          , p_fdei.fdei_sect
                          , p_fdei.fdei_extr_elect
                          , p_fdei.fdei_pais_cons
                          , p_fdei.fdei_giin
                          , p_fdei.fdei_fitp
                          , p_fdei.fdei_sponsor
                          , p_fdei.fdei_giin_sponsor
                          , p_fdei.fdei_aext_email
                          , p_fdei.fdei_lnds
                          , p_fdei.fdei_pep
                          , p_fdei.fdei_tpcl_banc
                          , p_fdei.fdei_pep_pj
                          , p_fdei.fdei_fdei
                          , p_fdei.fdei_nombre
                          , p_fdei.fdei_apell1
                          , p_fdei.fdei_apell2
                          , p_fdei.fdei_auxi
                          , p_fdei.fdei_tpid
                          , p_fdei.fdei_ciud_exp
                          , p_fdei.fdei_fecexp
                          , p_fdei.fdei_fecnac
                          , p_fdei.fdei_ciud_nac
                          , p_fdei.fdei_fisexo
                          , p_fdei.fdei_estciv
                          , p_fdei.fdei_nivest
                          , p_fdei.fdei_prof
                          , p_fdei.fdei_email
                          , p_fdei.fdei_acargo
                          , p_fdei.fdei_numhij
                          , p_fdei.fdei_ciud_dom
                          , p_fdei.fdei_dire_dom
                          , p_fdei.fdei_tele_dom
                          , p_fdei.fdei_nrofax
                          , p_fdei.fdei_celula
                          , p_fdei.fdei_ocupac
                          , p_fdei.fdei_person
                          , p_fdei.fdei_ciua
                          , p_fdei.fdei_ingres
                          , p_fdei.fdei_egreso
                          , p_fdei.fdei_impusa
                          , p_fdei.fdei_nombre_emp
                          , p_fdei.fdei_nit_emp
                          , p_fdei.fdei_dire_emp
                          , p_fdei.fdei_tele_emp
                          , p_fdei.fdei_ciud_emp
                          , p_fdei.fdei_ciua_emp
                          , p_fdei.fdei_fecvin
                          , p_fdei.fdei_cargo
                          , p_fdei.fdei_orig_recu
                          , p_fdei.fdei_nomb_repleg
                          , p_fdei.fdei_tele_repleg
                          , p_fdei.fdei_celu_repleg
                          , p_fdei.fdei_fax_repleg
                          , p_fdei.fdei_email_repleg
                          , p_fdei.fdei_num_escri
                          , p_fdei.fdei_notaria
                          , p_fdei.fdei_ciud_notaria
                          , p_fdei.fdei_activos
                          , p_fdei.fdei_pasivos
                          , p_fdei.fdei_matric_merc
                          , p_fdei.fdei_tpo_empresa
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.insertar_fdei:'||sqlerrm, 1, 200);
        --
end insertar_fdei;
procedure actualizar_fdei (  p_fdei         sf_tfdei%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tfdei 
       set fdei_dir_corresp = p_fdei.fdei_dir_corresp
         , fdei_feccre = p_fdei.fdei_feccre
         , fdei_usua = p_fdei.fdei_usua
         , fdei_exento_conesp = p_fdei.fdei_exento_conesp
         , fdei_sgmr = p_fdei.fdei_sgmr
         , fdei_tpcl = p_fdei.fdei_tpcl
         , fdei_stat = p_fdei.fdei_stat
         , fdei_fecact = p_fdei.fdei_fecact
         , fdei_ciud_repleg = p_fdei.fdei_ciud_repleg
         , fdei_dire_repleg = p_fdei.fdei_dire_repleg
         , fdei_nucx = p_fdei.fdei_nucx
         , fdei_retef = p_fdei.fdei_retef
         , fdei_gere_pd = p_fdei.fdei_gere_pd
         , fdei_gere_sg = p_fdei.fdei_gere_sg
         , fdei_rin = p_fdei.fdei_rin
         , fdei_ofic = p_fdei.fdei_ofic
         , fdei_qdire = p_fdei.fdei_qdire
         , fdei_tpbn = p_fdei.fdei_tpbn
         , fdei_exonerado = p_fdei.fdei_exonerado
         , fdei_nombre1 = p_fdei.fdei_nombre1
         , fdei_nombre2 = p_fdei.fdei_nombre2
         , fdei_email_emp = p_fdei.fdei_email_emp
         , fdei_ingres_otros = p_fdei.fdei_ingres_otros
         , fdei_concepto_otin = p_fdei.fdei_concepto_otin
         , fdei_hobbie = p_fdei.fdei_hobbie
         , fdei_cono_emp = p_fdei.fdei_cono_emp
         , fdei_tpdo_repleg = p_fdei.fdei_tpdo_repleg
         , fdei_nudo_repleg = p_fdei.fdei_nudo_repleg
         , fdei_nombre1_repleg = p_fdei.fdei_nombre1_repleg
         , fdei_nombre2_repleg = p_fdei.fdei_nombre2_repleg
         , fdei_apell1_repleg = p_fdei.fdei_apell1_repleg
         , fdei_apell2_repleg = p_fdei.fdei_apell2_repleg
         , fdei_sexo_repleg = p_fdei.fdei_sexo_repleg
         , fdei_opin = p_fdei.fdei_opin
         , fdei_enti_opin = p_fdei.fdei_enti_opin
         , fdei_nucu_opin = p_fdei.fdei_nucu_opin
         , fdei_mone_opin = p_fdei.fdei_mone_opin
         , fdei_obse_opin = p_fdei.fdei_obse_opin
         , fdei_clase_trib = p_fdei.fdei_clase_trib
         , fdei_pais_opin = p_fdei.fdei_pais_opin
         , fdei_ciud_opin = p_fdei.fdei_ciud_opin
         , fdei_fech_nac_repleg = p_fdei.fdei_fech_nac_repleg
         , fdei_ciud_nac_repleg = p_fdei.fdei_ciud_nac_repleg
         , fdei_feccdat = p_fdei.fdei_feccdat
         , fdei_usuacdat = p_fdei.fdei_usuacdat
         , fdei_num_emp = p_fdei.fdei_num_emp
         , fdei_soc_vig_hasta = p_fdei.fdei_soc_vig_hasta
         , fdei_orrc = p_fdei.fdei_orrc
         , fdei_exento = p_fdei.fdei_exento
         , fdei_verif_sip = p_fdei.fdei_verif_sip
         , fdei_usr_sip = p_fdei.fdei_usr_sip
         , fdei_fecver_sip = p_fdei.fdei_fecver_sip
         , fdei_prod = p_fdei.fdei_prod
         , fdei_fec_exon = p_fdei.fdei_fec_exon
         , fdei_exon = p_fdei.fdei_exon
         , fdei_exon_observ = p_fdei.fdei_exon_observ
         , fdei_usuamod = p_fdei.fdei_usuamod
         , fdei_fecmod = p_fdei.fdei_fecmod
         , fdei_sgcl = p_fdei.fdei_sgcl
         , fdei_cana = p_fdei.fdei_cana
         , fdei_terminal = p_fdei.fdei_terminal
         , fdei_pais_nac = p_fdei.fdei_pais_nac
         , fdei_opme = p_fdei.fdei_opme
         , fdei_tppr = p_fdei.fdei_tppr
         , fdei_monto_prod = p_fdei.fdei_monto_prod
         , fdei_pais_nac_repleg = p_fdei.fdei_pais_nac_repleg
         , fdei_ciud_exp_repleg = p_fdei.fdei_ciud_exp_repleg
         , fdei_fech_exp_repleg = p_fdei.fdei_fech_exp_repleg
         , fdei_sect = p_fdei.fdei_sect
         , fdei_extr_elect = p_fdei.fdei_extr_elect
         , fdei_pais_cons = p_fdei.fdei_pais_cons
         , fdei_giin = p_fdei.fdei_giin
         , fdei_fitp = p_fdei.fdei_fitp
         , fdei_sponsor = p_fdei.fdei_sponsor
         , fdei_giin_sponsor = p_fdei.fdei_giin_sponsor
         , fdei_aext_email = p_fdei.fdei_aext_email
         , fdei_lnds = p_fdei.fdei_lnds
         , fdei_pep = p_fdei.fdei_pep
         , fdei_tpcl_banc = p_fdei.fdei_tpcl_banc
         , fdei_pep_pj = p_fdei.fdei_pep_pj
         , fdei_fdei = p_fdei.fdei_fdei
         , fdei_nombre = p_fdei.fdei_nombre
         , fdei_apell1 = p_fdei.fdei_apell1
         , fdei_apell2 = p_fdei.fdei_apell2
         , fdei_auxi = p_fdei.fdei_auxi
         , fdei_tpid = p_fdei.fdei_tpid
         , fdei_ciud_exp = p_fdei.fdei_ciud_exp
         , fdei_fecexp = p_fdei.fdei_fecexp
         , fdei_fecnac = p_fdei.fdei_fecnac
         , fdei_ciud_nac = p_fdei.fdei_ciud_nac
         , fdei_fisexo = p_fdei.fdei_fisexo
         , fdei_estciv = p_fdei.fdei_estciv
         , fdei_nivest = p_fdei.fdei_nivest
         , fdei_prof = p_fdei.fdei_prof
         , fdei_email = p_fdei.fdei_email
         , fdei_acargo = p_fdei.fdei_acargo
         , fdei_numhij = p_fdei.fdei_numhij
         , fdei_ciud_dom = p_fdei.fdei_ciud_dom
         , fdei_dire_dom = p_fdei.fdei_dire_dom
         , fdei_tele_dom = p_fdei.fdei_tele_dom
         , fdei_nrofax = p_fdei.fdei_nrofax
         , fdei_celula = p_fdei.fdei_celula
         , fdei_ocupac = p_fdei.fdei_ocupac
         , fdei_person = p_fdei.fdei_person
         , fdei_ciua = p_fdei.fdei_ciua
         , fdei_ingres = p_fdei.fdei_ingres
         , fdei_egreso = p_fdei.fdei_egreso
         , fdei_impusa = p_fdei.fdei_impusa
         , fdei_nombre_emp = p_fdei.fdei_nombre_emp
         , fdei_nit_emp = p_fdei.fdei_nit_emp
         , fdei_dire_emp = p_fdei.fdei_dire_emp
         , fdei_tele_emp = p_fdei.fdei_tele_emp
         , fdei_ciud_emp = p_fdei.fdei_ciud_emp
         , fdei_ciua_emp = p_fdei.fdei_ciua_emp
         , fdei_fecvin = p_fdei.fdei_fecvin
         , fdei_cargo = p_fdei.fdei_cargo
         , fdei_orig_recu = p_fdei.fdei_orig_recu
         , fdei_nomb_repleg = p_fdei.fdei_nomb_repleg
         , fdei_tele_repleg = p_fdei.fdei_tele_repleg
         , fdei_celu_repleg = p_fdei.fdei_celu_repleg
         , fdei_fax_repleg = p_fdei.fdei_fax_repleg
         , fdei_email_repleg = p_fdei.fdei_email_repleg
         , fdei_num_escri = p_fdei.fdei_num_escri
         , fdei_notaria = p_fdei.fdei_notaria
         , fdei_ciud_notaria = p_fdei.fdei_ciud_notaria
         , fdei_activos = p_fdei.fdei_activos
         , fdei_pasivos = p_fdei.fdei_pasivos
         , fdei_matric_merc = p_fdei.fdei_matric_merc
         , fdei_tpo_empresa = p_fdei.fdei_tpo_empresa
     where fdei_fdei = p_fdei.fdei_fdei
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_fdei:'||sqlerrm, 1, 200);
        --
end actualizar_fdei;
procedure consultar_fdei (  p_ty_fdei  in out sf_tfdei%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_fdei is
    select *
      from sf_tfdei
     where fdei_fdei = p_ty_fdei.fdei_fdei
              ;
    --
begin
    open  c_fdei;
    fetch c_fdei into p_ty_fdei;
    close c_fdei;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_fdei:'||sqlerrm, 1, 200);
        --
end consultar_fdei;


procedure insertar_drcl (  p_drcl         sf_tdrcl%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tdrcl (  drcl_usua
                          , drcl_feccre
                          , drcl_drco
                          , drcl_pais
                          , drcl_fdei
                          , drcl_tpdr
                          , drcl_dire
                          , drcl_ciud
                          , drcl_tele
                          , drcl_ext_tele
                          , drcl_nrofax
                          , drcl_celular
                          )
                  values (  p_drcl.drcl_usua
                          , p_drcl.drcl_feccre
                          , p_drcl.drcl_drco
                          , p_drcl.drcl_pais
                          , p_drcl.drcl_fdei
                          , p_drcl.drcl_tpdr
                          , p_drcl.drcl_dire
                          , p_drcl.drcl_ciud
                          , p_drcl.drcl_tele
                          , p_drcl.drcl_ext_tele
                          , p_drcl.drcl_nrofax
                          , p_drcl.drcl_celular
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_drcl:'||sqlerrm, 1, 200);
        --
end insertar_drcl;
procedure actualizar_drcl (  p_drcl         sf_tdrcl%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tdrcl 
       set drcl_usua = p_drcl.drcl_usua
         , drcl_feccre = p_drcl.drcl_feccre
         , drcl_drco = p_drcl.drcl_drco
         , drcl_pais = p_drcl.drcl_pais
         , drcl_fdei = p_drcl.drcl_fdei
         , drcl_tpdr = p_drcl.drcl_tpdr
         , drcl_dire = p_drcl.drcl_dire
         , drcl_ciud = p_drcl.drcl_ciud
         , drcl_tele = p_drcl.drcl_tele
         , drcl_ext_tele = p_drcl.drcl_ext_tele
         , drcl_nrofax = p_drcl.drcl_nrofax
         , drcl_celular = p_drcl.drcl_celular
     where drcl_fdei = p_drcl.drcl_fdei
     and drcl_tpdr = p_drcl.drcl_tpdr
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_drcl:'||sqlerrm, 1, 200);
        --
end actualizar_drcl;
procedure consultar_drcl (  p_ty_drcl  in out sf_tdrcl%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_drcl is
    select *
      from sf_tdrcl
     where drcl_fdei = p_ty_drcl.drcl_fdei
     and drcl_tpdr = p_ty_drcl.drcl_tpdr
              ;
    --
begin
    open  c_drcl;
    fetch c_drcl into p_ty_drcl;
    close c_drcl;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_drcl:'||sqlerrm, 1, 200);
        --
end consultar_drcl;


--
procedure insertar_apfd (  p_apfd         sf_tapfd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tapfd (  apfd_fdei
                          , apfd_fdei_rela
                          , apfd_desc_rela
                          , apfd_esta_asoc
                          , apfd_fech_inac
                          , apfd_usua
                          , apfd_feccre
                          , apfd_usuamod
                          , apfd_fecmod
                          , apfd_radi
                          , apfd_pais
                          )
                  values (  p_apfd.apfd_fdei
                          , p_apfd.apfd_fdei_rela
                          , p_apfd.apfd_desc_rela
                          , p_apfd.apfd_esta_asoc
                          , p_apfd.apfd_fech_inac
                          , p_apfd.apfd_usua
                          , p_apfd.apfd_feccre
                          , p_apfd.apfd_usuamod
                          , p_apfd.apfd_fecmod
                          , p_apfd.apfd_radi
                          , p_apfd.apfd_pais
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_apfd:'||sqlerrm, 1, 200);
        --
end insertar_apfd;
procedure actualizar_apfd (  p_apfd         sf_tapfd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tapfd 
       set apfd_fdei = p_apfd.apfd_fdei
         , apfd_fdei_rela = p_apfd.apfd_fdei_rela
         , apfd_desc_rela = p_apfd.apfd_desc_rela
         , apfd_esta_asoc = p_apfd.apfd_esta_asoc
         , apfd_fech_inac = p_apfd.apfd_fech_inac
         , apfd_usua = p_apfd.apfd_usua
         , apfd_feccre = p_apfd.apfd_feccre
         , apfd_usuamod = p_apfd.apfd_usuamod
         , apfd_fecmod = p_apfd.apfd_fecmod
         , apfd_radi = p_apfd.apfd_radi
         , apfd_pais = p_apfd.apfd_pais
     where apfd_fdei = p_apfd.apfd_fdei
     and apfd_fdei_rela = p_apfd.apfd_fdei_rela
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_apfd:'||sqlerrm, 1, 200);
        --
end actualizar_apfd;
procedure consultar_apfd (  p_ty_apfd  in out sf_tapfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_apfd is
    select apfd_fdei
         , apfd_fdei_rela
         , apfd_desc_rela
         , apfd_esta_asoc
         , apfd_fech_inac
         , apfd_usua
         , apfd_feccre
         , apfd_usuamod
         , apfd_fecmod
         , apfd_radi
         , apfd_pais
      from sf_tapfd
     where apfd_fdei = p_ty_apfd.apfd_fdei
     and apfd_fdei_rela = p_ty_apfd.apfd_fdei_rela
              ;
    --
begin
    open  c_apfd;
    fetch c_apfd into p_ty_apfd;
    close c_apfd;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_apfd:'||sqlerrm, 1, 200);
        --
end consultar_apfd;
--
procedure insertar_rffd (  p_rffd         sf_trffd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_trffd (  rffd_usua
                          , rffd_sucu_aval_ref
                          , rffd_tpba
                          , rffd_nucx
                          , rffd_fdei
                          , rffd_rffd
                          , rffd_tipo
                          , rffd_descri
                          , rffd_dire
                          , rffd_ciud
                          , rffd_tele
                          , rffd_cuenta
                          , rffd_observa
                          , rffd_feccre
                          )
                  values (  p_rffd.rffd_usua
                          , p_rffd.rffd_sucu_aval_ref
                          , p_rffd.rffd_tpba
                          , p_rffd.rffd_nucx
                          , p_rffd.rffd_fdei
                          , p_rffd.rffd_rffd
                          , p_rffd.rffd_tipo
                          , p_rffd.rffd_descri
                          , p_rffd.rffd_dire
                          , p_rffd.rffd_ciud
                          , p_rffd.rffd_tele
                          , p_rffd.rffd_cuenta
                          , p_rffd.rffd_observa
                          , p_rffd.rffd_feccre
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_rffd:'||sqlerrm, 1, 200);
        --
end insertar_rffd;
procedure actualizar_rffd (  p_rffd         sf_trffd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_trffd 
       set rffd_usua = p_rffd.rffd_usua
         , rffd_sucu_aval_ref = p_rffd.rffd_sucu_aval_ref
         , rffd_tpba = p_rffd.rffd_tpba
         , rffd_nucx = p_rffd.rffd_nucx
         , rffd_fdei = p_rffd.rffd_fdei
         , rffd_rffd = p_rffd.rffd_rffd
         , rffd_tipo = p_rffd.rffd_tipo
         , rffd_descri = p_rffd.rffd_descri
         , rffd_dire = p_rffd.rffd_dire
         , rffd_ciud = p_rffd.rffd_ciud
         , rffd_tele = p_rffd.rffd_tele
         , rffd_cuenta = p_rffd.rffd_cuenta
         , rffd_observa = p_rffd.rffd_observa
         , rffd_feccre = p_rffd.rffd_feccre
     where rffd_fdei = p_rffd.rffd_fdei
     and rffd_rffd = p_rffd.rffd_rffd
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_rffd:'||sqlerrm, 1, 200);
        --
end actualizar_rffd;
procedure consultar_rffd (  p_ty_rffd  in out sf_trffd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_rffd is
    select *
      from sf_trffd
     where rffd_fdei = p_ty_rffd.rffd_fdei
     and rffd_rffd = p_ty_rffd.rffd_rffd
              ;
    --
begin
    open  c_rffd;
    fetch c_rffd into p_ty_rffd;
    close c_rffd;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_rffd:'||sqlerrm, 1, 200);
        --
end consultar_rffd;



procedure insertar_cnfd (  p_cnfd         sf_tcnfd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tcnfd (  cnfd_fdei
                          , cnfd_cnfd
                          , cnfd_descri
                          , cnfd_dire
                          , cnfd_ciud
                          , cnfd_tele
                          , cnfd_celu
                          , cnfd_fax
                          , cnfd_email
                          , cnfd_feccre
                          , cnfd_usua
                          )
                  values (  p_cnfd.cnfd_fdei
                          , p_cnfd.cnfd_cnfd
                          , p_cnfd.cnfd_descri
                          , p_cnfd.cnfd_dire
                          , p_cnfd.cnfd_ciud
                          , p_cnfd.cnfd_tele
                          , p_cnfd.cnfd_celu
                          , p_cnfd.cnfd_fax
                          , p_cnfd.cnfd_email
                          , p_cnfd.cnfd_feccre
                          , p_cnfd.cnfd_usua
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_cnfd:'||sqlerrm, 1, 200);
        --
end insertar_cnfd;
procedure actualizar_cnfd (  p_cnfd         sf_tcnfd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tcnfd 
       set cnfd_fdei = p_cnfd.cnfd_fdei
         , cnfd_cnfd = p_cnfd.cnfd_cnfd
         , cnfd_descri = p_cnfd.cnfd_descri
         , cnfd_dire = p_cnfd.cnfd_dire
         , cnfd_ciud = p_cnfd.cnfd_ciud
         , cnfd_tele = p_cnfd.cnfd_tele
         , cnfd_celu = p_cnfd.cnfd_celu
         , cnfd_fax = p_cnfd.cnfd_fax
         , cnfd_email = p_cnfd.cnfd_email
         , cnfd_feccre = p_cnfd.cnfd_feccre
         , cnfd_usua = p_cnfd.cnfd_usua
     where cnfd_fdei = p_cnfd.cnfd_fdei
     and cnfd_cnfd = p_cnfd.cnfd_cnfd
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_cnfd:'||sqlerrm, 1, 200);
        --
end actualizar_cnfd;
procedure consultar_cnfd (  p_ty_cnfd  in out sf_tcnfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_cnfd is
    select cnfd_fdei
         , cnfd_cnfd
         , cnfd_descri
         , cnfd_dire
         , cnfd_ciud
         , cnfd_tele
         , cnfd_celu
         , cnfd_fax
         , cnfd_email
         , cnfd_feccre
         , cnfd_usua
      from sf_tcnfd
     where cnfd_fdei = p_ty_cnfd.cnfd_fdei
     and cnfd_cnfd = p_ty_cnfd.cnfd_cnfd
              ;
    --
begin
    open  c_cnfd;
    fetch c_cnfd into p_ty_cnfd;
    close c_cnfd;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_cnfd:'||sqlerrm, 1, 200);
        --
end consultar_cnfd;


--
procedure insertar_soci (  p_soci         sf_tsoci%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tsoci (  soci_fdei
                          , soci_auxi
                          , soci_porc
                          , soci_usua
                          , soci_feccre
                          , soci_pais
                          , soci_id_trib
                          , soci_tipo_soci
                          , soci_pep
                          , soci_stat
                          , soci_usua_ina
                          , soci_fec_ina
                          , soci_soci
                          )
                  values (  p_soci.soci_fdei
                          , p_soci.soci_auxi
                          , p_soci.soci_porc
                          , p_soci.soci_usua
                          , p_soci.soci_feccre
                          , p_soci.soci_pais
                          , p_soci.soci_id_trib
                          , p_soci.soci_tipo_soci
                          , p_soci.soci_pep
                          , p_soci.soci_stat
                          , p_soci.soci_usua_ina
                          , p_soci.soci_fec_ina
                          , p_soci.soci_soci
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_soci:'||sqlerrm, 1, 200);
        --
end insertar_soci;
--
procedure actualizar_soci (  p_soci         sf_tsoci%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tsoci 
       set soci_fdei = p_soci.soci_fdei
         , soci_auxi = p_soci.soci_auxi
         , soci_porc = p_soci.soci_porc
         , soci_usua = p_soci.soci_usua
         , soci_feccre = p_soci.soci_feccre
         , soci_pais = p_soci.soci_pais
         , soci_id_trib = p_soci.soci_id_trib
         , soci_tipo_soci = p_soci.soci_tipo_soci
         , soci_pep = p_soci.soci_pep
         , soci_stat = p_soci.soci_stat
         , soci_usua_ina = p_soci.soci_usua_ina
         , soci_fec_ina = p_soci.soci_fec_ina
         , soci_soci = p_soci.soci_soci
     where soci_fdei = p_soci.soci_fdei
     and soci_auxi = p_soci.soci_auxi
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_soci:'||sqlerrm, 1, 200);
        --
end actualizar_soci;
procedure consultar_soci (  p_ty_soci  in out sf_tsoci%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_soci is
    select soci_fdei
         , soci_auxi
         , soci_porc
         , soci_usua
         , soci_feccre
         , soci_pais
         , soci_id_trib
         , soci_tipo_soci
         , soci_pep
         , soci_stat
         , soci_usua_ina
         , soci_fec_ina
         , soci_soci
      from sf_tsoci
     where soci_fdei = p_ty_soci.soci_fdei
     and soci_auxi = p_ty_soci.soci_auxi
              ;
    --
begin
    open  c_soci;
    fetch c_soci into p_ty_soci;
    close c_soci;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_soci:'||sqlerrm, 1, 200);
        --
end consultar_soci;


--
procedure insertar_fsuc (  p_fsuc         sf_tfsuc%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tfsuc (  fsuc_fdei
                          , fsuc_fsuc
                          , fsuc_descri
                          , fsuc_dire
                          , fsuc_ciud
                          , fsuc_tele
                          , fsuc_feccre
                          , fsuc_usua
                          , fsuc_fax
                          )
                  values (  p_fsuc.fsuc_fdei
                          , p_fsuc.fsuc_fsuc
                          , p_fsuc.fsuc_descri
                          , p_fsuc.fsuc_dire
                          , p_fsuc.fsuc_ciud
                          , p_fsuc.fsuc_tele
                          , p_fsuc.fsuc_feccre
                          , p_fsuc.fsuc_usua
                          , p_fsuc.fsuc_fax
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_fsuc:'||sqlerrm, 1, 200);
        --
end insertar_fsuc;
--
procedure actualizar_fsuc (  p_fsuc         sf_tfsuc%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tfsuc 
       set fsuc_fdei = p_fsuc.fsuc_fdei
         , fsuc_fsuc = p_fsuc.fsuc_fsuc
         , fsuc_descri = p_fsuc.fsuc_descri
         , fsuc_dire = p_fsuc.fsuc_dire
         , fsuc_ciud = p_fsuc.fsuc_ciud
         , fsuc_tele = p_fsuc.fsuc_tele
         , fsuc_feccre = p_fsuc.fsuc_feccre
         , fsuc_usua = p_fsuc.fsuc_usua
         , fsuc_fax = p_fsuc.fsuc_fax
     where fsuc_fdei = p_fsuc.fsuc_fdei
     and fsuc_fsuc = p_fsuc.fsuc_fsuc
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_fsuc:'||sqlerrm, 1, 200);
        --
end actualizar_fsuc;
procedure consultar_fsuc (  p_ty_fsuc  in out sf_tfsuc%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_fsuc is
    select fsuc_fdei
         , fsuc_fsuc
         , fsuc_descri
         , fsuc_dire
         , fsuc_ciud
         , fsuc_tele
         , fsuc_feccre
         , fsuc_usua
         , fsuc_fax
      from sf_tfsuc
     where fsuc_fdei = p_ty_fsuc.fsuc_fdei
     and fsuc_fsuc = p_ty_fsuc.fsuc_fsuc
              ;
    --
begin
    open  c_fsuc;
    fetch c_fsuc into p_ty_fsuc;
    close c_fsuc;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_fsuc:'||sqlerrm, 1, 200);
        --
end consultar_fsuc;


--
procedure insertar_admi (  p_admi         sf_tadmi%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tadmi (  admi_fdei
                          , admi_nit
                          , admi_tpid
                          , admi_nombre1
                          , admi_nombre2
                          , admi_apell1
                          , admi_apell2
                          , admi_vinculo
                          , admi_pep
                          , admi_usua
                          , admi_fech
                          , admi_stat
                          , admi_usua_ina
                          , admi_fec_ina
                          , admi_tiad
                          )
                  values (  p_admi.admi_fdei
                          , p_admi.admi_nit
                          , p_admi.admi_tpid
                          , p_admi.admi_nombre1
                          , p_admi.admi_nombre2
                          , p_admi.admi_apell1
                          , p_admi.admi_apell2
                          , p_admi.admi_vinculo
                          , p_admi.admi_pep
                          , p_admi.admi_usua
                          , p_admi.admi_fech
                          , p_admi.admi_stat
                          , p_admi.admi_usua_ina
                          , p_admi.admi_fec_ina
                          , p_admi.admi_tiad
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_admi:'||sqlerrm, 1, 200);
        --
end insertar_admi;
procedure actualizar_admi (  p_admi         sf_tadmi%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tadmi 
       set admi_fdei = p_admi.admi_fdei
         , admi_nit = p_admi.admi_nit
         , admi_tpid = p_admi.admi_tpid
         , admi_nombre1 = p_admi.admi_nombre1
         , admi_nombre2 = p_admi.admi_nombre2
         , admi_apell1 = p_admi.admi_apell1
         , admi_apell2 = p_admi.admi_apell2
         , admi_vinculo = p_admi.admi_vinculo
         , admi_pep = p_admi.admi_pep
         , admi_usua = p_admi.admi_usua
         , admi_fech = p_admi.admi_fech
         , admi_stat = p_admi.admi_stat
         , admi_usua_ina = p_admi.admi_usua_ina
         , admi_fec_ina = p_admi.admi_fec_ina
         , admi_tiad = p_admi.admi_tiad
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_admi:'||sqlerrm, 1, 200);
        --
end actualizar_admi;
procedure consultar_admi (  p_ty_admi  in out sf_tadmi%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_admi is
    select admi_fdei
         , admi_nit
         , admi_tpid
         , admi_nombre1
         , admi_nombre2
         , admi_apell1
         , admi_apell2
         , admi_vinculo
         , admi_pep
         , admi_usua
         , admi_fech
         , admi_stat
         , admi_usua_ina
         , admi_fec_ina
         , admi_tiad
      from sf_tadmi
              ;
    --
begin
    open  c_admi;
    fetch c_admi into p_ty_admi;
    close c_admi;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_admi:'||sqlerrm, 1, 200);
        --
end consultar_admi;
--
procedure insertar_frau (  p_frau         sf_tfrau%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tfrau (  frau_fdei
                          , frau_auxi
                          , frau_cargo
                          , frau_usua
                          , frau_feccre
                          , frau_pep_pn
                          , frau_pep_pj
                          )
                  values (  p_frau.frau_fdei
                          , p_frau.frau_auxi
                          , p_frau.frau_cargo
                          , p_frau.frau_usua
                          , p_frau.frau_feccre
                          , p_frau.frau_pep_pn
                          , p_frau.frau_pep_pj
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_frau:'||sqlerrm, 1, 200);
        --
end insertar_frau;
procedure actualizar_frau (  p_frau         sf_tfrau%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tfrau 
       set frau_fdei = p_frau.frau_fdei
         , frau_auxi = p_frau.frau_auxi
         , frau_cargo = p_frau.frau_cargo
         , frau_usua = p_frau.frau_usua
         , frau_feccre = p_frau.frau_feccre
         , frau_pep_pn = p_frau.frau_pep_pn
         , frau_pep_pj = p_frau.frau_pep_pj
     where frau_fdei = p_frau.frau_fdei
     and frau_auxi = p_frau.frau_auxi
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_frau:'||sqlerrm, 1, 200);
        --
end actualizar_frau;
procedure consultar_frau (  p_ty_frau  in out sf_tfrau%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_frau is
    select frau_fdei
         , frau_auxi
         , frau_cargo
         , frau_usua
         , frau_feccre
         , frau_pep_pn
         , frau_pep_pj
      from sf_tfrau
     where frau_fdei = p_ty_frau.frau_fdei
     and frau_auxi = p_ty_frau.frau_auxi
              ;
    --
begin
    open  c_frau;
    fetch c_frau into p_ty_frau;
    close c_frau;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_frau:'||sqlerrm, 1, 200);
        --
end consultar_frau;
--
procedure insertar_emfd (  p_emfd         sf_temfd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_temfd (  emfd_fdei
                          , emfd_emfd
                          , emfd_descri
                          , emfd_cargo
                          , emfd_dire
                          , emfd_ciud
                          , emfd_tele
                          , emfd_fecvinc
                          , emfd_feccre
                          , emfd_usua
                          )
                  values (  p_emfd.emfd_fdei
                          , p_emfd.emfd_emfd
                          , p_emfd.emfd_descri
                          , p_emfd.emfd_cargo
                          , p_emfd.emfd_dire
                          , p_emfd.emfd_ciud
                          , p_emfd.emfd_tele
                          , p_emfd.emfd_fecvinc
                          , p_emfd.emfd_feccre
                          , p_emfd.emfd_usua
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_emfd:'||sqlerrm, 1, 200);
        --
end insertar_emfd;
--
procedure actualizar_emfd (  p_emfd         sf_temfd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_temfd 
       set emfd_fdei = p_emfd.emfd_fdei
         , emfd_emfd = p_emfd.emfd_emfd
         , emfd_descri = p_emfd.emfd_descri
         , emfd_cargo = p_emfd.emfd_cargo
         , emfd_dire = p_emfd.emfd_dire
         , emfd_ciud = p_emfd.emfd_ciud
         , emfd_tele = p_emfd.emfd_tele
         , emfd_fecvinc = p_emfd.emfd_fecvinc
         , emfd_feccre = p_emfd.emfd_feccre
         , emfd_usua = p_emfd.emfd_usua
     where emfd_fdei = p_emfd.emfd_fdei
     and emfd_emfd = p_emfd.emfd_emfd
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_emfd:'||sqlerrm, 1, 200);
        --
end actualizar_emfd;
procedure consultar_emfd (  p_ty_emfd  in out sf_temfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_emfd is
    select emfd_fdei
         , emfd_emfd
         , emfd_descri
         , emfd_cargo
         , emfd_dire
         , emfd_ciud
         , emfd_tele
         , emfd_fecvinc
         , emfd_feccre
         , emfd_usua
      from sf_temfd
     where emfd_fdei = p_ty_emfd.emfd_fdei
     and emfd_emfd = p_ty_emfd.emfd_emfd
              ;
    --
begin
    open  c_emfd;
    fetch c_emfd into p_ty_emfd;
    close c_emfd;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_emfd:'||sqlerrm, 1, 200);
        --
end consultar_emfd;
--
procedure insertar_dcfd (  p_dcfd         sf_tdcfd%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tdcfd (  dcfd_fdei
                          , dcfd_email
                          , dcfd_tipo
                          , dcfd_usua
                          , dcfd_feccre
                          , dcfd_usuamod
                          , dcfd_fecmod
                          , dcfd_drco
                          )
                  values (  p_dcfd.dcfd_fdei
                          , p_dcfd.dcfd_email
                          , p_dcfd.dcfd_tipo
                          , p_dcfd.dcfd_usua
                          , p_dcfd.dcfd_feccre
                          , p_dcfd.dcfd_usuamod
                          , p_dcfd.dcfd_fecmod
                          , p_dcfd.dcfd_drco
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_dcfd:'||sqlerrm, 1, 200);
        --
end insertar_dcfd;
procedure actualizar_dcfd (  p_dcfd         sf_tdcfd%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tdcfd 
       set dcfd_fdei = p_dcfd.dcfd_fdei
         , dcfd_email = p_dcfd.dcfd_email
         , dcfd_tipo = p_dcfd.dcfd_tipo
         , dcfd_usua = p_dcfd.dcfd_usua
         , dcfd_feccre = p_dcfd.dcfd_feccre
         , dcfd_usuamod = p_dcfd.dcfd_usuamod
         , dcfd_fecmod = p_dcfd.dcfd_fecmod
         , dcfd_drco = p_dcfd.dcfd_drco
     where dcfd_fdei = p_dcfd.dcfd_fdei
     and dcfd_email = p_dcfd.dcfd_email
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_dcfd:'||sqlerrm, 1, 200);
        --
end actualizar_dcfd;
procedure consultar_dcfd (  p_ty_dcfd  in out sf_tdcfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_dcfd is
    select dcfd_fdei
         , dcfd_email
         , dcfd_tipo
         , dcfd_usua
         , dcfd_feccre
         , dcfd_usuamod
         , dcfd_fecmod
         , dcfd_drco
      from sf_tdcfd
     where dcfd_fdei = p_ty_dcfd.dcfd_fdei
     and dcfd_email = p_ty_dcfd.dcfd_email
              ;
    --
begin
    open  c_dcfd;
    fetch c_dcfd into p_ty_dcfd;
    close c_dcfd;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_dcfd:'||sqlerrm, 1, 200);
        --
end consultar_dcfd;
--
procedure insertar_prcg (  p_prcg         sf_tprcg%rowtype
                         , p_ty_erro  out ge_qtipo.tr_error
                        ) is
begin
    --
    insert into sf_tprcg (  prcg_prcg
                          , prcg_fdei
                          , prcg_usua
                          , prcg_feccre
                          , prcg_usuamod
                          , prcg_fecmod
                          , prcg_parent
                          , prcg_descri
                          , prcg_fech_nac
                          , prcg_pep
                          , prcg_id
                          )
                  values (  p_prcg.prcg_prcg
                          , p_prcg.prcg_fdei
                          , p_prcg.prcg_usua
                          , p_prcg.prcg_feccre
                          , p_prcg.prcg_usuamod
                          , p_prcg.prcg_fecmod
                          , p_prcg.prcg_parent
                          , p_prcg.prcg_descri
                          , p_prcg.prcg_fech_nac
                          , p_prcg.prcg_pep
                          , p_prcg.prcg_id
                         );
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Inserción exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en SF_QMDFD_CRUD.insertar_prcg:'||sqlerrm, 1, 200);
        --
end insertar_prcg;
procedure actualizar_prcg (  p_prcg         sf_tprcg%rowtype
                           , p_ty_erro  out ge_qtipo.tr_error
                          ) is
begin
    --
    update sf_tprcg 
       set prcg_prcg = p_prcg.prcg_prcg
         , prcg_fdei = p_prcg.prcg_fdei
         , prcg_usua = p_prcg.prcg_usua
         , prcg_feccre = p_prcg.prcg_feccre
         , prcg_usuamod = p_prcg.prcg_usuamod
         , prcg_fecmod = p_prcg.prcg_fecmod
         , prcg_parent = p_prcg.prcg_parent
         , prcg_descri = p_prcg.prcg_descri
         , prcg_fech_nac = p_prcg.prcg_fech_nac
         , prcg_pep = p_prcg.prcg_pep
         , prcg_id = p_prcg.prcg_id
     where prcg_prcg = p_prcg.prcg_prcg
     and prcg_fdei = p_prcg.prcg_fdei
;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Actualizacion exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.actualizar_prcg:'||sqlerrm, 1, 200);
        --
end actualizar_prcg;
procedure consultar_prcg (  p_ty_prcg  in out sf_tprcg%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_prcg is
    select prcg_prcg
         , prcg_fdei
         , prcg_usua
         , prcg_feccre
         , prcg_usuamod
         , prcg_fecmod
         , prcg_parent
         , prcg_descri
         , prcg_fech_nac
         , prcg_pep
         , prcg_id
      from sf_tprcg
     where prcg_prcg = p_ty_prcg.prcg_prcg
     and prcg_fdei = p_ty_prcg.prcg_fdei
              ;
    --
begin
    open  c_prcg;
    fetch c_prcg into p_ty_prcg;
    close c_prcg;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_prcg:'||sqlerrm, 1, 200);
        --
end consultar_prcg;
--
procedure elimina_dcfd (  p_ty_dcfd  in out sf_tdcfd%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
begin
    --
    delete sf_tdcfd
     where dcfd_fdei = p_ty_dcfd.dcfd_fdei
     and dcfd_email = p_ty_dcfd.dcfd_email;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.elimina_dcfd:'||sqlerrm, 1, 200);
        --
end elimina_dcfd;
--
-- ini 1001
procedure consultar_auxil (  p_ty_auxil  in out ge_tauxil%rowtype
                          , p_ty_erro     out ge_qtipo.tr_error
                         ) is
    --
    cursor c_auxi is
    select *
      from ge_tauxil
     where auxi_auxi = p_ty_auxil.auxi_auxi
              ;
    --
begin
    open  c_auxi;
    fetch c_auxi into p_ty_auxil;
    close c_auxi;
    --
    p_ty_erro.cod_error := 'OK';
    p_ty_erro.msg_error := 'Consulta exitosa';
    --
exception
    when others then
        --
        p_ty_erro.cod_error := 'ORA'||ltrim(to_char(sqlcode, '000000'));
        p_ty_erro.msg_error := substr('Error en sf_qfdei_crud.consultar_auxi:'||sqlerrm, 1, 200);
        --
end consultar_auxil;
-- fin 1001
end SF_QMDFD_CRUD;
/
