prompt
prompt package SF_QPRFD_CRUD
prompt
create or replace package SF_QPRFD_CRUD as
    --
    -- #VERSION:0000001000
    --
    -- HISTORIAL DE CAMBIOS
    --
    -- Versi�n     GAP           Solicitud        Fecha        Realiz�                          Descripci�n
    -- =========== ============= ================ ============ ================================ ==============================================================================
    -- 1000                      80577            28/03/2019   Inaction                         .Se crea paquete CRUD proceso fideicomitente (SF_TPRFD)
    --
    -- =========== ============= ================ ============ ================================ ==============================================================================
    --
    type ty_prfd is record  (  prfd_prfd   sf_tprfd.prfd_prfd%type
                             , prfd_fdei   sf_tprfd.prfd_fdei%type
                             , prfd_tpid   sf_tprfd.prfd_tpid%type
                             , prfd_usua   sf_tprfd.prfd_usua%type
                             , prfd_esta   sf_tprfd.prfd_esta%type
                             , prfd_obser  sf_tprfd.prfd_obser%type
                             , prfd_feccre sf_tprfd.prfd_feccre%type
                             , prfd_fecmod sf_tprfd.prfd_fecmod%type
                             , prfd_tppr   sf_tprfd.prfd_tppr%type
                            );
    --
    ----------------------------------------------------------------------------------
    --
    -- proceso de inserccion tabla SF_TPRFD
    --
    procedure insertar_sf_tprfd (  p_ty_prfd       in out  ty_prfd
                                 , p_ty_erro       out     ge_qtipo.tr_error
                                );
    --
    ----------------------------------------------------------------------------------
    --
    procedure actualiza_sf_tprfd (  p_ty_prfd       in out  ty_prfd
                                  , p_ty_erro       out     ge_qtipo.tr_error
                                 );
end SF_QPRFD_CRUD;
/
prompt
prompt package body SF_QPRFD_CRUD
prompt
create or replace package body SF_QPRFD_CRUD as
    --
    -- #VERSION:0000001010
    --
    procedure insertar_sf_tprfd (  p_ty_prfd       in out  ty_prfd
                                 , p_ty_erro       out     ge_qtipo.tr_error
                                ) is
                                pragma autonomous_transaction;--1001

    begin
        --
        ge_psecuencia( 'SF_SPRFD', p_ty_prfd.prfd_prfd , p_ty_erro.msg_error );
        --
        insert into sf_tprfd   (
                                  prfd_prfd
                                , prfd_fdei
                                , prfd_tpid
                                , prfd_usua
                                , prfd_esta
                                , prfd_obser
                                , prfd_tppr
                                , prfd_feccre
                                , prfd_fecmod
                                )
                        values (
                                  p_ty_prfd.prfd_prfd
                                , p_ty_prfd.prfd_fdei
                                , p_ty_prfd.prfd_tpid
                                , p_ty_prfd.prfd_usua
                                , p_ty_prfd.prfd_esta
                                , p_ty_prfd.prfd_obser
                                , p_ty_prfd.prfd_tppr
                                , trunc(p_ty_prfd.prfd_feccre)
                                , trunc(p_ty_prfd.prfd_fecmod)
                                );
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Inserci�n �xitosa '||sqlerrm;
        commit;
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'Inserci�n erronea insertar_SF_TPRFD : '||sqlerrm;
            --
    end insertar_sf_tprfd;
    --
    ----------------------------------------------------------------------------------
    --
    procedure actualiza_sf_tprfd (  p_ty_prfd       in out  ty_prfd
                                  , p_ty_erro       out     ge_qtipo.tr_error
                                 ) is
                                 pragma autonomous_transaction;--1001
    begin
        --
        update  sf_tprfd
        set
                prfd_fdei   =   nvl( p_ty_prfd.prfd_fdei  ,prfd_fdei   ) ,
                prfd_tpid   =   nvl( p_ty_prfd.prfd_tpid  ,prfd_tpid   ) ,
                prfd_usua   =   nvl( p_ty_prfd.prfd_usua  ,prfd_usua   ) ,
                prfd_esta   =   nvl( p_ty_prfd.prfd_esta  ,prfd_esta   ) ,
                prfd_obser  =   nvl( p_ty_prfd.prfd_obser ,prfd_obser  ) ,
                prfd_feccre =   nvl( trunc(p_ty_prfd.prfd_feccre),prfd_feccre ) ,
                prfd_fecmod =   nvl( trunc(p_ty_prfd.prfd_fecmod),prfd_fecmod ) ,
                prfd_tppr   =   nvl( p_ty_prfd.prfd_tppr  ,prfd_tppr   )

        where   prfd_prfd   =   p_ty_prfd.prfd_prfd     ;
        --
        p_ty_erro.cod_error := 'OK';
        p_ty_erro.msg_error := 'Actualizaci�n �xitosa '||sqlerrm;
         commit;
        --
    exception
        when others then
            --
            p_ty_erro.cod_error := 'ERROR';
            p_ty_erro.msg_error := 'Actualizaci�n erronea actualiza_SF_TPRFD : '||sqlerrm;
            --
    end actualiza_sf_tprfd;
    --
end SF_QPRFD_CRUD;
/